/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : captcode

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 15/09/2023 12:11:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dictionary
-- ----------------------------
DROP TABLE IF EXISTS `dictionary`;
CREATE TABLE `dictionary`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `singleTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'singleTitle',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `isLock` int(0) NOT NULL COMMENT '0启用1禁用',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '编码',
  `parentID` int(0) NULL DEFAULT NULL COMMENT '父级关系',
  `classList` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `classLayer` int(0) NULL DEFAULT NULL,
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dictionary
-- ----------------------------
INSERT INTO `dictionary` VALUES (2, '商品类型管理', '商品类型管理', 99, 0, 'ProductType', 0, NULL, NULL, '2023-09-11 16:02:07', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (3, '精品服饰', '精品服饰', 99, 0, '', 2, NULL, NULL, '2023-09-11 16:02:23', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (4, '手机数码', '手机数码', 99, 0, '', 2, NULL, NULL, '2023-09-11 16:02:31', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (5, '办公家电', '办公家电', 99, 0, '', 2, NULL, NULL, '2023-09-11 16:02:39', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (6, '美妆护肤', '美妆护肤', 99, 0, '', 2, NULL, NULL, '2023-09-11 16:02:46', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (7, '其他商品', '其他商品', 99, 0, '', 2, NULL, NULL, '2023-09-11 16:02:54', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (8, '特色女装', '特色女装', 99, 0, '', 3, NULL, NULL, '2023-09-11 16:03:40', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (9, '精品男装', '精品男装', 99, 0, '', 3, NULL, NULL, '2023-09-11 16:03:51', 1, 1, '2023-09-11 16:04:04', 1, 1);
INSERT INTO `dictionary` VALUES (10, '潮流女鞋', '潮流女鞋', 99, 0, '', 3, NULL, NULL, '2023-09-11 16:04:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (11, '品质男鞋', '品质男鞋', 99, 0, '', 3, NULL, NULL, '2023-09-11 16:04:22', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (12, '手机通讯', '手机通讯', 99, 0, '', 4, NULL, NULL, '2023-09-11 16:04:36', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (13, '手机配件', '手机配件', 99, 0, '', 4, NULL, NULL, '2023-09-11 16:04:48', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (14, '摄影摄像', '摄影摄像', 99, 0, '', 4, NULL, NULL, '2023-09-11 16:04:57', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (15, '数码配件', '数码配件', 99, 0, '', 4, NULL, NULL, '2023-09-11 16:05:08', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (16, '电脑整机', '电脑整机', 99, 0, '', 5, NULL, NULL, '2023-09-11 16:05:23', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (17, '电脑配件', '电脑配件', 99, 0, '', 5, NULL, NULL, '2023-09-11 16:05:34', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (18, '办公设备', '办公设备', 99, 0, '', 5, NULL, NULL, '2023-09-11 16:05:45', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (19, '家居电器', '家居电器', 99, 0, '', 5, NULL, NULL, '2023-09-11 16:05:54', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (20, '面部护理', '面部护理', 99, 0, '', 6, NULL, NULL, '2023-09-11 16:06:05', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (21, '魅力彩妆', '魅力彩妆', 99, 0, '', 6, NULL, NULL, '2023-09-11 16:06:15', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (22, '香水香氛', '香水香氛', 99, 0, '', 6, NULL, NULL, '2023-09-11 16:06:24', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (23, '美发护发', '美发护发', 99, 0, '', 6, NULL, NULL, '2023-09-11 16:06:35', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (24, '商品标签', '商品标签', 99, 0, 'productlabel', 0, NULL, NULL, '2023-09-12 12:13:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (25, '折扣', '折扣', 99, 0, '', 24, NULL, NULL, '2023-09-12 12:13:25', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (26, '预售', '预售', 99, 0, '', 24, NULL, NULL, '2023-09-12 12:13:36', 1, 1, NULL, NULL, NULL);
INSERT INTO `dictionary` VALUES (27, '拼团', '拼团', 99, 0, '', 24, NULL, NULL, '2023-09-12 12:13:51', 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for interfacelog
-- ----------------------------
DROP TABLE IF EXISTS `interfacelog`;
CREATE TABLE `interfacelog`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `interfacename` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '路由',
  `userid` bigint(0) NULL DEFAULT NULL,
  `userip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `resultcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '返回状态码',
  `requesttype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求类型',
  `resultmsg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '返回信息',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1701 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '接口日志监控' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of interfacelog
-- ----------------------------
INSERT INTO `interfacelog` VALUES (1, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 12:55:01');
INSERT INTO `interfacelog` VALUES (2, 'User/Login', 0, '::1', '200', 'POST', '登陆成功', '2023-08-30 12:55:21');
INSERT INTO `interfacelog` VALUES (3, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 12:55:25');
INSERT INTO `interfacelog` VALUES (4, 'User/outlogin', 0, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 12:56:21');
INSERT INTO `interfacelog` VALUES (5, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 12:56:22');
INSERT INTO `interfacelog` VALUES (6, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 13:03:38');
INSERT INTO `interfacelog` VALUES (7, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 13:03:38');
INSERT INTO `interfacelog` VALUES (8, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 13:03:39');
INSERT INTO `interfacelog` VALUES (9, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:04:14');
INSERT INTO `interfacelog` VALUES (10, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:04:50');
INSERT INTO `interfacelog` VALUES (11, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:04:50');
INSERT INTO `interfacelog` VALUES (12, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:04:51');
INSERT INTO `interfacelog` VALUES (13, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:04:51');
INSERT INTO `interfacelog` VALUES (14, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:04:52');
INSERT INTO `interfacelog` VALUES (15, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:04:52');
INSERT INTO `interfacelog` VALUES (16, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:06:45');
INSERT INTO `interfacelog` VALUES (17, 'Menu/Addmenu', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:07:01');
INSERT INTO `interfacelog` VALUES (18, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:04');
INSERT INTO `interfacelog` VALUES (19, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:07:04');
INSERT INTO `interfacelog` VALUES (20, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:06');
INSERT INTO `interfacelog` VALUES (21, 'Menu/Addmenu', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:07:16');
INSERT INTO `interfacelog` VALUES (22, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:20');
INSERT INTO `interfacelog` VALUES (23, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:07:20');
INSERT INTO `interfacelog` VALUES (24, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:21');
INSERT INTO `interfacelog` VALUES (25, 'Menu/Addmenu', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:07:29');
INSERT INTO `interfacelog` VALUES (26, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:32');
INSERT INTO `interfacelog` VALUES (27, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:07:32');
INSERT INTO `interfacelog` VALUES (28, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:35');
INSERT INTO `interfacelog` VALUES (29, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:35');
INSERT INTO `interfacelog` VALUES (30, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 13:07:40');
INSERT INTO `interfacelog` VALUES (31, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:07:43');
INSERT INTO `interfacelog` VALUES (32, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:07:44');
INSERT INTO `interfacelog` VALUES (33, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:07:44');
INSERT INTO `interfacelog` VALUES (34, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:07:50');
INSERT INTO `interfacelog` VALUES (35, 'Menu/addcheckbox', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:07:53');
INSERT INTO `interfacelog` VALUES (36, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:07:56');
INSERT INTO `interfacelog` VALUES (37, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:08:13');
INSERT INTO `interfacelog` VALUES (38, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:08:17');
INSERT INTO `interfacelog` VALUES (39, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:08:33');
INSERT INTO `interfacelog` VALUES (40, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:08:36');
INSERT INTO `interfacelog` VALUES (41, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:08:55');
INSERT INTO `interfacelog` VALUES (42, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:08:59');
INSERT INTO `interfacelog` VALUES (43, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:09:15');
INSERT INTO `interfacelog` VALUES (44, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:09:18');
INSERT INTO `interfacelog` VALUES (45, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:09:37');
INSERT INTO `interfacelog` VALUES (46, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:09:41');
INSERT INTO `interfacelog` VALUES (47, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:09:54');
INSERT INTO `interfacelog` VALUES (48, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:09:58');
INSERT INTO `interfacelog` VALUES (49, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:10:03');
INSERT INTO `interfacelog` VALUES (50, 'Menu/addcheckbox', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:10:06');
INSERT INTO `interfacelog` VALUES (51, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:10:09');
INSERT INTO `interfacelog` VALUES (52, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:10:22');
INSERT INTO `interfacelog` VALUES (53, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:10:25');
INSERT INTO `interfacelog` VALUES (54, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:11:24');
INSERT INTO `interfacelog` VALUES (55, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:11:27');
INSERT INTO `interfacelog` VALUES (56, 'Menu/getfieldinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:11:31');
INSERT INTO `interfacelog` VALUES (57, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 13:11:34');
INSERT INTO `interfacelog` VALUES (58, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:11:37');
INSERT INTO `interfacelog` VALUES (59, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:11:59');
INSERT INTO `interfacelog` VALUES (60, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:12:04');
INSERT INTO `interfacelog` VALUES (61, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:12:17');
INSERT INTO `interfacelog` VALUES (62, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:12:21');
INSERT INTO `interfacelog` VALUES (63, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:12:35');
INSERT INTO `interfacelog` VALUES (64, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:12:38');
INSERT INTO `interfacelog` VALUES (65, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:12:59');
INSERT INTO `interfacelog` VALUES (66, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:13:03');
INSERT INTO `interfacelog` VALUES (67, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:13:23');
INSERT INTO `interfacelog` VALUES (68, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:13:27');
INSERT INTO `interfacelog` VALUES (69, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 13:13:40');
INSERT INTO `interfacelog` VALUES (70, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:13:43');
INSERT INTO `interfacelog` VALUES (71, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:13:45');
INSERT INTO `interfacelog` VALUES (72, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:14:01');
INSERT INTO `interfacelog` VALUES (73, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:14:13');
INSERT INTO `interfacelog` VALUES (74, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:14:41');
INSERT INTO `interfacelog` VALUES (75, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:04');
INSERT INTO `interfacelog` VALUES (76, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:06');
INSERT INTO `interfacelog` VALUES (77, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:21');
INSERT INTO `interfacelog` VALUES (78, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:23');
INSERT INTO `interfacelog` VALUES (79, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:35');
INSERT INTO `interfacelog` VALUES (80, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:43');
INSERT INTO `interfacelog` VALUES (81, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:53');
INSERT INTO `interfacelog` VALUES (82, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:15:59');
INSERT INTO `interfacelog` VALUES (83, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:03');
INSERT INTO `interfacelog` VALUES (84, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:07');
INSERT INTO `interfacelog` VALUES (85, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:09');
INSERT INTO `interfacelog` VALUES (86, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:12');
INSERT INTO `interfacelog` VALUES (87, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:14');
INSERT INTO `interfacelog` VALUES (88, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:27');
INSERT INTO `interfacelog` VALUES (89, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:29');
INSERT INTO `interfacelog` VALUES (90, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:16:31');
INSERT INTO `interfacelog` VALUES (91, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:29');
INSERT INTO `interfacelog` VALUES (92, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:17:29');
INSERT INTO `interfacelog` VALUES (93, 'Menu/RolePer', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:30');
INSERT INTO `interfacelog` VALUES (94, 'Menu/setrolepermise', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 13:17:36');
INSERT INTO `interfacelog` VALUES (95, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:41');
INSERT INTO `interfacelog` VALUES (96, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:17:41');
INSERT INTO `interfacelog` VALUES (97, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:49');
INSERT INTO `interfacelog` VALUES (98, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:52');
INSERT INTO `interfacelog` VALUES (99, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:54');
INSERT INTO `interfacelog` VALUES (100, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:56');
INSERT INTO `interfacelog` VALUES (101, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:57');
INSERT INTO `interfacelog` VALUES (102, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:58');
INSERT INTO `interfacelog` VALUES (103, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:17:59');
INSERT INTO `interfacelog` VALUES (104, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:18:10');
INSERT INTO `interfacelog` VALUES (105, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:19:01');
INSERT INTO `interfacelog` VALUES (106, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:19:01');
INSERT INTO `interfacelog` VALUES (107, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:19:04');
INSERT INTO `interfacelog` VALUES (108, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:19:04');
INSERT INTO `interfacelog` VALUES (109, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:19:05');
INSERT INTO `interfacelog` VALUES (110, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:19:07');
INSERT INTO `interfacelog` VALUES (111, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:19:07');
INSERT INTO `interfacelog` VALUES (112, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 13:19:10');
INSERT INTO `interfacelog` VALUES (113, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:19:13');
INSERT INTO `interfacelog` VALUES (114, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:20:02');
INSERT INTO `interfacelog` VALUES (115, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:20:04');
INSERT INTO `interfacelog` VALUES (116, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:20:04');
INSERT INTO `interfacelog` VALUES (117, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 13:20:05');
INSERT INTO `interfacelog` VALUES (118, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:20:08');
INSERT INTO `interfacelog` VALUES (119, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 13:20:08');
INSERT INTO `interfacelog` VALUES (120, 'Menu/Addmenu', 1, '127.0.0.1', '200', 'POST', '修改成功', '2023-08-30 13:20:51');
INSERT INTO `interfacelog` VALUES (121, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:20:54');
INSERT INTO `interfacelog` VALUES (122, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:20:54');
INSERT INTO `interfacelog` VALUES (123, 'Menu/SysMenulist', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:20:57');
INSERT INTO `interfacelog` VALUES (124, 'Menu/SysMenulist', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:21:00');
INSERT INTO `interfacelog` VALUES (125, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:21:03');
INSERT INTO `interfacelog` VALUES (126, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:21:03');
INSERT INTO `interfacelog` VALUES (127, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:21:04');
INSERT INTO `interfacelog` VALUES (128, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:21:07');
INSERT INTO `interfacelog` VALUES (129, 'Menu/getmenuinfo', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:21:13');
INSERT INTO `interfacelog` VALUES (130, 'Menu/Treeselect', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:21:13');
INSERT INTO `interfacelog` VALUES (131, 'Menu/Addmenu', 1, '127.0.0.1', '200', 'POST', '修改成功', '2023-08-30 13:22:17');
INSERT INTO `interfacelog` VALUES (132, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:22:20');
INSERT INTO `interfacelog` VALUES (133, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:22:20');
INSERT INTO `interfacelog` VALUES (134, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:22:21');
INSERT INTO `interfacelog` VALUES (135, 'Menu/Treeselect', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:22:25');
INSERT INTO `interfacelog` VALUES (136, 'Menu/getmenuinfo', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:22:25');
INSERT INTO `interfacelog` VALUES (137, 'Menu/Addmenu', 1, '127.0.0.1', '200', 'POST', '修改成功', '2023-08-30 13:22:38');
INSERT INTO `interfacelog` VALUES (138, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:22:41');
INSERT INTO `interfacelog` VALUES (139, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:22:41');
INSERT INTO `interfacelog` VALUES (140, 'Menu/SysMenulist', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:22:43');
INSERT INTO `interfacelog` VALUES (141, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:22:54');
INSERT INTO `interfacelog` VALUES (142, 'Menu/menulist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:22:54');
INSERT INTO `interfacelog` VALUES (143, 'Menu/SysMenulist', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:23:20');
INSERT INTO `interfacelog` VALUES (144, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:23:22');
INSERT INTO `interfacelog` VALUES (145, 'Role/rolelist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:23:22');
INSERT INTO `interfacelog` VALUES (146, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:23:24');
INSERT INTO `interfacelog` VALUES (147, 'Role/rolelist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:23:24');
INSERT INTO `interfacelog` VALUES (148, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:23:28');
INSERT INTO `interfacelog` VALUES (149, 'Role/rolelist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:23:28');
INSERT INTO `interfacelog` VALUES (150, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:23:35');
INSERT INTO `interfacelog` VALUES (151, 'Role/rolelist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:23:35');
INSERT INTO `interfacelog` VALUES (152, 'Menu/getbtn', 1, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:23:42');
INSERT INTO `interfacelog` VALUES (153, 'Role/rolelist', 1, '127.0.0.1', '200', 'POST', 'Success', '2023-08-30 13:23:42');
INSERT INTO `interfacelog` VALUES (154, 'User/Captcha', 0, '127.0.0.1', '200', 'GET', 'Success', '2023-08-30 13:24:11');
INSERT INTO `interfacelog` VALUES (155, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 13:30:52');
INSERT INTO `interfacelog` VALUES (156, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 14:08:52');
INSERT INTO `interfacelog` VALUES (157, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:01');
INSERT INTO `interfacelog` VALUES (158, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:03');
INSERT INTO `interfacelog` VALUES (159, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:03');
INSERT INTO `interfacelog` VALUES (160, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:05');
INSERT INTO `interfacelog` VALUES (161, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:05');
INSERT INTO `interfacelog` VALUES (162, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:41');
INSERT INTO `interfacelog` VALUES (163, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:50');
INSERT INTO `interfacelog` VALUES (164, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:53');
INSERT INTO `interfacelog` VALUES (165, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:53');
INSERT INTO `interfacelog` VALUES (166, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:54');
INSERT INTO `interfacelog` VALUES (167, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:54');
INSERT INTO `interfacelog` VALUES (168, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:55');
INSERT INTO `interfacelog` VALUES (169, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:55');
INSERT INTO `interfacelog` VALUES (170, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:56');
INSERT INTO `interfacelog` VALUES (171, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:56');
INSERT INTO `interfacelog` VALUES (172, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:57');
INSERT INTO `interfacelog` VALUES (173, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:57');
INSERT INTO `interfacelog` VALUES (174, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:58');
INSERT INTO `interfacelog` VALUES (175, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:58');
INSERT INTO `interfacelog` VALUES (176, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:09:59');
INSERT INTO `interfacelog` VALUES (177, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:09:59');
INSERT INTO `interfacelog` VALUES (178, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:10:00');
INSERT INTO `interfacelog` VALUES (179, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:10:00');
INSERT INTO `interfacelog` VALUES (180, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:10:01');
INSERT INTO `interfacelog` VALUES (181, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:10:01');
INSERT INTO `interfacelog` VALUES (182, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:10:02');
INSERT INTO `interfacelog` VALUES (183, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:10:02');
INSERT INTO `interfacelog` VALUES (184, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:10:02');
INSERT INTO `interfacelog` VALUES (185, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:10:02');
INSERT INTO `interfacelog` VALUES (186, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:10:02');
INSERT INTO `interfacelog` VALUES (187, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:10:02');
INSERT INTO `interfacelog` VALUES (188, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:26:07');
INSERT INTO `interfacelog` VALUES (189, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:26:08');
INSERT INTO `interfacelog` VALUES (190, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:26:18');
INSERT INTO `interfacelog` VALUES (191, 'Menu/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:26:18');
INSERT INTO `interfacelog` VALUES (192, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:26:27');
INSERT INTO `interfacelog` VALUES (193, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:26:27');
INSERT INTO `interfacelog` VALUES (194, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:28:26');
INSERT INTO `interfacelog` VALUES (195, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 14:32:28');
INSERT INTO `interfacelog` VALUES (196, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:35:32');
INSERT INTO `interfacelog` VALUES (197, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:35:35');
INSERT INTO `interfacelog` VALUES (198, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:35');
INSERT INTO `interfacelog` VALUES (199, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:37');
INSERT INTO `interfacelog` VALUES (200, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:38');
INSERT INTO `interfacelog` VALUES (201, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:38');
INSERT INTO `interfacelog` VALUES (202, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:39');
INSERT INTO `interfacelog` VALUES (203, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:42');
INSERT INTO `interfacelog` VALUES (204, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:44');
INSERT INTO `interfacelog` VALUES (205, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:45');
INSERT INTO `interfacelog` VALUES (206, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:46');
INSERT INTO `interfacelog` VALUES (207, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:46');
INSERT INTO `interfacelog` VALUES (208, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:46');
INSERT INTO `interfacelog` VALUES (209, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:46');
INSERT INTO `interfacelog` VALUES (210, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:47');
INSERT INTO `interfacelog` VALUES (211, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:47');
INSERT INTO `interfacelog` VALUES (212, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:47');
INSERT INTO `interfacelog` VALUES (213, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:47');
INSERT INTO `interfacelog` VALUES (214, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:49');
INSERT INTO `interfacelog` VALUES (215, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:50');
INSERT INTO `interfacelog` VALUES (216, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:51');
INSERT INTO `interfacelog` VALUES (217, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:52');
INSERT INTO `interfacelog` VALUES (218, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:52');
INSERT INTO `interfacelog` VALUES (219, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:52');
INSERT INTO `interfacelog` VALUES (220, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:53');
INSERT INTO `interfacelog` VALUES (221, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:53');
INSERT INTO `interfacelog` VALUES (222, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:54');
INSERT INTO `interfacelog` VALUES (223, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:54');
INSERT INTO `interfacelog` VALUES (224, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:54');
INSERT INTO `interfacelog` VALUES (225, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:54');
INSERT INTO `interfacelog` VALUES (226, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:54');
INSERT INTO `interfacelog` VALUES (227, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:55');
INSERT INTO `interfacelog` VALUES (228, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:55');
INSERT INTO `interfacelog` VALUES (229, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:55');
INSERT INTO `interfacelog` VALUES (230, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:55');
INSERT INTO `interfacelog` VALUES (231, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:55');
INSERT INTO `interfacelog` VALUES (232, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:56');
INSERT INTO `interfacelog` VALUES (233, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:56');
INSERT INTO `interfacelog` VALUES (234, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:35:56');
INSERT INTO `interfacelog` VALUES (235, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:37:55');
INSERT INTO `interfacelog` VALUES (236, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:37:56');
INSERT INTO `interfacelog` VALUES (237, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:24');
INSERT INTO `interfacelog` VALUES (238, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:34');
INSERT INTO `interfacelog` VALUES (239, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:36');
INSERT INTO `interfacelog` VALUES (240, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:36');
INSERT INTO `interfacelog` VALUES (241, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:37');
INSERT INTO `interfacelog` VALUES (242, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:37');
INSERT INTO `interfacelog` VALUES (243, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:38:39');
INSERT INTO `interfacelog` VALUES (244, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:39');
INSERT INTO `interfacelog` VALUES (245, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:38:43');
INSERT INTO `interfacelog` VALUES (246, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:43');
INSERT INTO `interfacelog` VALUES (247, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:46');
INSERT INTO `interfacelog` VALUES (248, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:49');
INSERT INTO `interfacelog` VALUES (249, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:50');
INSERT INTO `interfacelog` VALUES (250, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:51');
INSERT INTO `interfacelog` VALUES (251, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:51');
INSERT INTO `interfacelog` VALUES (252, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:52');
INSERT INTO `interfacelog` VALUES (253, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:52');
INSERT INTO `interfacelog` VALUES (254, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:53');
INSERT INTO `interfacelog` VALUES (255, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:53');
INSERT INTO `interfacelog` VALUES (256, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:53');
INSERT INTO `interfacelog` VALUES (257, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:56');
INSERT INTO `interfacelog` VALUES (258, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:57');
INSERT INTO `interfacelog` VALUES (259, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:57');
INSERT INTO `interfacelog` VALUES (260, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:38:59');
INSERT INTO `interfacelog` VALUES (261, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:38:59');
INSERT INTO `interfacelog` VALUES (262, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:39:00');
INSERT INTO `interfacelog` VALUES (263, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:39:00');
INSERT INTO `interfacelog` VALUES (264, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:39:02');
INSERT INTO `interfacelog` VALUES (265, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:39:02');
INSERT INTO `interfacelog` VALUES (266, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:39:03');
INSERT INTO `interfacelog` VALUES (267, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:39:03');
INSERT INTO `interfacelog` VALUES (268, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:39:06');
INSERT INTO `interfacelog` VALUES (269, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:39:07');
INSERT INTO `interfacelog` VALUES (270, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:48:19');
INSERT INTO `interfacelog` VALUES (271, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:48:19');
INSERT INTO `interfacelog` VALUES (272, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:48:21');
INSERT INTO `interfacelog` VALUES (273, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:48:21');
INSERT INTO `interfacelog` VALUES (274, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:48:26');
INSERT INTO `interfacelog` VALUES (275, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:48:26');
INSERT INTO `interfacelog` VALUES (276, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 14:52:09');
INSERT INTO `interfacelog` VALUES (277, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 14:52:09');
INSERT INTO `interfacelog` VALUES (278, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 15:40:50');
INSERT INTO `interfacelog` VALUES (279, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:41:31');
INSERT INTO `interfacelog` VALUES (280, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:42:57');
INSERT INTO `interfacelog` VALUES (281, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:42:57');
INSERT INTO `interfacelog` VALUES (282, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:42:59');
INSERT INTO `interfacelog` VALUES (283, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:43:04');
INSERT INTO `interfacelog` VALUES (284, 'Menu/Addmenu', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:43:27');
INSERT INTO `interfacelog` VALUES (285, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:43:31');
INSERT INTO `interfacelog` VALUES (286, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:43:31');
INSERT INTO `interfacelog` VALUES (287, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:43:31');
INSERT INTO `interfacelog` VALUES (288, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:43:41');
INSERT INTO `interfacelog` VALUES (289, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:43:41');
INSERT INTO `interfacelog` VALUES (290, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 15:43:48');
INSERT INTO `interfacelog` VALUES (291, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:43:51');
INSERT INTO `interfacelog` VALUES (292, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:43:51');
INSERT INTO `interfacelog` VALUES (293, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:43:52');
INSERT INTO `interfacelog` VALUES (294, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:43:55');
INSERT INTO `interfacelog` VALUES (295, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:44:02');
INSERT INTO `interfacelog` VALUES (296, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:44:13');
INSERT INTO `interfacelog` VALUES (297, 'Menu/addtempt', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:44:17');
INSERT INTO `interfacelog` VALUES (298, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:44:21');
INSERT INTO `interfacelog` VALUES (299, 'Menu/addcheckbox', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:44:21');
INSERT INTO `interfacelog` VALUES (300, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:44:24');
INSERT INTO `interfacelog` VALUES (301, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:45:06');
INSERT INTO `interfacelog` VALUES (302, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:45:09');
INSERT INTO `interfacelog` VALUES (303, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:45:29');
INSERT INTO `interfacelog` VALUES (304, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:45:32');
INSERT INTO `interfacelog` VALUES (305, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:45:43');
INSERT INTO `interfacelog` VALUES (306, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:45:46');
INSERT INTO `interfacelog` VALUES (307, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:45:55');
INSERT INTO `interfacelog` VALUES (308, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:46:00');
INSERT INTO `interfacelog` VALUES (309, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:46:12');
INSERT INTO `interfacelog` VALUES (310, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:46:16');
INSERT INTO `interfacelog` VALUES (311, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:46:43');
INSERT INTO `interfacelog` VALUES (312, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:46:46');
INSERT INTO `interfacelog` VALUES (313, 'Menu/getfieldinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:46:51');
INSERT INTO `interfacelog` VALUES (314, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 15:46:55');
INSERT INTO `interfacelog` VALUES (315, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:46:59');
INSERT INTO `interfacelog` VALUES (316, 'Menu/getfieldinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:47:00');
INSERT INTO `interfacelog` VALUES (317, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 15:47:03');
INSERT INTO `interfacelog` VALUES (318, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:47:08');
INSERT INTO `interfacelog` VALUES (319, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:47:40');
INSERT INTO `interfacelog` VALUES (320, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:47:41');
INSERT INTO `interfacelog` VALUES (321, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:47:48');
INSERT INTO `interfacelog` VALUES (322, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:47:53');
INSERT INTO `interfacelog` VALUES (323, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:03');
INSERT INTO `interfacelog` VALUES (324, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:06');
INSERT INTO `interfacelog` VALUES (325, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:08');
INSERT INTO `interfacelog` VALUES (326, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:10');
INSERT INTO `interfacelog` VALUES (327, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:13');
INSERT INTO `interfacelog` VALUES (328, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:20');
INSERT INTO `interfacelog` VALUES (329, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:26');
INSERT INTO `interfacelog` VALUES (330, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:48:28');
INSERT INTO `interfacelog` VALUES (331, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:03');
INSERT INTO `interfacelog` VALUES (332, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:05');
INSERT INTO `interfacelog` VALUES (333, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:06');
INSERT INTO `interfacelog` VALUES (334, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:11');
INSERT INTO `interfacelog` VALUES (335, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:14');
INSERT INTO `interfacelog` VALUES (336, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:23');
INSERT INTO `interfacelog` VALUES (337, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:25');
INSERT INTO `interfacelog` VALUES (338, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:27');
INSERT INTO `interfacelog` VALUES (339, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:28');
INSERT INTO `interfacelog` VALUES (340, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:30');
INSERT INTO `interfacelog` VALUES (341, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:34');
INSERT INTO `interfacelog` VALUES (342, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:36');
INSERT INTO `interfacelog` VALUES (343, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:40');
INSERT INTO `interfacelog` VALUES (344, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:42');
INSERT INTO `interfacelog` VALUES (345, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:50');
INSERT INTO `interfacelog` VALUES (346, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:54');
INSERT INTO `interfacelog` VALUES (347, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:55');
INSERT INTO `interfacelog` VALUES (348, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:49:59');
INSERT INTO `interfacelog` VALUES (349, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:04');
INSERT INTO `interfacelog` VALUES (350, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:08');
INSERT INTO `interfacelog` VALUES (351, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:13');
INSERT INTO `interfacelog` VALUES (352, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:16');
INSERT INTO `interfacelog` VALUES (353, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:50:16');
INSERT INTO `interfacelog` VALUES (354, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:17');
INSERT INTO `interfacelog` VALUES (355, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:50:17');
INSERT INTO `interfacelog` VALUES (356, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:50:18');
INSERT INTO `interfacelog` VALUES (357, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:50:28');
INSERT INTO `interfacelog` VALUES (358, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:47');
INSERT INTO `interfacelog` VALUES (359, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:48');
INSERT INTO `interfacelog` VALUES (360, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:50');
INSERT INTO `interfacelog` VALUES (361, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:50:55');
INSERT INTO `interfacelog` VALUES (362, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:50:55');
INSERT INTO `interfacelog` VALUES (363, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:50:57');
INSERT INTO `interfacelog` VALUES (364, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:51:00');
INSERT INTO `interfacelog` VALUES (365, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:51:12');
INSERT INTO `interfacelog` VALUES (366, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:51:14');
INSERT INTO `interfacelog` VALUES (367, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:51:27');
INSERT INTO `interfacelog` VALUES (368, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:51:27');
INSERT INTO `interfacelog` VALUES (369, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:51:28');
INSERT INTO `interfacelog` VALUES (370, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:51:34');
INSERT INTO `interfacelog` VALUES (371, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:51:39');
INSERT INTO `interfacelog` VALUES (372, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:52:17');
INSERT INTO `interfacelog` VALUES (373, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:52:17');
INSERT INTO `interfacelog` VALUES (374, 'Menu/RolePer', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:52:20');
INSERT INTO `interfacelog` VALUES (375, 'Menu/setrolepermise', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 15:52:25');
INSERT INTO `interfacelog` VALUES (376, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:52:27');
INSERT INTO `interfacelog` VALUES (377, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:52:45');
INSERT INTO `interfacelog` VALUES (378, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:52:58');
INSERT INTO `interfacelog` VALUES (379, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:53:01');
INSERT INTO `interfacelog` VALUES (380, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:53:05');
INSERT INTO `interfacelog` VALUES (381, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:53:06');
INSERT INTO `interfacelog` VALUES (382, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:57:08');
INSERT INTO `interfacelog` VALUES (383, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:57:08');
INSERT INTO `interfacelog` VALUES (384, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:57:09');
INSERT INTO `interfacelog` VALUES (385, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:57:11');
INSERT INTO `interfacelog` VALUES (386, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 15:57:24');
INSERT INTO `interfacelog` VALUES (387, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:57:27');
INSERT INTO `interfacelog` VALUES (388, 'Menu/getfieldinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:57:32');
INSERT INTO `interfacelog` VALUES (389, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 15:57:36');
INSERT INTO `interfacelog` VALUES (390, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:57:39');
INSERT INTO `interfacelog` VALUES (391, 'Menu/getfieldinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-30 15:57:45');
INSERT INTO `interfacelog` VALUES (392, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '修改成功', '2023-08-30 15:57:48');
INSERT INTO `interfacelog` VALUES (393, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 15:57:51');
INSERT INTO `interfacelog` VALUES (394, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 16:07:05');
INSERT INTO `interfacelog` VALUES (395, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:07:12');
INSERT INTO `interfacelog` VALUES (396, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:07:15');
INSERT INTO `interfacelog` VALUES (397, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:07:57');
INSERT INTO `interfacelog` VALUES (398, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:07:57');
INSERT INTO `interfacelog` VALUES (399, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:08:33');
INSERT INTO `interfacelog` VALUES (400, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:08:33');
INSERT INTO `interfacelog` VALUES (401, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:11:33');
INSERT INTO `interfacelog` VALUES (402, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:12:06');
INSERT INTO `interfacelog` VALUES (403, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:13:57');
INSERT INTO `interfacelog` VALUES (404, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:13:57');
INSERT INTO `interfacelog` VALUES (405, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:13:57');
INSERT INTO `interfacelog` VALUES (406, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:13:59');
INSERT INTO `interfacelog` VALUES (407, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:13:59');
INSERT INTO `interfacelog` VALUES (408, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:01');
INSERT INTO `interfacelog` VALUES (409, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:01');
INSERT INTO `interfacelog` VALUES (410, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:02');
INSERT INTO `interfacelog` VALUES (411, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:02');
INSERT INTO `interfacelog` VALUES (412, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:08');
INSERT INTO `interfacelog` VALUES (413, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:08');
INSERT INTO `interfacelog` VALUES (414, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:11');
INSERT INTO `interfacelog` VALUES (415, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:11');
INSERT INTO `interfacelog` VALUES (416, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:14');
INSERT INTO `interfacelog` VALUES (417, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:14');
INSERT INTO `interfacelog` VALUES (418, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:17');
INSERT INTO `interfacelog` VALUES (419, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:17');
INSERT INTO `interfacelog` VALUES (420, 'Menu/RolePer', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:19');
INSERT INTO `interfacelog` VALUES (421, 'Menu/setrolepermise', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:14:22');
INSERT INTO `interfacelog` VALUES (422, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:25');
INSERT INTO `interfacelog` VALUES (423, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:26');
INSERT INTO `interfacelog` VALUES (424, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:27');
INSERT INTO `interfacelog` VALUES (425, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:27');
INSERT INTO `interfacelog` VALUES (426, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:29');
INSERT INTO `interfacelog` VALUES (427, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:29');
INSERT INTO `interfacelog` VALUES (428, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:30');
INSERT INTO `interfacelog` VALUES (429, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:30');
INSERT INTO `interfacelog` VALUES (430, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:31');
INSERT INTO `interfacelog` VALUES (431, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:31');
INSERT INTO `interfacelog` VALUES (432, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:14:32');
INSERT INTO `interfacelog` VALUES (433, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:14:32');
INSERT INTO `interfacelog` VALUES (434, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:00');
INSERT INTO `interfacelog` VALUES (435, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:01');
INSERT INTO `interfacelog` VALUES (436, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:09');
INSERT INTO `interfacelog` VALUES (437, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:15');
INSERT INTO `interfacelog` VALUES (438, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:18');
INSERT INTO `interfacelog` VALUES (439, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:15:18');
INSERT INTO `interfacelog` VALUES (440, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:19');
INSERT INTO `interfacelog` VALUES (441, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:15:19');
INSERT INTO `interfacelog` VALUES (442, 'Menu/RolePer', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:21');
INSERT INTO `interfacelog` VALUES (443, 'Menu/setrolepermise', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:15:24');
INSERT INTO `interfacelog` VALUES (444, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:27');
INSERT INTO `interfacelog` VALUES (445, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:15:27');
INSERT INTO `interfacelog` VALUES (446, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:41');
INSERT INTO `interfacelog` VALUES (447, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:44');
INSERT INTO `interfacelog` VALUES (448, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:46');
INSERT INTO `interfacelog` VALUES (449, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:15:51');
INSERT INTO `interfacelog` VALUES (450, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:15:51');
INSERT INTO `interfacelog` VALUES (451, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:16:31');
INSERT INTO `interfacelog` VALUES (452, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:16:34');
INSERT INTO `interfacelog` VALUES (453, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:16:36');
INSERT INTO `interfacelog` VALUES (454, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:16:38');
INSERT INTO `interfacelog` VALUES (455, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:16:42');
INSERT INTO `interfacelog` VALUES (456, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:16:45');
INSERT INTO `interfacelog` VALUES (457, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:16:46');
INSERT INTO `interfacelog` VALUES (458, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:17:23');
INSERT INTO `interfacelog` VALUES (459, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:17:25');
INSERT INTO `interfacelog` VALUES (460, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:17:36');
INSERT INTO `interfacelog` VALUES (461, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:17:42');
INSERT INTO `interfacelog` VALUES (462, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:17:52');
INSERT INTO `interfacelog` VALUES (463, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:17:59');
INSERT INTO `interfacelog` VALUES (464, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:18:02');
INSERT INTO `interfacelog` VALUES (465, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:18:04');
INSERT INTO `interfacelog` VALUES (466, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:18:05');
INSERT INTO `interfacelog` VALUES (467, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:18:12');
INSERT INTO `interfacelog` VALUES (468, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:18:54');
INSERT INTO `interfacelog` VALUES (469, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:18:58');
INSERT INTO `interfacelog` VALUES (470, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:18:58');
INSERT INTO `interfacelog` VALUES (471, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:18:59');
INSERT INTO `interfacelog` VALUES (472, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:18:59');
INSERT INTO `interfacelog` VALUES (473, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:19:39');
INSERT INTO `interfacelog` VALUES (474, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:19:40');
INSERT INTO `interfacelog` VALUES (475, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:19:43');
INSERT INTO `interfacelog` VALUES (476, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:19:46');
INSERT INTO `interfacelog` VALUES (477, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:19:47');
INSERT INTO `interfacelog` VALUES (478, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:20:08');
INSERT INTO `interfacelog` VALUES (479, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:20:09');
INSERT INTO `interfacelog` VALUES (480, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:20:32');
INSERT INTO `interfacelog` VALUES (481, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:20:36');
INSERT INTO `interfacelog` VALUES (482, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:20:36');
INSERT INTO `interfacelog` VALUES (483, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:20:58');
INSERT INTO `interfacelog` VALUES (484, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:20:58');
INSERT INTO `interfacelog` VALUES (485, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:21:22');
INSERT INTO `interfacelog` VALUES (486, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:21:22');
INSERT INTO `interfacelog` VALUES (487, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:21:38');
INSERT INTO `interfacelog` VALUES (488, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:21:42');
INSERT INTO `interfacelog` VALUES (489, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:21:48');
INSERT INTO `interfacelog` VALUES (490, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:21:54');
INSERT INTO `interfacelog` VALUES (491, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:22:50');
INSERT INTO `interfacelog` VALUES (492, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:22:55');
INSERT INTO `interfacelog` VALUES (493, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:22:57');
INSERT INTO `interfacelog` VALUES (494, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:01');
INSERT INTO `interfacelog` VALUES (495, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:05');
INSERT INTO `interfacelog` VALUES (496, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:09');
INSERT INTO `interfacelog` VALUES (497, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:16');
INSERT INTO `interfacelog` VALUES (498, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:18');
INSERT INTO `interfacelog` VALUES (499, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:20');
INSERT INTO `interfacelog` VALUES (500, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:23');
INSERT INTO `interfacelog` VALUES (501, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:24');
INSERT INTO `interfacelog` VALUES (502, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:26');
INSERT INTO `interfacelog` VALUES (503, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:23:26');
INSERT INTO `interfacelog` VALUES (504, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:27');
INSERT INTO `interfacelog` VALUES (505, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:23:27');
INSERT INTO `interfacelog` VALUES (506, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:32');
INSERT INTO `interfacelog` VALUES (507, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:35');
INSERT INTO `interfacelog` VALUES (508, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:23:35');
INSERT INTO `interfacelog` VALUES (509, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:23:40');
INSERT INTO `interfacelog` VALUES (510, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:25:51');
INSERT INTO `interfacelog` VALUES (511, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:26:26');
INSERT INTO `interfacelog` VALUES (512, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:26:30');
INSERT INTO `interfacelog` VALUES (513, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:26:31');
INSERT INTO `interfacelog` VALUES (514, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:26:37');
INSERT INTO `interfacelog` VALUES (515, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:26:39');
INSERT INTO `interfacelog` VALUES (516, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 16:27:49');
INSERT INTO `interfacelog` VALUES (517, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:03');
INSERT INTO `interfacelog` VALUES (518, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:08');
INSERT INTO `interfacelog` VALUES (519, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:28:08');
INSERT INTO `interfacelog` VALUES (520, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:23');
INSERT INTO `interfacelog` VALUES (521, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:24');
INSERT INTO `interfacelog` VALUES (522, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:26');
INSERT INTO `interfacelog` VALUES (523, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:30');
INSERT INTO `interfacelog` VALUES (524, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:33');
INSERT INTO `interfacelog` VALUES (525, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:28:38');
INSERT INTO `interfacelog` VALUES (526, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:28:38');
INSERT INTO `interfacelog` VALUES (527, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:29:23');
INSERT INTO `interfacelog` VALUES (528, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:29:26');
INSERT INTO `interfacelog` VALUES (529, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:30:15');
INSERT INTO `interfacelog` VALUES (530, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:30:15');
INSERT INTO `interfacelog` VALUES (531, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:31:08');
INSERT INTO `interfacelog` VALUES (532, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:31:09');
INSERT INTO `interfacelog` VALUES (533, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:31:12');
INSERT INTO `interfacelog` VALUES (534, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:31:14');
INSERT INTO `interfacelog` VALUES (535, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:31:20');
INSERT INTO `interfacelog` VALUES (536, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:31:23');
INSERT INTO `interfacelog` VALUES (537, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:31:23');
INSERT INTO `interfacelog` VALUES (538, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:32:03');
INSERT INTO `interfacelog` VALUES (539, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:32:07');
INSERT INTO `interfacelog` VALUES (540, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:32:07');
INSERT INTO `interfacelog` VALUES (541, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:32:08');
INSERT INTO `interfacelog` VALUES (542, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:32:08');
INSERT INTO `interfacelog` VALUES (543, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:32:56');
INSERT INTO `interfacelog` VALUES (544, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:03');
INSERT INTO `interfacelog` VALUES (545, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:06');
INSERT INTO `interfacelog` VALUES (546, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:08');
INSERT INTO `interfacelog` VALUES (547, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:30');
INSERT INTO `interfacelog` VALUES (548, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:34');
INSERT INTO `interfacelog` VALUES (549, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:35');
INSERT INTO `interfacelog` VALUES (550, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:39');
INSERT INTO `interfacelog` VALUES (551, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:40');
INSERT INTO `interfacelog` VALUES (552, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:43');
INSERT INTO `interfacelog` VALUES (553, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:49');
INSERT INTO `interfacelog` VALUES (554, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:56');
INSERT INTO `interfacelog` VALUES (555, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:33:58');
INSERT INTO `interfacelog` VALUES (556, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:08');
INSERT INTO `interfacelog` VALUES (557, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:10');
INSERT INTO `interfacelog` VALUES (558, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:14');
INSERT INTO `interfacelog` VALUES (559, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:31');
INSERT INTO `interfacelog` VALUES (560, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:32');
INSERT INTO `interfacelog` VALUES (561, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:35');
INSERT INTO `interfacelog` VALUES (562, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:38');
INSERT INTO `interfacelog` VALUES (563, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:39');
INSERT INTO `interfacelog` VALUES (564, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:34:40');
INSERT INTO `interfacelog` VALUES (565, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:35:02');
INSERT INTO `interfacelog` VALUES (566, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:36:12');
INSERT INTO `interfacelog` VALUES (567, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:36:12');
INSERT INTO `interfacelog` VALUES (568, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:42:33');
INSERT INTO `interfacelog` VALUES (569, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:42:33');
INSERT INTO `interfacelog` VALUES (570, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:43:11');
INSERT INTO `interfacelog` VALUES (571, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:43:17');
INSERT INTO `interfacelog` VALUES (572, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:43:17');
INSERT INTO `interfacelog` VALUES (573, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:43:31');
INSERT INTO `interfacelog` VALUES (574, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:43:34');
INSERT INTO `interfacelog` VALUES (575, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:43:36');
INSERT INTO `interfacelog` VALUES (576, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:43:36');
INSERT INTO `interfacelog` VALUES (577, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:43:37');
INSERT INTO `interfacelog` VALUES (578, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:43:55');
INSERT INTO `interfacelog` VALUES (579, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:44:11');
INSERT INTO `interfacelog` VALUES (580, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:44:50');
INSERT INTO `interfacelog` VALUES (581, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:44:52');
INSERT INTO `interfacelog` VALUES (582, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:46:23');
INSERT INTO `interfacelog` VALUES (583, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:46:28');
INSERT INTO `interfacelog` VALUES (584, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:46:30');
INSERT INTO `interfacelog` VALUES (585, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:46:37');
INSERT INTO `interfacelog` VALUES (586, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:46:37');
INSERT INTO `interfacelog` VALUES (587, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:46:39');
INSERT INTO `interfacelog` VALUES (588, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:46:53');
INSERT INTO `interfacelog` VALUES (589, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:46:59');
INSERT INTO `interfacelog` VALUES (590, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:46:59');
INSERT INTO `interfacelog` VALUES (591, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:47:02');
INSERT INTO `interfacelog` VALUES (592, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:47:05');
INSERT INTO `interfacelog` VALUES (593, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:47:13');
INSERT INTO `interfacelog` VALUES (594, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:47:16');
INSERT INTO `interfacelog` VALUES (595, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:47:18');
INSERT INTO `interfacelog` VALUES (596, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:47:23');
INSERT INTO `interfacelog` VALUES (597, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:47:42');
INSERT INTO `interfacelog` VALUES (598, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:47:44');
INSERT INTO `interfacelog` VALUES (599, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:47:44');
INSERT INTO `interfacelog` VALUES (600, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:47:45');
INSERT INTO `interfacelog` VALUES (601, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:47:49');
INSERT INTO `interfacelog` VALUES (602, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:47:49');
INSERT INTO `interfacelog` VALUES (603, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:47:54');
INSERT INTO `interfacelog` VALUES (604, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:47:59');
INSERT INTO `interfacelog` VALUES (605, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 16:49:38');
INSERT INTO `interfacelog` VALUES (606, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:49:44');
INSERT INTO `interfacelog` VALUES (607, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:49:46');
INSERT INTO `interfacelog` VALUES (608, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:49:46');
INSERT INTO `interfacelog` VALUES (609, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:49:48');
INSERT INTO `interfacelog` VALUES (610, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:49:48');
INSERT INTO `interfacelog` VALUES (611, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:49:49');
INSERT INTO `interfacelog` VALUES (612, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:49:49');
INSERT INTO `interfacelog` VALUES (613, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:49:52');
INSERT INTO `interfacelog` VALUES (614, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:49:53');
INSERT INTO `interfacelog` VALUES (615, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:49:54');
INSERT INTO `interfacelog` VALUES (616, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:49:54');
INSERT INTO `interfacelog` VALUES (617, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:52:01');
INSERT INTO `interfacelog` VALUES (618, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:52:01');
INSERT INTO `interfacelog` VALUES (619, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:52:02');
INSERT INTO `interfacelog` VALUES (620, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:52:05');
INSERT INTO `interfacelog` VALUES (621, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 16:52:06');
INSERT INTO `interfacelog` VALUES (622, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:52:09');
INSERT INTO `interfacelog` VALUES (623, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:56:03');
INSERT INTO `interfacelog` VALUES (624, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:56:04');
INSERT INTO `interfacelog` VALUES (625, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:56:05');
INSERT INTO `interfacelog` VALUES (626, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:56:05');
INSERT INTO `interfacelog` VALUES (627, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:56:06');
INSERT INTO `interfacelog` VALUES (628, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:56:06');
INSERT INTO `interfacelog` VALUES (629, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:10');
INSERT INTO `interfacelog` VALUES (630, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:10');
INSERT INTO `interfacelog` VALUES (631, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:11');
INSERT INTO `interfacelog` VALUES (632, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:11');
INSERT INTO `interfacelog` VALUES (633, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:12');
INSERT INTO `interfacelog` VALUES (634, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:12');
INSERT INTO `interfacelog` VALUES (635, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:13');
INSERT INTO `interfacelog` VALUES (636, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:13');
INSERT INTO `interfacelog` VALUES (637, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:13');
INSERT INTO `interfacelog` VALUES (638, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:14');
INSERT INTO `interfacelog` VALUES (639, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:15');
INSERT INTO `interfacelog` VALUES (640, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:15');
INSERT INTO `interfacelog` VALUES (641, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:16');
INSERT INTO `interfacelog` VALUES (642, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:16');
INSERT INTO `interfacelog` VALUES (643, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:17');
INSERT INTO `interfacelog` VALUES (644, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:17');
INSERT INTO `interfacelog` VALUES (645, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:19');
INSERT INTO `interfacelog` VALUES (646, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:23');
INSERT INTO `interfacelog` VALUES (647, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:24');
INSERT INTO `interfacelog` VALUES (648, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:25');
INSERT INTO `interfacelog` VALUES (649, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:26');
INSERT INTO `interfacelog` VALUES (650, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:26');
INSERT INTO `interfacelog` VALUES (651, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:27');
INSERT INTO `interfacelog` VALUES (652, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:28');
INSERT INTO `interfacelog` VALUES (653, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:28');
INSERT INTO `interfacelog` VALUES (654, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:29');
INSERT INTO `interfacelog` VALUES (655, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:29');
INSERT INTO `interfacelog` VALUES (656, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:31');
INSERT INTO `interfacelog` VALUES (657, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:32');
INSERT INTO `interfacelog` VALUES (658, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:32');
INSERT INTO `interfacelog` VALUES (659, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:35');
INSERT INTO `interfacelog` VALUES (660, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:35');
INSERT INTO `interfacelog` VALUES (661, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:38');
INSERT INTO `interfacelog` VALUES (662, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:38');
INSERT INTO `interfacelog` VALUES (663, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:40');
INSERT INTO `interfacelog` VALUES (664, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:40');
INSERT INTO `interfacelog` VALUES (665, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:41');
INSERT INTO `interfacelog` VALUES (666, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:41');
INSERT INTO `interfacelog` VALUES (667, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:42');
INSERT INTO `interfacelog` VALUES (668, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 16:57:42');
INSERT INTO `interfacelog` VALUES (669, 'Menu/RolePer', 1, '::1', '200', 'GET', 'Success', '2023-08-30 16:57:43');
INSERT INTO `interfacelog` VALUES (670, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:03:40');
INSERT INTO `interfacelog` VALUES (671, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:03:40');
INSERT INTO `interfacelog` VALUES (672, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 17:03:42');
INSERT INTO `interfacelog` VALUES (673, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:03:46');
INSERT INTO `interfacelog` VALUES (674, 'User/switchustate', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 17:03:46');
INSERT INTO `interfacelog` VALUES (675, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:03:49');
INSERT INTO `interfacelog` VALUES (676, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:04');
INSERT INTO `interfacelog` VALUES (677, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:04');
INSERT INTO `interfacelog` VALUES (678, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:31');
INSERT INTO `interfacelog` VALUES (679, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:31');
INSERT INTO `interfacelog` VALUES (680, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:33');
INSERT INTO `interfacelog` VALUES (681, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:33');
INSERT INTO `interfacelog` VALUES (682, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:34');
INSERT INTO `interfacelog` VALUES (683, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:34');
INSERT INTO `interfacelog` VALUES (684, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:35');
INSERT INTO `interfacelog` VALUES (685, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:35');
INSERT INTO `interfacelog` VALUES (686, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:36');
INSERT INTO `interfacelog` VALUES (687, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:36');
INSERT INTO `interfacelog` VALUES (688, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:36');
INSERT INTO `interfacelog` VALUES (689, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:36');
INSERT INTO `interfacelog` VALUES (690, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:37');
INSERT INTO `interfacelog` VALUES (691, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:37');
INSERT INTO `interfacelog` VALUES (692, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:39');
INSERT INTO `interfacelog` VALUES (693, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:39');
INSERT INTO `interfacelog` VALUES (694, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:42');
INSERT INTO `interfacelog` VALUES (695, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:42');
INSERT INTO `interfacelog` VALUES (696, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:43');
INSERT INTO `interfacelog` VALUES (697, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:43');
INSERT INTO `interfacelog` VALUES (698, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:04:44');
INSERT INTO `interfacelog` VALUES (699, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:04:44');
INSERT INTO `interfacelog` VALUES (700, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:09:20');
INSERT INTO `interfacelog` VALUES (701, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:09:20');
INSERT INTO `interfacelog` VALUES (702, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:09:22');
INSERT INTO `interfacelog` VALUES (703, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:09:24');
INSERT INTO `interfacelog` VALUES (704, 'Menu/Addsysbutton', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 17:09:38');
INSERT INTO `interfacelog` VALUES (705, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 17:10:08');
INSERT INTO `interfacelog` VALUES (706, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:10:16');
INSERT INTO `interfacelog` VALUES (707, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:10:18');
INSERT INTO `interfacelog` VALUES (708, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:10:20');
INSERT INTO `interfacelog` VALUES (709, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:10:20');
INSERT INTO `interfacelog` VALUES (710, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:10:21');
INSERT INTO `interfacelog` VALUES (711, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:10:24');
INSERT INTO `interfacelog` VALUES (712, 'Menu/Addsysbutton', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 17:10:36');
INSERT INTO `interfacelog` VALUES (713, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:10:39');
INSERT INTO `interfacelog` VALUES (714, 'Menu/Addsysbutton', 1, '::1', '200', 'POST', '新增成功', '2023-08-30 17:10:56');
INSERT INTO `interfacelog` VALUES (715, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:11:00');
INSERT INTO `interfacelog` VALUES (716, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:11:02');
INSERT INTO `interfacelog` VALUES (717, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:11:02');
INSERT INTO `interfacelog` VALUES (718, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:11:04');
INSERT INTO `interfacelog` VALUES (719, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:11:04');
INSERT INTO `interfacelog` VALUES (720, 'Menu/RolePer', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:11:05');
INSERT INTO `interfacelog` VALUES (721, 'Menu/setrolepermise', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-30 17:11:10');
INSERT INTO `interfacelog` VALUES (722, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:11:15');
INSERT INTO `interfacelog` VALUES (723, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:11:15');
INSERT INTO `interfacelog` VALUES (724, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:11:28');
INSERT INTO `interfacelog` VALUES (725, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:11:28');
INSERT INTO `interfacelog` VALUES (726, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:14:31');
INSERT INTO `interfacelog` VALUES (727, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:14:44');
INSERT INTO `interfacelog` VALUES (728, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:14:49');
INSERT INTO `interfacelog` VALUES (729, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:17:54');
INSERT INTO `interfacelog` VALUES (730, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:17:54');
INSERT INTO `interfacelog` VALUES (731, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:17:55');
INSERT INTO `interfacelog` VALUES (732, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:17:55');
INSERT INTO `interfacelog` VALUES (733, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:17:58');
INSERT INTO `interfacelog` VALUES (734, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:17:58');
INSERT INTO `interfacelog` VALUES (735, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:18:01');
INSERT INTO `interfacelog` VALUES (736, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:18:01');
INSERT INTO `interfacelog` VALUES (737, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:18:02');
INSERT INTO `interfacelog` VALUES (738, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:18:02');
INSERT INTO `interfacelog` VALUES (739, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:18:03');
INSERT INTO `interfacelog` VALUES (740, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:18:03');
INSERT INTO `interfacelog` VALUES (741, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:18:04');
INSERT INTO `interfacelog` VALUES (742, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:18:04');
INSERT INTO `interfacelog` VALUES (743, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:18:05');
INSERT INTO `interfacelog` VALUES (744, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:18:05');
INSERT INTO `interfacelog` VALUES (745, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:18:06');
INSERT INTO `interfacelog` VALUES (746, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:18:06');
INSERT INTO `interfacelog` VALUES (747, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:18:09');
INSERT INTO `interfacelog` VALUES (748, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-30 17:18:09');
INSERT INTO `interfacelog` VALUES (749, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:21:27');
INSERT INTO `interfacelog` VALUES (750, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:21:29');
INSERT INTO `interfacelog` VALUES (751, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:21:31');
INSERT INTO `interfacelog` VALUES (752, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:21:33');
INSERT INTO `interfacelog` VALUES (753, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:21:35');
INSERT INTO `interfacelog` VALUES (754, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-30 17:21:37');
INSERT INTO `interfacelog` VALUES (755, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-30 17:37:03');
INSERT INTO `interfacelog` VALUES (756, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:50:36');
INSERT INTO `interfacelog` VALUES (757, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:50:40');
INSERT INTO `interfacelog` VALUES (758, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:50:42');
INSERT INTO `interfacelog` VALUES (759, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:50:49');
INSERT INTO `interfacelog` VALUES (760, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:50:53');
INSERT INTO `interfacelog` VALUES (761, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:51:31');
INSERT INTO `interfacelog` VALUES (762, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:52:45');
INSERT INTO `interfacelog` VALUES (763, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:53:12');
INSERT INTO `interfacelog` VALUES (764, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:53:14');
INSERT INTO `interfacelog` VALUES (765, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:55:37');
INSERT INTO `interfacelog` VALUES (766, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:55:39');
INSERT INTO `interfacelog` VALUES (767, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:55:41');
INSERT INTO `interfacelog` VALUES (768, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:55:50');
INSERT INTO `interfacelog` VALUES (769, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:55:53');
INSERT INTO `interfacelog` VALUES (770, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:00');
INSERT INTO `interfacelog` VALUES (771, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:01');
INSERT INTO `interfacelog` VALUES (772, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:09');
INSERT INTO `interfacelog` VALUES (773, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:13');
INSERT INTO `interfacelog` VALUES (774, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:14');
INSERT INTO `interfacelog` VALUES (775, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:17');
INSERT INTO `interfacelog` VALUES (776, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:18');
INSERT INTO `interfacelog` VALUES (777, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:27');
INSERT INTO `interfacelog` VALUES (778, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:29');
INSERT INTO `interfacelog` VALUES (779, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:32');
INSERT INTO `interfacelog` VALUES (780, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:39');
INSERT INTO `interfacelog` VALUES (781, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:53');
INSERT INTO `interfacelog` VALUES (782, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:56:56');
INSERT INTO `interfacelog` VALUES (783, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:09');
INSERT INTO `interfacelog` VALUES (784, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:26');
INSERT INTO `interfacelog` VALUES (785, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:27');
INSERT INTO `interfacelog` VALUES (786, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:29');
INSERT INTO `interfacelog` VALUES (787, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:35');
INSERT INTO `interfacelog` VALUES (788, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:41');
INSERT INTO `interfacelog` VALUES (789, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:45');
INSERT INTO `interfacelog` VALUES (790, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:57:52');
INSERT INTO `interfacelog` VALUES (791, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:58:00');
INSERT INTO `interfacelog` VALUES (792, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:58:06');
INSERT INTO `interfacelog` VALUES (793, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:58:19');
INSERT INTO `interfacelog` VALUES (794, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:58:21');
INSERT INTO `interfacelog` VALUES (795, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:58:24');
INSERT INTO `interfacelog` VALUES (796, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:58:57');
INSERT INTO `interfacelog` VALUES (797, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:59:02');
INSERT INTO `interfacelog` VALUES (798, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 09:59:34');
INSERT INTO `interfacelog` VALUES (799, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:00:11');
INSERT INTO `interfacelog` VALUES (800, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:00:36');
INSERT INTO `interfacelog` VALUES (801, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:00:38');
INSERT INTO `interfacelog` VALUES (802, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:04:09');
INSERT INTO `interfacelog` VALUES (803, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:04:12');
INSERT INTO `interfacelog` VALUES (804, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:04:15');
INSERT INTO `interfacelog` VALUES (805, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:04:16');
INSERT INTO `interfacelog` VALUES (806, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:08:24');
INSERT INTO `interfacelog` VALUES (807, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:08:36');
INSERT INTO `interfacelog` VALUES (808, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:08:40');
INSERT INTO `interfacelog` VALUES (809, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:08:52');
INSERT INTO `interfacelog` VALUES (810, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:08:54');
INSERT INTO `interfacelog` VALUES (811, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:08:56');
INSERT INTO `interfacelog` VALUES (812, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:08:59');
INSERT INTO `interfacelog` VALUES (813, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:09:01');
INSERT INTO `interfacelog` VALUES (814, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:09:03');
INSERT INTO `interfacelog` VALUES (815, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:09:04');
INSERT INTO `interfacelog` VALUES (816, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:09:19');
INSERT INTO `interfacelog` VALUES (817, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:09:21');
INSERT INTO `interfacelog` VALUES (818, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:09:23');
INSERT INTO `interfacelog` VALUES (819, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:09:35');
INSERT INTO `interfacelog` VALUES (820, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:10:11');
INSERT INTO `interfacelog` VALUES (821, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:10:14');
INSERT INTO `interfacelog` VALUES (822, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:10:14');
INSERT INTO `interfacelog` VALUES (823, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:10:28');
INSERT INTO `interfacelog` VALUES (824, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:10:28');
INSERT INTO `interfacelog` VALUES (825, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:10:29');
INSERT INTO `interfacelog` VALUES (826, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:10:40');
INSERT INTO `interfacelog` VALUES (827, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:10:44');
INSERT INTO `interfacelog` VALUES (828, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:10:44');
INSERT INTO `interfacelog` VALUES (829, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:10:55');
INSERT INTO `interfacelog` VALUES (830, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:10:55');
INSERT INTO `interfacelog` VALUES (831, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:00');
INSERT INTO `interfacelog` VALUES (832, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:11:00');
INSERT INTO `interfacelog` VALUES (833, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:01');
INSERT INTO `interfacelog` VALUES (834, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:05');
INSERT INTO `interfacelog` VALUES (835, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:11:05');
INSERT INTO `interfacelog` VALUES (836, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:05');
INSERT INTO `interfacelog` VALUES (837, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:11:05');
INSERT INTO `interfacelog` VALUES (838, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:07');
INSERT INTO `interfacelog` VALUES (839, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:11:07');
INSERT INTO `interfacelog` VALUES (840, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:08');
INSERT INTO `interfacelog` VALUES (841, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:08');
INSERT INTO `interfacelog` VALUES (842, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:35');
INSERT INTO `interfacelog` VALUES (843, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:38');
INSERT INTO `interfacelog` VALUES (844, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:42');
INSERT INTO `interfacelog` VALUES (845, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:11:42');
INSERT INTO `interfacelog` VALUES (846, 'Role/Addrole', 1, '::1', '200', 'POST', '新增成功', '2023-08-31 10:11:47');
INSERT INTO `interfacelog` VALUES (847, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:11:50');
INSERT INTO `interfacelog` VALUES (848, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:11:50');
INSERT INTO `interfacelog` VALUES (849, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:01');
INSERT INTO `interfacelog` VALUES (850, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:05');
INSERT INTO `interfacelog` VALUES (851, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:17');
INSERT INTO `interfacelog` VALUES (852, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:20');
INSERT INTO `interfacelog` VALUES (853, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:24');
INSERT INTO `interfacelog` VALUES (854, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:34');
INSERT INTO `interfacelog` VALUES (855, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:44');
INSERT INTO `interfacelog` VALUES (856, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:45');
INSERT INTO `interfacelog` VALUES (857, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:50');
INSERT INTO `interfacelog` VALUES (858, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:52');
INSERT INTO `interfacelog` VALUES (859, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:12:54');
INSERT INTO `interfacelog` VALUES (860, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:00');
INSERT INTO `interfacelog` VALUES (861, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:14');
INSERT INTO `interfacelog` VALUES (862, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:15');
INSERT INTO `interfacelog` VALUES (863, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:20');
INSERT INTO `interfacelog` VALUES (864, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:22');
INSERT INTO `interfacelog` VALUES (865, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:33');
INSERT INTO `interfacelog` VALUES (866, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:35');
INSERT INTO `interfacelog` VALUES (867, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:48');
INSERT INTO `interfacelog` VALUES (868, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:51');
INSERT INTO `interfacelog` VALUES (869, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:13:55');
INSERT INTO `interfacelog` VALUES (870, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:00');
INSERT INTO `interfacelog` VALUES (871, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:06');
INSERT INTO `interfacelog` VALUES (872, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:10');
INSERT INTO `interfacelog` VALUES (873, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:14');
INSERT INTO `interfacelog` VALUES (874, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:17');
INSERT INTO `interfacelog` VALUES (875, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:20');
INSERT INTO `interfacelog` VALUES (876, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:21');
INSERT INTO `interfacelog` VALUES (877, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:26');
INSERT INTO `interfacelog` VALUES (878, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:14:27');
INSERT INTO `interfacelog` VALUES (879, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:15:09');
INSERT INTO `interfacelog` VALUES (880, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:15:12');
INSERT INTO `interfacelog` VALUES (881, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:15:14');
INSERT INTO `interfacelog` VALUES (882, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:15:19');
INSERT INTO `interfacelog` VALUES (883, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:15:26');
INSERT INTO `interfacelog` VALUES (884, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:15:29');
INSERT INTO `interfacelog` VALUES (885, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:15:54');
INSERT INTO `interfacelog` VALUES (886, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:16:06');
INSERT INTO `interfacelog` VALUES (887, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:16:13');
INSERT INTO `interfacelog` VALUES (888, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:16:14');
INSERT INTO `interfacelog` VALUES (889, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:16:19');
INSERT INTO `interfacelog` VALUES (890, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:16:48');
INSERT INTO `interfacelog` VALUES (891, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:17:12');
INSERT INTO `interfacelog` VALUES (892, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:17:22');
INSERT INTO `interfacelog` VALUES (893, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:17:28');
INSERT INTO `interfacelog` VALUES (894, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:17:30');
INSERT INTO `interfacelog` VALUES (895, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:17:31');
INSERT INTO `interfacelog` VALUES (896, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:17:41');
INSERT INTO `interfacelog` VALUES (897, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:17:45');
INSERT INTO `interfacelog` VALUES (898, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:18:09');
INSERT INTO `interfacelog` VALUES (899, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:18:19');
INSERT INTO `interfacelog` VALUES (900, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:18:24');
INSERT INTO `interfacelog` VALUES (901, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:18:43');
INSERT INTO `interfacelog` VALUES (902, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:18:48');
INSERT INTO `interfacelog` VALUES (903, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:18:50');
INSERT INTO `interfacelog` VALUES (904, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:18:53');
INSERT INTO `interfacelog` VALUES (905, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:19:06');
INSERT INTO `interfacelog` VALUES (906, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:19:10');
INSERT INTO `interfacelog` VALUES (907, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:19:11');
INSERT INTO `interfacelog` VALUES (908, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:19:18');
INSERT INTO `interfacelog` VALUES (909, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:19:18');
INSERT INTO `interfacelog` VALUES (910, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:20:47');
INSERT INTO `interfacelog` VALUES (911, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:20:49');
INSERT INTO `interfacelog` VALUES (912, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:20:49');
INSERT INTO `interfacelog` VALUES (913, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:21:19');
INSERT INTO `interfacelog` VALUES (914, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:21:21');
INSERT INTO `interfacelog` VALUES (915, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:21:21');
INSERT INTO `interfacelog` VALUES (916, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:21:59');
INSERT INTO `interfacelog` VALUES (917, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:22:01');
INSERT INTO `interfacelog` VALUES (918, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:22:07');
INSERT INTO `interfacelog` VALUES (919, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:22:08');
INSERT INTO `interfacelog` VALUES (920, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:22:46');
INSERT INTO `interfacelog` VALUES (921, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:22:51');
INSERT INTO `interfacelog` VALUES (922, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:23:19');
INSERT INTO `interfacelog` VALUES (923, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:23:19');
INSERT INTO `interfacelog` VALUES (924, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:03');
INSERT INTO `interfacelog` VALUES (925, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:05');
INSERT INTO `interfacelog` VALUES (926, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:20');
INSERT INTO `interfacelog` VALUES (927, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:22');
INSERT INTO `interfacelog` VALUES (928, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:37');
INSERT INTO `interfacelog` VALUES (929, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:38');
INSERT INTO `interfacelog` VALUES (930, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:41');
INSERT INTO `interfacelog` VALUES (931, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:24:51');
INSERT INTO `interfacelog` VALUES (932, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:25:43');
INSERT INTO `interfacelog` VALUES (933, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:26:09');
INSERT INTO `interfacelog` VALUES (934, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:26:11');
INSERT INTO `interfacelog` VALUES (935, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:26:11');
INSERT INTO `interfacelog` VALUES (936, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:26:15');
INSERT INTO `interfacelog` VALUES (937, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:26:15');
INSERT INTO `interfacelog` VALUES (938, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:26:16');
INSERT INTO `interfacelog` VALUES (939, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:26:16');
INSERT INTO `interfacelog` VALUES (940, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:26:30');
INSERT INTO `interfacelog` VALUES (941, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:28:03');
INSERT INTO `interfacelog` VALUES (942, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:28:18');
INSERT INTO `interfacelog` VALUES (943, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:28:43');
INSERT INTO `interfacelog` VALUES (944, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:28:46');
INSERT INTO `interfacelog` VALUES (945, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:28:46');
INSERT INTO `interfacelog` VALUES (946, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:17');
INSERT INTO `interfacelog` VALUES (947, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:18');
INSERT INTO `interfacelog` VALUES (948, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:26');
INSERT INTO `interfacelog` VALUES (949, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:28');
INSERT INTO `interfacelog` VALUES (950, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:30:28');
INSERT INTO `interfacelog` VALUES (951, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:52');
INSERT INTO `interfacelog` VALUES (952, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:55');
INSERT INTO `interfacelog` VALUES (953, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:56');
INSERT INTO `interfacelog` VALUES (954, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:30:59');
INSERT INTO `interfacelog` VALUES (955, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:31:02');
INSERT INTO `interfacelog` VALUES (956, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:31:02');
INSERT INTO `interfacelog` VALUES (957, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:31:45');
INSERT INTO `interfacelog` VALUES (958, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:31:46');
INSERT INTO `interfacelog` VALUES (959, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:31:52');
INSERT INTO `interfacelog` VALUES (960, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:31:55');
INSERT INTO `interfacelog` VALUES (961, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:31:55');
INSERT INTO `interfacelog` VALUES (962, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:32:09');
INSERT INTO `interfacelog` VALUES (963, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:32:12');
INSERT INTO `interfacelog` VALUES (964, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:32:27');
INSERT INTO `interfacelog` VALUES (965, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:32:27');
INSERT INTO `interfacelog` VALUES (966, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:33:35');
INSERT INTO `interfacelog` VALUES (967, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:33:35');
INSERT INTO `interfacelog` VALUES (968, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:34:01');
INSERT INTO `interfacelog` VALUES (969, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:34:03');
INSERT INTO `interfacelog` VALUES (970, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:34:03');
INSERT INTO `interfacelog` VALUES (971, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:34:17');
INSERT INTO `interfacelog` VALUES (972, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:34:18');
INSERT INTO `interfacelog` VALUES (973, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:35:20');
INSERT INTO `interfacelog` VALUES (974, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:35:23');
INSERT INTO `interfacelog` VALUES (975, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:35:24');
INSERT INTO `interfacelog` VALUES (976, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:35:26');
INSERT INTO `interfacelog` VALUES (977, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:05');
INSERT INTO `interfacelog` VALUES (978, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:09');
INSERT INTO `interfacelog` VALUES (979, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:10');
INSERT INTO `interfacelog` VALUES (980, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:15');
INSERT INTO `interfacelog` VALUES (981, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:16');
INSERT INTO `interfacelog` VALUES (982, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:18');
INSERT INTO `interfacelog` VALUES (983, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:21');
INSERT INTO `interfacelog` VALUES (984, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:22');
INSERT INTO `interfacelog` VALUES (985, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:36:43');
INSERT INTO `interfacelog` VALUES (986, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:36:43');
INSERT INTO `interfacelog` VALUES (987, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:38:10');
INSERT INTO `interfacelog` VALUES (988, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:38:12');
INSERT INTO `interfacelog` VALUES (989, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:38:12');
INSERT INTO `interfacelog` VALUES (990, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:38:21');
INSERT INTO `interfacelog` VALUES (991, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:38:24');
INSERT INTO `interfacelog` VALUES (992, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:38:24');
INSERT INTO `interfacelog` VALUES (993, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:40:58');
INSERT INTO `interfacelog` VALUES (994, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:41:01');
INSERT INTO `interfacelog` VALUES (995, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:41:02');
INSERT INTO `interfacelog` VALUES (996, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:41:06');
INSERT INTO `interfacelog` VALUES (997, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:41:06');
INSERT INTO `interfacelog` VALUES (998, 'Role/Addrole', 1, '::1', '200', 'POST', '新增成功', '2023-08-31 10:41:20');
INSERT INTO `interfacelog` VALUES (999, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:41:23');
INSERT INTO `interfacelog` VALUES (1000, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:41:23');
INSERT INTO `interfacelog` VALUES (1001, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:10');
INSERT INTO `interfacelog` VALUES (1002, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:16');
INSERT INTO `interfacelog` VALUES (1003, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:19');
INSERT INTO `interfacelog` VALUES (1004, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:22');
INSERT INTO `interfacelog` VALUES (1005, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:25');
INSERT INTO `interfacelog` VALUES (1006, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:42:25');
INSERT INTO `interfacelog` VALUES (1007, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:28');
INSERT INTO `interfacelog` VALUES (1008, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:42:28');
INSERT INTO `interfacelog` VALUES (1009, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:30');
INSERT INTO `interfacelog` VALUES (1010, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:42:30');
INSERT INTO `interfacelog` VALUES (1011, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:44');
INSERT INTO `interfacelog` VALUES (1012, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:45');
INSERT INTO `interfacelog` VALUES (1013, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:46');
INSERT INTO `interfacelog` VALUES (1014, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:53');
INSERT INTO `interfacelog` VALUES (1015, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:55');
INSERT INTO `interfacelog` VALUES (1016, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:56');
INSERT INTO `interfacelog` VALUES (1017, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:42:58');
INSERT INTO `interfacelog` VALUES (1018, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:43:00');
INSERT INTO `interfacelog` VALUES (1019, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:43:00');
INSERT INTO `interfacelog` VALUES (1020, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:45:23');
INSERT INTO `interfacelog` VALUES (1021, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:45:27');
INSERT INTO `interfacelog` VALUES (1022, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:45:28');
INSERT INTO `interfacelog` VALUES (1023, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:45:34');
INSERT INTO `interfacelog` VALUES (1024, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:45:34');
INSERT INTO `interfacelog` VALUES (1025, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:46:21');
INSERT INTO `interfacelog` VALUES (1026, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:46:23');
INSERT INTO `interfacelog` VALUES (1027, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:46:25');
INSERT INTO `interfacelog` VALUES (1028, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:46:30');
INSERT INTO `interfacelog` VALUES (1029, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:46:30');
INSERT INTO `interfacelog` VALUES (1030, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:48:28');
INSERT INTO `interfacelog` VALUES (1031, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:48:30');
INSERT INTO `interfacelog` VALUES (1032, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:48:34');
INSERT INTO `interfacelog` VALUES (1033, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:48:36');
INSERT INTO `interfacelog` VALUES (1034, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:48:39');
INSERT INTO `interfacelog` VALUES (1035, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:48:56');
INSERT INTO `interfacelog` VALUES (1036, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:48:58');
INSERT INTO `interfacelog` VALUES (1037, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:08');
INSERT INTO `interfacelog` VALUES (1038, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:23');
INSERT INTO `interfacelog` VALUES (1039, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:32');
INSERT INTO `interfacelog` VALUES (1040, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:35');
INSERT INTO `interfacelog` VALUES (1041, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:40');
INSERT INTO `interfacelog` VALUES (1042, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:42');
INSERT INTO `interfacelog` VALUES (1043, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:43');
INSERT INTO `interfacelog` VALUES (1044, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:49:47');
INSERT INTO `interfacelog` VALUES (1045, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:49:47');
INSERT INTO `interfacelog` VALUES (1046, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:02');
INSERT INTO `interfacelog` VALUES (1047, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:02');
INSERT INTO `interfacelog` VALUES (1048, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:11');
INSERT INTO `interfacelog` VALUES (1049, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:37');
INSERT INTO `interfacelog` VALUES (1050, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:43');
INSERT INTO `interfacelog` VALUES (1051, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:44');
INSERT INTO `interfacelog` VALUES (1052, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:47');
INSERT INTO `interfacelog` VALUES (1053, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:50');
INSERT INTO `interfacelog` VALUES (1054, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:53');
INSERT INTO `interfacelog` VALUES (1055, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:51:54');
INSERT INTO `interfacelog` VALUES (1056, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 10:52:01');
INSERT INTO `interfacelog` VALUES (1057, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:52:08');
INSERT INTO `interfacelog` VALUES (1058, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:52:12');
INSERT INTO `interfacelog` VALUES (1059, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:52:12');
INSERT INTO `interfacelog` VALUES (1060, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:54:28');
INSERT INTO `interfacelog` VALUES (1061, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:54:30');
INSERT INTO `interfacelog` VALUES (1062, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:54:32');
INSERT INTO `interfacelog` VALUES (1063, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:54:32');
INSERT INTO `interfacelog` VALUES (1064, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:54:51');
INSERT INTO `interfacelog` VALUES (1065, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:54:54');
INSERT INTO `interfacelog` VALUES (1066, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:54:57');
INSERT INTO `interfacelog` VALUES (1067, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:54:57');
INSERT INTO `interfacelog` VALUES (1068, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:55:49');
INSERT INTO `interfacelog` VALUES (1069, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:55:55');
INSERT INTO `interfacelog` VALUES (1070, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:08');
INSERT INTO `interfacelog` VALUES (1071, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:11');
INSERT INTO `interfacelog` VALUES (1072, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:18');
INSERT INTO `interfacelog` VALUES (1073, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:23');
INSERT INTO `interfacelog` VALUES (1074, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:56:23');
INSERT INTO `interfacelog` VALUES (1075, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:40');
INSERT INTO `interfacelog` VALUES (1076, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:41');
INSERT INTO `interfacelog` VALUES (1077, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:45');
INSERT INTO `interfacelog` VALUES (1078, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:56:45');
INSERT INTO `interfacelog` VALUES (1079, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:45');
INSERT INTO `interfacelog` VALUES (1080, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:56:45');
INSERT INTO `interfacelog` VALUES (1081, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:58');
INSERT INTO `interfacelog` VALUES (1082, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:56:59');
INSERT INTO `interfacelog` VALUES (1083, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:57:06');
INSERT INTO `interfacelog` VALUES (1084, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:57:07');
INSERT INTO `interfacelog` VALUES (1085, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:57:56');
INSERT INTO `interfacelog` VALUES (1086, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:57:56');
INSERT INTO `interfacelog` VALUES (1087, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:59:37');
INSERT INTO `interfacelog` VALUES (1088, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:59:39');
INSERT INTO `interfacelog` VALUES (1089, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:59:45');
INSERT INTO `interfacelog` VALUES (1090, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:59:47');
INSERT INTO `interfacelog` VALUES (1091, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:59:50');
INSERT INTO `interfacelog` VALUES (1092, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:59:50');
INSERT INTO `interfacelog` VALUES (1093, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 10:59:50');
INSERT INTO `interfacelog` VALUES (1094, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 10:59:50');
INSERT INTO `interfacelog` VALUES (1095, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:27');
INSERT INTO `interfacelog` VALUES (1096, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:28');
INSERT INTO `interfacelog` VALUES (1097, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:33');
INSERT INTO `interfacelog` VALUES (1098, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:35');
INSERT INTO `interfacelog` VALUES (1099, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:39');
INSERT INTO `interfacelog` VALUES (1100, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:41');
INSERT INTO `interfacelog` VALUES (1101, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:43');
INSERT INTO `interfacelog` VALUES (1102, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:45');
INSERT INTO `interfacelog` VALUES (1103, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:00:45');
INSERT INTO `interfacelog` VALUES (1104, 'Role/Addrole', 1, '::1', '200', 'POST', '新增成功', '2023-08-31 11:00:50');
INSERT INTO `interfacelog` VALUES (1105, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:00:54');
INSERT INTO `interfacelog` VALUES (1106, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:00:54');
INSERT INTO `interfacelog` VALUES (1107, 'Menu/getbtninfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:00');
INSERT INTO `interfacelog` VALUES (1108, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:09');
INSERT INTO `interfacelog` VALUES (1109, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:01:09');
INSERT INTO `interfacelog` VALUES (1110, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:12');
INSERT INTO `interfacelog` VALUES (1111, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:01:12');
INSERT INTO `interfacelog` VALUES (1112, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:14');
INSERT INTO `interfacelog` VALUES (1113, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:29');
INSERT INTO `interfacelog` VALUES (1114, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:32');
INSERT INTO `interfacelog` VALUES (1115, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:01:32');
INSERT INTO `interfacelog` VALUES (1116, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:47');
INSERT INTO `interfacelog` VALUES (1117, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:01:49');
INSERT INTO `interfacelog` VALUES (1118, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:01:49');
INSERT INTO `interfacelog` VALUES (1119, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:04:00');
INSERT INTO `interfacelog` VALUES (1120, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:04:03');
INSERT INTO `interfacelog` VALUES (1121, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:04:03');
INSERT INTO `interfacelog` VALUES (1122, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:05:29');
INSERT INTO `interfacelog` VALUES (1123, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:05:34');
INSERT INTO `interfacelog` VALUES (1124, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:05:52');
INSERT INTO `interfacelog` VALUES (1125, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:06:27');
INSERT INTO `interfacelog` VALUES (1126, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:06:29');
INSERT INTO `interfacelog` VALUES (1127, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:06:32');
INSERT INTO `interfacelog` VALUES (1128, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:06:33');
INSERT INTO `interfacelog` VALUES (1129, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:06:46');
INSERT INTO `interfacelog` VALUES (1130, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:06:50');
INSERT INTO `interfacelog` VALUES (1131, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:06:50');
INSERT INTO `interfacelog` VALUES (1132, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:07:14');
INSERT INTO `interfacelog` VALUES (1133, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:07:16');
INSERT INTO `interfacelog` VALUES (1134, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:07:18');
INSERT INTO `interfacelog` VALUES (1135, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:07:20');
INSERT INTO `interfacelog` VALUES (1136, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:07:20');
INSERT INTO `interfacelog` VALUES (1137, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:08:31');
INSERT INTO `interfacelog` VALUES (1138, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:08:36');
INSERT INTO `interfacelog` VALUES (1139, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:08:37');
INSERT INTO `interfacelog` VALUES (1140, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 11:08:40');
INSERT INTO `interfacelog` VALUES (1141, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 11:08:40');
INSERT INTO `interfacelog` VALUES (1142, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:04:47');
INSERT INTO `interfacelog` VALUES (1143, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:04:49');
INSERT INTO `interfacelog` VALUES (1144, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:04:59');
INSERT INTO `interfacelog` VALUES (1145, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:05:11');
INSERT INTO `interfacelog` VALUES (1146, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:05:20');
INSERT INTO `interfacelog` VALUES (1147, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:05:21');
INSERT INTO `interfacelog` VALUES (1148, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:05:23');
INSERT INTO `interfacelog` VALUES (1149, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:05:24');
INSERT INTO `interfacelog` VALUES (1150, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:05:40');
INSERT INTO `interfacelog` VALUES (1151, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:05:50');
INSERT INTO `interfacelog` VALUES (1152, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:06:03');
INSERT INTO `interfacelog` VALUES (1153, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:06:10');
INSERT INTO `interfacelog` VALUES (1154, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:06:17');
INSERT INTO `interfacelog` VALUES (1155, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:06:18');
INSERT INTO `interfacelog` VALUES (1156, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:07:05');
INSERT INTO `interfacelog` VALUES (1157, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:07:08');
INSERT INTO `interfacelog` VALUES (1158, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:07:16');
INSERT INTO `interfacelog` VALUES (1159, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:07:19');
INSERT INTO `interfacelog` VALUES (1160, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:07:29');
INSERT INTO `interfacelog` VALUES (1161, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:08:27');
INSERT INTO `interfacelog` VALUES (1162, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:08:32');
INSERT INTO `interfacelog` VALUES (1163, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:08:33');
INSERT INTO `interfacelog` VALUES (1164, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:08:35');
INSERT INTO `interfacelog` VALUES (1165, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:08:47');
INSERT INTO `interfacelog` VALUES (1166, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:08:50');
INSERT INTO `interfacelog` VALUES (1167, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:08:55');
INSERT INTO `interfacelog` VALUES (1168, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:09:24');
INSERT INTO `interfacelog` VALUES (1169, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:09:27');
INSERT INTO `interfacelog` VALUES (1170, 'User/interfacelog', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:09:27');
INSERT INTO `interfacelog` VALUES (1171, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:09:29');
INSERT INTO `interfacelog` VALUES (1172, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:09:29');
INSERT INTO `interfacelog` VALUES (1173, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:09:33');
INSERT INTO `interfacelog` VALUES (1174, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:09:33');
INSERT INTO `interfacelog` VALUES (1175, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:10:09');
INSERT INTO `interfacelog` VALUES (1176, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:10:11');
INSERT INTO `interfacelog` VALUES (1177, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:10:13');
INSERT INTO `interfacelog` VALUES (1178, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:10:13');
INSERT INTO `interfacelog` VALUES (1179, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:11:02');
INSERT INTO `interfacelog` VALUES (1180, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:11:04');
INSERT INTO `interfacelog` VALUES (1181, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:11:07');
INSERT INTO `interfacelog` VALUES (1182, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:11:11');
INSERT INTO `interfacelog` VALUES (1183, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:11:11');
INSERT INTO `interfacelog` VALUES (1184, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:11:11');
INSERT INTO `interfacelog` VALUES (1185, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:11:22');
INSERT INTO `interfacelog` VALUES (1186, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:12:01');
INSERT INTO `interfacelog` VALUES (1187, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:12:02');
INSERT INTO `interfacelog` VALUES (1188, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:12:04');
INSERT INTO `interfacelog` VALUES (1189, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:12:04');
INSERT INTO `interfacelog` VALUES (1190, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:12:06');
INSERT INTO `interfacelog` VALUES (1191, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:12:30');
INSERT INTO `interfacelog` VALUES (1192, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:12:30');
INSERT INTO `interfacelog` VALUES (1193, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:12:32');
INSERT INTO `interfacelog` VALUES (1194, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:12:32');
INSERT INTO `interfacelog` VALUES (1195, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:13:38');
INSERT INTO `interfacelog` VALUES (1196, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:13:43');
INSERT INTO `interfacelog` VALUES (1197, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:13:43');
INSERT INTO `interfacelog` VALUES (1198, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:13:44');
INSERT INTO `interfacelog` VALUES (1199, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:14:19');
INSERT INTO `interfacelog` VALUES (1200, 'Menu/Addmenu', 1, '::1', '200', 'POST', '新增成功', '2023-08-31 12:14:32');
INSERT INTO `interfacelog` VALUES (1201, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:14:35');
INSERT INTO `interfacelog` VALUES (1202, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:14:35');
INSERT INTO `interfacelog` VALUES (1203, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:15:10');
INSERT INTO `interfacelog` VALUES (1204, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:15:10');
INSERT INTO `interfacelog` VALUES (1205, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:15:12');
INSERT INTO `interfacelog` VALUES (1206, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:15:12');
INSERT INTO `interfacelog` VALUES (1207, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:18:15');
INSERT INTO `interfacelog` VALUES (1208, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:18:15');
INSERT INTO `interfacelog` VALUES (1209, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:18:19');
INSERT INTO `interfacelog` VALUES (1210, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:18:19');
INSERT INTO `interfacelog` VALUES (1211, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:18:29');
INSERT INTO `interfacelog` VALUES (1212, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:18:32');
INSERT INTO `interfacelog` VALUES (1213, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:18:33');
INSERT INTO `interfacelog` VALUES (1214, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:18:36');
INSERT INTO `interfacelog` VALUES (1215, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:18:36');
INSERT INTO `interfacelog` VALUES (1216, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:19:24');
INSERT INTO `interfacelog` VALUES (1217, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:19:25');
INSERT INTO `interfacelog` VALUES (1218, 'Menu/Addmenu', 1, '::1', '200', 'POST', '新增成功', '2023-08-31 12:19:37');
INSERT INTO `interfacelog` VALUES (1219, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:19:40');
INSERT INTO `interfacelog` VALUES (1220, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:19:40');
INSERT INTO `interfacelog` VALUES (1221, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:19:42');
INSERT INTO `interfacelog` VALUES (1222, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:19:46');
INSERT INTO `interfacelog` VALUES (1223, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:19:50');
INSERT INTO `interfacelog` VALUES (1224, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:19:50');
INSERT INTO `interfacelog` VALUES (1225, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:19:54');
INSERT INTO `interfacelog` VALUES (1226, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:19:57');
INSERT INTO `interfacelog` VALUES (1227, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:19:57');
INSERT INTO `interfacelog` VALUES (1228, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:19:58');
INSERT INTO `interfacelog` VALUES (1229, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:03');
INSERT INTO `interfacelog` VALUES (1230, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:03');
INSERT INTO `interfacelog` VALUES (1231, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:20:06');
INSERT INTO `interfacelog` VALUES (1232, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:09');
INSERT INTO `interfacelog` VALUES (1233, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:09');
INSERT INTO `interfacelog` VALUES (1234, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:11');
INSERT INTO `interfacelog` VALUES (1235, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:15');
INSERT INTO `interfacelog` VALUES (1236, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:15');
INSERT INTO `interfacelog` VALUES (1237, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:20:17');
INSERT INTO `interfacelog` VALUES (1238, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:20');
INSERT INTO `interfacelog` VALUES (1239, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:21');
INSERT INTO `interfacelog` VALUES (1240, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:22');
INSERT INTO `interfacelog` VALUES (1241, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:25');
INSERT INTO `interfacelog` VALUES (1242, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:25');
INSERT INTO `interfacelog` VALUES (1243, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:20:29');
INSERT INTO `interfacelog` VALUES (1244, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:32');
INSERT INTO `interfacelog` VALUES (1245, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:32');
INSERT INTO `interfacelog` VALUES (1246, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:34');
INSERT INTO `interfacelog` VALUES (1247, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:40');
INSERT INTO `interfacelog` VALUES (1248, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:43');
INSERT INTO `interfacelog` VALUES (1249, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:43');
INSERT INTO `interfacelog` VALUES (1250, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:20:47');
INSERT INTO `interfacelog` VALUES (1251, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:50');
INSERT INTO `interfacelog` VALUES (1252, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:50');
INSERT INTO `interfacelog` VALUES (1253, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:52');
INSERT INTO `interfacelog` VALUES (1254, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:52');
INSERT INTO `interfacelog` VALUES (1255, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:20:56');
INSERT INTO `interfacelog` VALUES (1256, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:20:59');
INSERT INTO `interfacelog` VALUES (1257, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:20:59');
INSERT INTO `interfacelog` VALUES (1258, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:00');
INSERT INTO `interfacelog` VALUES (1259, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:00');
INSERT INTO `interfacelog` VALUES (1260, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:01');
INSERT INTO `interfacelog` VALUES (1261, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:01');
INSERT INTO `interfacelog` VALUES (1262, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:06');
INSERT INTO `interfacelog` VALUES (1263, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:09');
INSERT INTO `interfacelog` VALUES (1264, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:12');
INSERT INTO `interfacelog` VALUES (1265, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:13');
INSERT INTO `interfacelog` VALUES (1266, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:14');
INSERT INTO `interfacelog` VALUES (1267, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:14');
INSERT INTO `interfacelog` VALUES (1268, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:14');
INSERT INTO `interfacelog` VALUES (1269, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:14');
INSERT INTO `interfacelog` VALUES (1270, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:16');
INSERT INTO `interfacelog` VALUES (1271, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:25');
INSERT INTO `interfacelog` VALUES (1272, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:25');
INSERT INTO `interfacelog` VALUES (1273, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:21:33');
INSERT INTO `interfacelog` VALUES (1274, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:34');
INSERT INTO `interfacelog` VALUES (1275, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:21:34');
INSERT INTO `interfacelog` VALUES (1276, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:23:21');
INSERT INTO `interfacelog` VALUES (1277, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:23:21');
INSERT INTO `interfacelog` VALUES (1278, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:23:27');
INSERT INTO `interfacelog` VALUES (1279, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:23:27');
INSERT INTO `interfacelog` VALUES (1280, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:23:32');
INSERT INTO `interfacelog` VALUES (1281, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:23:32');
INSERT INTO `interfacelog` VALUES (1282, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:24:34');
INSERT INTO `interfacelog` VALUES (1283, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:24:36');
INSERT INTO `interfacelog` VALUES (1284, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:24:36');
INSERT INTO `interfacelog` VALUES (1285, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:24:38');
INSERT INTO `interfacelog` VALUES (1286, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:24:53');
INSERT INTO `interfacelog` VALUES (1287, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:24:55');
INSERT INTO `interfacelog` VALUES (1288, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:15');
INSERT INTO `interfacelog` VALUES (1289, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:16');
INSERT INTO `interfacelog` VALUES (1290, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:21');
INSERT INTO `interfacelog` VALUES (1291, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:25:21');
INSERT INTO `interfacelog` VALUES (1292, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:25:23');
INSERT INTO `interfacelog` VALUES (1293, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:24');
INSERT INTO `interfacelog` VALUES (1294, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:24');
INSERT INTO `interfacelog` VALUES (1295, 'Menu/Addmenu', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 12:25:31');
INSERT INTO `interfacelog` VALUES (1296, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:35');
INSERT INTO `interfacelog` VALUES (1297, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:25:35');
INSERT INTO `interfacelog` VALUES (1298, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:25:35');
INSERT INTO `interfacelog` VALUES (1299, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:36');
INSERT INTO `interfacelog` VALUES (1300, 'Menu/getmenuinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:36');
INSERT INTO `interfacelog` VALUES (1301, 'Menu/Treeselect', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:25:42');
INSERT INTO `interfacelog` VALUES (1302, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:27:54');
INSERT INTO `interfacelog` VALUES (1303, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:27:54');
INSERT INTO `interfacelog` VALUES (1304, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:27:57');
INSERT INTO `interfacelog` VALUES (1305, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:27:57');
INSERT INTO `interfacelog` VALUES (1306, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:27:58');
INSERT INTO `interfacelog` VALUES (1307, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:28:05');
INSERT INTO `interfacelog` VALUES (1308, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:28:16');
INSERT INTO `interfacelog` VALUES (1309, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:28:40');
INSERT INTO `interfacelog` VALUES (1310, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:28:45');
INSERT INTO `interfacelog` VALUES (1311, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:47:52');
INSERT INTO `interfacelog` VALUES (1312, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:50:10');
INSERT INTO `interfacelog` VALUES (1313, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 12:50:11');
INSERT INTO `interfacelog` VALUES (1314, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:50:20');
INSERT INTO `interfacelog` VALUES (1315, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:50:58');
INSERT INTO `interfacelog` VALUES (1316, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:50:58');
INSERT INTO `interfacelog` VALUES (1317, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:01');
INSERT INTO `interfacelog` VALUES (1318, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:01');
INSERT INTO `interfacelog` VALUES (1319, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:10');
INSERT INTO `interfacelog` VALUES (1320, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:11');
INSERT INTO `interfacelog` VALUES (1321, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:12');
INSERT INTO `interfacelog` VALUES (1322, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:12');
INSERT INTO `interfacelog` VALUES (1323, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:18');
INSERT INTO `interfacelog` VALUES (1324, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:18');
INSERT INTO `interfacelog` VALUES (1325, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:27');
INSERT INTO `interfacelog` VALUES (1326, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:28');
INSERT INTO `interfacelog` VALUES (1327, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:31');
INSERT INTO `interfacelog` VALUES (1328, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:31');
INSERT INTO `interfacelog` VALUES (1329, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:31');
INSERT INTO `interfacelog` VALUES (1330, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:34');
INSERT INTO `interfacelog` VALUES (1331, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:34');
INSERT INTO `interfacelog` VALUES (1332, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:51:40');
INSERT INTO `interfacelog` VALUES (1333, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:51:40');
INSERT INTO `interfacelog` VALUES (1334, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:54:41');
INSERT INTO `interfacelog` VALUES (1335, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:54:41');
INSERT INTO `interfacelog` VALUES (1336, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:54:50');
INSERT INTO `interfacelog` VALUES (1337, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:54:54');
INSERT INTO `interfacelog` VALUES (1338, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:54:54');
INSERT INTO `interfacelog` VALUES (1339, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 12:56:29');
INSERT INTO `interfacelog` VALUES (1340, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 12:56:29');
INSERT INTO `interfacelog` VALUES (1341, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:12:59');
INSERT INTO `interfacelog` VALUES (1342, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:15:31');
INSERT INTO `interfacelog` VALUES (1343, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:15:41');
INSERT INTO `interfacelog` VALUES (1344, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:15:44');
INSERT INTO `interfacelog` VALUES (1345, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:15:50');
INSERT INTO `interfacelog` VALUES (1346, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:02');
INSERT INTO `interfacelog` VALUES (1347, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:06');
INSERT INTO `interfacelog` VALUES (1348, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:14');
INSERT INTO `interfacelog` VALUES (1349, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:17');
INSERT INTO `interfacelog` VALUES (1350, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:16:17');
INSERT INTO `interfacelog` VALUES (1351, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:18');
INSERT INTO `interfacelog` VALUES (1352, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:16:18');
INSERT INTO `interfacelog` VALUES (1353, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:19');
INSERT INTO `interfacelog` VALUES (1354, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:16:19');
INSERT INTO `interfacelog` VALUES (1355, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:20');
INSERT INTO `interfacelog` VALUES (1356, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:16:20');
INSERT INTO `interfacelog` VALUES (1357, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:21');
INSERT INTO `interfacelog` VALUES (1358, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:16:21');
INSERT INTO `interfacelog` VALUES (1359, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:22');
INSERT INTO `interfacelog` VALUES (1360, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:16:22');
INSERT INTO `interfacelog` VALUES (1361, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:33');
INSERT INTO `interfacelog` VALUES (1362, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:35');
INSERT INTO `interfacelog` VALUES (1363, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:38');
INSERT INTO `interfacelog` VALUES (1364, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:16:41');
INSERT INTO `interfacelog` VALUES (1365, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:16:41');
INSERT INTO `interfacelog` VALUES (1366, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:17:09');
INSERT INTO `interfacelog` VALUES (1367, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:17:11');
INSERT INTO `interfacelog` VALUES (1368, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:17:13');
INSERT INTO `interfacelog` VALUES (1369, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:17:15');
INSERT INTO `interfacelog` VALUES (1370, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:17:15');
INSERT INTO `interfacelog` VALUES (1371, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:17:16');
INSERT INTO `interfacelog` VALUES (1372, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:17:17');
INSERT INTO `interfacelog` VALUES (1373, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:17:57');
INSERT INTO `interfacelog` VALUES (1374, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:17:59');
INSERT INTO `interfacelog` VALUES (1375, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:17:59');
INSERT INTO `interfacelog` VALUES (1376, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:18:00');
INSERT INTO `interfacelog` VALUES (1377, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:18:00');
INSERT INTO `interfacelog` VALUES (1378, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:18:20');
INSERT INTO `interfacelog` VALUES (1379, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:18:31');
INSERT INTO `interfacelog` VALUES (1380, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:18:34');
INSERT INTO `interfacelog` VALUES (1381, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:18:34');
INSERT INTO `interfacelog` VALUES (1382, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:18:38');
INSERT INTO `interfacelog` VALUES (1383, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:18:41');
INSERT INTO `interfacelog` VALUES (1384, 'Menu/menulist', 0, '::1', '200', 'POST', 'Success', '2023-08-31 13:44:05');
INSERT INTO `interfacelog` VALUES (1385, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 13:44:08');
INSERT INTO `interfacelog` VALUES (1386, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:44:14');
INSERT INTO `interfacelog` VALUES (1387, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:44:16');
INSERT INTO `interfacelog` VALUES (1388, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:44:16');
INSERT INTO `interfacelog` VALUES (1389, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:44:39');
INSERT INTO `interfacelog` VALUES (1390, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:01');
INSERT INTO `interfacelog` VALUES (1391, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:02');
INSERT INTO `interfacelog` VALUES (1392, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:04');
INSERT INTO `interfacelog` VALUES (1393, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:04');
INSERT INTO `interfacelog` VALUES (1394, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:12');
INSERT INTO `interfacelog` VALUES (1395, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:12');
INSERT INTO `interfacelog` VALUES (1396, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:15');
INSERT INTO `interfacelog` VALUES (1397, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:15');
INSERT INTO `interfacelog` VALUES (1398, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:16');
INSERT INTO `interfacelog` VALUES (1399, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:16');
INSERT INTO `interfacelog` VALUES (1400, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:16');
INSERT INTO `interfacelog` VALUES (1401, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:16');
INSERT INTO `interfacelog` VALUES (1402, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:17');
INSERT INTO `interfacelog` VALUES (1403, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:17');
INSERT INTO `interfacelog` VALUES (1404, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:21');
INSERT INTO `interfacelog` VALUES (1405, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:21');
INSERT INTO `interfacelog` VALUES (1406, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:22');
INSERT INTO `interfacelog` VALUES (1407, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:22');
INSERT INTO `interfacelog` VALUES (1408, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:23');
INSERT INTO `interfacelog` VALUES (1409, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:23');
INSERT INTO `interfacelog` VALUES (1410, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:45:23');
INSERT INTO `interfacelog` VALUES (1411, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:45:23');
INSERT INTO `interfacelog` VALUES (1412, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:46:45');
INSERT INTO `interfacelog` VALUES (1413, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:46:45');
INSERT INTO `interfacelog` VALUES (1414, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:48:23');
INSERT INTO `interfacelog` VALUES (1415, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:48:26');
INSERT INTO `interfacelog` VALUES (1416, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:48:29');
INSERT INTO `interfacelog` VALUES (1417, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:48:55');
INSERT INTO `interfacelog` VALUES (1418, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:48:55');
INSERT INTO `interfacelog` VALUES (1419, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:48:56');
INSERT INTO `interfacelog` VALUES (1420, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:48:56');
INSERT INTO `interfacelog` VALUES (1421, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:48:58');
INSERT INTO `interfacelog` VALUES (1422, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:48:58');
INSERT INTO `interfacelog` VALUES (1423, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:48:59');
INSERT INTO `interfacelog` VALUES (1424, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:48:59');
INSERT INTO `interfacelog` VALUES (1425, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:03');
INSERT INTO `interfacelog` VALUES (1426, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:07');
INSERT INTO `interfacelog` VALUES (1427, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:09');
INSERT INTO `interfacelog` VALUES (1428, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:50:09');
INSERT INTO `interfacelog` VALUES (1429, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:13');
INSERT INTO `interfacelog` VALUES (1430, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:50:13');
INSERT INTO `interfacelog` VALUES (1431, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:14');
INSERT INTO `interfacelog` VALUES (1432, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:50:14');
INSERT INTO `interfacelog` VALUES (1433, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:21');
INSERT INTO `interfacelog` VALUES (1434, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:29');
INSERT INTO `interfacelog` VALUES (1435, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:50:29');
INSERT INTO `interfacelog` VALUES (1436, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:33');
INSERT INTO `interfacelog` VALUES (1437, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:50:33');
INSERT INTO `interfacelog` VALUES (1438, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:50:33');
INSERT INTO `interfacelog` VALUES (1439, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:50:34');
INSERT INTO `interfacelog` VALUES (1440, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:03');
INSERT INTO `interfacelog` VALUES (1441, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:06');
INSERT INTO `interfacelog` VALUES (1442, 'User/logininfo', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:06');
INSERT INTO `interfacelog` VALUES (1443, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:08');
INSERT INTO `interfacelog` VALUES (1444, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:08');
INSERT INTO `interfacelog` VALUES (1445, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:15');
INSERT INTO `interfacelog` VALUES (1446, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:16');
INSERT INTO `interfacelog` VALUES (1447, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:16');
INSERT INTO `interfacelog` VALUES (1448, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:16');
INSERT INTO `interfacelog` VALUES (1449, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:17');
INSERT INTO `interfacelog` VALUES (1450, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:17');
INSERT INTO `interfacelog` VALUES (1451, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:18');
INSERT INTO `interfacelog` VALUES (1452, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:18');
INSERT INTO `interfacelog` VALUES (1453, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:30');
INSERT INTO `interfacelog` VALUES (1454, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:30');
INSERT INTO `interfacelog` VALUES (1455, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:51:31');
INSERT INTO `interfacelog` VALUES (1456, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:51:31');
INSERT INTO `interfacelog` VALUES (1457, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:16');
INSERT INTO `interfacelog` VALUES (1458, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:26');
INSERT INTO `interfacelog` VALUES (1459, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:29');
INSERT INTO `interfacelog` VALUES (1460, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:30');
INSERT INTO `interfacelog` VALUES (1461, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:34');
INSERT INTO `interfacelog` VALUES (1462, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:36');
INSERT INTO `interfacelog` VALUES (1463, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:37');
INSERT INTO `interfacelog` VALUES (1464, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:52:40');
INSERT INTO `interfacelog` VALUES (1465, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:52:40');
INSERT INTO `interfacelog` VALUES (1466, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:53:30');
INSERT INTO `interfacelog` VALUES (1467, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:53:33');
INSERT INTO `interfacelog` VALUES (1468, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:53:36');
INSERT INTO `interfacelog` VALUES (1469, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:53:37');
INSERT INTO `interfacelog` VALUES (1470, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:02');
INSERT INTO `interfacelog` VALUES (1471, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:04');
INSERT INTO `interfacelog` VALUES (1472, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:06');
INSERT INTO `interfacelog` VALUES (1473, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:08');
INSERT INTO `interfacelog` VALUES (1474, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:10');
INSERT INTO `interfacelog` VALUES (1475, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:14');
INSERT INTO `interfacelog` VALUES (1476, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:15');
INSERT INTO `interfacelog` VALUES (1477, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:29');
INSERT INTO `interfacelog` VALUES (1478, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:31');
INSERT INTO `interfacelog` VALUES (1479, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:34');
INSERT INTO `interfacelog` VALUES (1480, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:39');
INSERT INTO `interfacelog` VALUES (1481, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:54:39');
INSERT INTO `interfacelog` VALUES (1482, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:54:52');
INSERT INTO `interfacelog` VALUES (1483, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:54:52');
INSERT INTO `interfacelog` VALUES (1484, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:55:48');
INSERT INTO `interfacelog` VALUES (1485, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:55:49');
INSERT INTO `interfacelog` VALUES (1486, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:57:02');
INSERT INTO `interfacelog` VALUES (1487, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:57:13');
INSERT INTO `interfacelog` VALUES (1488, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:57:16');
INSERT INTO `interfacelog` VALUES (1489, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:57:30');
INSERT INTO `interfacelog` VALUES (1490, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:57:32');
INSERT INTO `interfacelog` VALUES (1491, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:57:37');
INSERT INTO `interfacelog` VALUES (1492, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:57:40');
INSERT INTO `interfacelog` VALUES (1493, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 13:57:40');
INSERT INTO `interfacelog` VALUES (1494, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:59:02');
INSERT INTO `interfacelog` VALUES (1495, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 13:59:04');
INSERT INTO `interfacelog` VALUES (1496, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:06');
INSERT INTO `interfacelog` VALUES (1497, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:08');
INSERT INTO `interfacelog` VALUES (1498, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:09');
INSERT INTO `interfacelog` VALUES (1499, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:14');
INSERT INTO `interfacelog` VALUES (1500, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:15');
INSERT INTO `interfacelog` VALUES (1501, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:23');
INSERT INTO `interfacelog` VALUES (1502, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:29');
INSERT INTO `interfacelog` VALUES (1503, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:32');
INSERT INTO `interfacelog` VALUES (1504, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:34');
INSERT INTO `interfacelog` VALUES (1505, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:37');
INSERT INTO `interfacelog` VALUES (1506, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:38');
INSERT INTO `interfacelog` VALUES (1507, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:00:41');
INSERT INTO `interfacelog` VALUES (1508, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:01:07');
INSERT INTO `interfacelog` VALUES (1509, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:01:07');
INSERT INTO `interfacelog` VALUES (1510, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:01:23');
INSERT INTO `interfacelog` VALUES (1511, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:01:26');
INSERT INTO `interfacelog` VALUES (1512, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:01:29');
INSERT INTO `interfacelog` VALUES (1513, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:01:29');
INSERT INTO `interfacelog` VALUES (1514, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:01:53');
INSERT INTO `interfacelog` VALUES (1515, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:01:55');
INSERT INTO `interfacelog` VALUES (1516, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:02:03');
INSERT INTO `interfacelog` VALUES (1517, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:02:10');
INSERT INTO `interfacelog` VALUES (1518, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:02:12');
INSERT INTO `interfacelog` VALUES (1519, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:02:17');
INSERT INTO `interfacelog` VALUES (1520, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:02:17');
INSERT INTO `interfacelog` VALUES (1521, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:02:40');
INSERT INTO `interfacelog` VALUES (1522, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:02:43');
INSERT INTO `interfacelog` VALUES (1523, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:02:43');
INSERT INTO `interfacelog` VALUES (1524, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:02:46');
INSERT INTO `interfacelog` VALUES (1525, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 14:05:48');
INSERT INTO `interfacelog` VALUES (1526, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:05:55');
INSERT INTO `interfacelog` VALUES (1527, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:05:57');
INSERT INTO `interfacelog` VALUES (1528, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:06:00');
INSERT INTO `interfacelog` VALUES (1529, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:06:36');
INSERT INTO `interfacelog` VALUES (1530, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:07:26');
INSERT INTO `interfacelog` VALUES (1531, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:07:28');
INSERT INTO `interfacelog` VALUES (1532, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:07:40');
INSERT INTO `interfacelog` VALUES (1533, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:07:43');
INSERT INTO `interfacelog` VALUES (1534, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:07:45');
INSERT INTO `interfacelog` VALUES (1535, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:07:48');
INSERT INTO `interfacelog` VALUES (1536, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:08:24');
INSERT INTO `interfacelog` VALUES (1537, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:08:27');
INSERT INTO `interfacelog` VALUES (1538, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:08:29');
INSERT INTO `interfacelog` VALUES (1539, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:08:31');
INSERT INTO `interfacelog` VALUES (1540, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:08:31');
INSERT INTO `interfacelog` VALUES (1541, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:08:31');
INSERT INTO `interfacelog` VALUES (1542, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:08:33');
INSERT INTO `interfacelog` VALUES (1543, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:09:18');
INSERT INTO `interfacelog` VALUES (1544, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:10:35');
INSERT INTO `interfacelog` VALUES (1545, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:11:07');
INSERT INTO `interfacelog` VALUES (1546, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:11:09');
INSERT INTO `interfacelog` VALUES (1547, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:13:12');
INSERT INTO `interfacelog` VALUES (1548, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:13:52');
INSERT INTO `interfacelog` VALUES (1549, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:14:19');
INSERT INTO `interfacelog` VALUES (1550, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:23:41');
INSERT INTO `interfacelog` VALUES (1551, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:25:37');
INSERT INTO `interfacelog` VALUES (1552, 'Role/rolelist', 0, '::1', '200', 'POST', 'Success', '2023-08-31 14:25:57');
INSERT INTO `interfacelog` VALUES (1553, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 14:28:13');
INSERT INTO `interfacelog` VALUES (1554, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:28:20');
INSERT INTO `interfacelog` VALUES (1555, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:28:23');
INSERT INTO `interfacelog` VALUES (1556, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:28:31');
INSERT INTO `interfacelog` VALUES (1557, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:28:52');
INSERT INTO `interfacelog` VALUES (1558, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:30:10');
INSERT INTO `interfacelog` VALUES (1559, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:30:12');
INSERT INTO `interfacelog` VALUES (1560, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:30:16');
INSERT INTO `interfacelog` VALUES (1561, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:30:19');
INSERT INTO `interfacelog` VALUES (1562, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:30:19');
INSERT INTO `interfacelog` VALUES (1563, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:30:25');
INSERT INTO `interfacelog` VALUES (1564, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:30:39');
INSERT INTO `interfacelog` VALUES (1565, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:30:42');
INSERT INTO `interfacelog` VALUES (1566, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:33:26');
INSERT INTO `interfacelog` VALUES (1567, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:33:26');
INSERT INTO `interfacelog` VALUES (1568, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:33:30');
INSERT INTO `interfacelog` VALUES (1569, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:33:33');
INSERT INTO `interfacelog` VALUES (1570, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:33:35');
INSERT INTO `interfacelog` VALUES (1571, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:33:39');
INSERT INTO `interfacelog` VALUES (1572, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:33:42');
INSERT INTO `interfacelog` VALUES (1573, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:34:57');
INSERT INTO `interfacelog` VALUES (1574, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:34:57');
INSERT INTO `interfacelog` VALUES (1575, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:34:58');
INSERT INTO `interfacelog` VALUES (1576, 'User/userlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:34:58');
INSERT INTO `interfacelog` VALUES (1577, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:34:59');
INSERT INTO `interfacelog` VALUES (1578, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:34:59');
INSERT INTO `interfacelog` VALUES (1579, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:35:00');
INSERT INTO `interfacelog` VALUES (1580, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:35:00');
INSERT INTO `interfacelog` VALUES (1581, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:35:28');
INSERT INTO `interfacelog` VALUES (1582, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:35:36');
INSERT INTO `interfacelog` VALUES (1583, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:35:39');
INSERT INTO `interfacelog` VALUES (1584, 'Menu/Addsysbutton', 1, '::1', '200', 'POST', '新增成功', '2023-08-31 14:36:03');
INSERT INTO `interfacelog` VALUES (1585, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:36:06');
INSERT INTO `interfacelog` VALUES (1586, 'Menu/getbtninfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:36:23');
INSERT INTO `interfacelog` VALUES (1587, 'Menu/Addsysbutton', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 14:36:25');
INSERT INTO `interfacelog` VALUES (1588, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:36:28');
INSERT INTO `interfacelog` VALUES (1589, 'Menu/getbtninfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:36:30');
INSERT INTO `interfacelog` VALUES (1590, 'Menu/getbtninfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:36:34');
INSERT INTO `interfacelog` VALUES (1591, 'Menu/Addsysbutton', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 14:36:41');
INSERT INTO `interfacelog` VALUES (1592, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:36:44');
INSERT INTO `interfacelog` VALUES (1593, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:36:44');
INSERT INTO `interfacelog` VALUES (1594, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:36:45');
INSERT INTO `interfacelog` VALUES (1595, 'Menu/RolePer', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:36:45');
INSERT INTO `interfacelog` VALUES (1596, 'Menu/setrolepermise', 1, '::1', '200', 'POST', '请求(或处理)成功', '2023-08-31 14:36:49');
INSERT INTO `interfacelog` VALUES (1597, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:36:50');
INSERT INTO `interfacelog` VALUES (1598, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:36:50');
INSERT INTO `interfacelog` VALUES (1599, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:36:52');
INSERT INTO `interfacelog` VALUES (1600, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:36:52');
INSERT INTO `interfacelog` VALUES (1601, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:36:54');
INSERT INTO `interfacelog` VALUES (1602, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:03');
INSERT INTO `interfacelog` VALUES (1603, 'Menu/getfieldinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:37:08');
INSERT INTO `interfacelog` VALUES (1604, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 14:37:13');
INSERT INTO `interfacelog` VALUES (1605, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:16');
INSERT INTO `interfacelog` VALUES (1606, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:37:19');
INSERT INTO `interfacelog` VALUES (1607, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:19');
INSERT INTO `interfacelog` VALUES (1608, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:37:20');
INSERT INTO `interfacelog` VALUES (1609, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:20');
INSERT INTO `interfacelog` VALUES (1610, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:24');
INSERT INTO `interfacelog` VALUES (1611, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:28');
INSERT INTO `interfacelog` VALUES (1612, 'Menu/getfieldinfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:37:31');
INSERT INTO `interfacelog` VALUES (1613, 'Menu/addmenufiled', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 14:37:36');
INSERT INTO `interfacelog` VALUES (1614, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:37:37');
INSERT INTO `interfacelog` VALUES (1615, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:37');
INSERT INTO `interfacelog` VALUES (1616, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:37:38');
INSERT INTO `interfacelog` VALUES (1617, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:37:38');
INSERT INTO `interfacelog` VALUES (1618, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:05');
INSERT INTO `interfacelog` VALUES (1619, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:07');
INSERT INTO `interfacelog` VALUES (1620, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:10');
INSERT INTO `interfacelog` VALUES (1621, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:11');
INSERT INTO `interfacelog` VALUES (1622, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:14');
INSERT INTO `interfacelog` VALUES (1623, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:14');
INSERT INTO `interfacelog` VALUES (1624, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:15');
INSERT INTO `interfacelog` VALUES (1625, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:15');
INSERT INTO `interfacelog` VALUES (1626, 'Menu/stsmenufiledlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:17');
INSERT INTO `interfacelog` VALUES (1627, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:29');
INSERT INTO `interfacelog` VALUES (1628, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:31');
INSERT INTO `interfacelog` VALUES (1629, 'Role/rolelist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:31');
INSERT INTO `interfacelog` VALUES (1630, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:32');
INSERT INTO `interfacelog` VALUES (1631, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:32');
INSERT INTO `interfacelog` VALUES (1632, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:37');
INSERT INTO `interfacelog` VALUES (1633, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:40');
INSERT INTO `interfacelog` VALUES (1634, 'Menu/getbtninfo', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:38:44');
INSERT INTO `interfacelog` VALUES (1635, 'Menu/Addsysbutton', 1, '::1', '200', 'POST', '修改成功', '2023-08-31 14:38:49');
INSERT INTO `interfacelog` VALUES (1636, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 14:38:54');
INSERT INTO `interfacelog` VALUES (1637, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:39:00');
INSERT INTO `interfacelog` VALUES (1638, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:39:04');
INSERT INTO `interfacelog` VALUES (1639, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:39:07');
INSERT INTO `interfacelog` VALUES (1640, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:39:15');
INSERT INTO `interfacelog` VALUES (1641, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:39:22');
INSERT INTO `interfacelog` VALUES (1642, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 14:39:29');
INSERT INTO `interfacelog` VALUES (1643, 'User/Captcha', 0, '::1', '200', 'GET', 'Success', '2023-08-31 15:11:04');
INSERT INTO `interfacelog` VALUES (1644, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 15:11:13');
INSERT INTO `interfacelog` VALUES (1645, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 15:11:17');
INSERT INTO `interfacelog` VALUES (1646, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 15:11:18');
INSERT INTO `interfacelog` VALUES (1647, 'Menu/searchlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 15:11:19');
INSERT INTO `interfacelog` VALUES (1648, 'Menu/SysMenulist', 1, '::1', '200', 'GET', 'Success', '2023-08-31 15:12:42');
INSERT INTO `interfacelog` VALUES (1649, 'Menu/getbtn', 1, '::1', '200', 'GET', 'Success', '2023-08-31 15:12:44');
INSERT INTO `interfacelog` VALUES (1650, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 15:12:44');
INSERT INTO `interfacelog` VALUES (1651, 'Menu/stsbuttonlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 15:12:45');
INSERT INTO `interfacelog` VALUES (1652, 'Menu/menulist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 15:12:49');
INSERT INTO `interfacelog` VALUES (1653, 'Menu/searchlist', 1, '::1', '200', 'POST', 'Success', '2023-08-31 15:12:50');
INSERT INTO `interfacelog` VALUES (1654, 'User/Captcha', 0, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:21');
INSERT INTO `interfacelog` VALUES (1655, 'Menu/SysMenulist', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:31');
INSERT INTO `interfacelog` VALUES (1656, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:33');
INSERT INTO `interfacelog` VALUES (1657, 'Role/rolelist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:34');
INSERT INTO `interfacelog` VALUES (1658, 'Role/gettree', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:36');
INSERT INTO `interfacelog` VALUES (1659, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:36');
INSERT INTO `interfacelog` VALUES (1660, 'User/userlist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:36');
INSERT INTO `interfacelog` VALUES (1661, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:38');
INSERT INTO `interfacelog` VALUES (1662, 'User/logininfo', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:38');
INSERT INTO `interfacelog` VALUES (1663, 'Role/gettree', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:40');
INSERT INTO `interfacelog` VALUES (1664, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:40');
INSERT INTO `interfacelog` VALUES (1665, 'User/userlist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:41');
INSERT INTO `interfacelog` VALUES (1666, 'Role/gettree', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:41');
INSERT INTO `interfacelog` VALUES (1667, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:41');
INSERT INTO `interfacelog` VALUES (1668, 'User/userlist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:42');
INSERT INTO `interfacelog` VALUES (1669, 'Role/gettree', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:42');
INSERT INTO `interfacelog` VALUES (1670, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:42');
INSERT INTO `interfacelog` VALUES (1671, 'User/userlist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:43');
INSERT INTO `interfacelog` VALUES (1672, 'Role/gettree', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:43');
INSERT INTO `interfacelog` VALUES (1673, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:43');
INSERT INTO `interfacelog` VALUES (1674, 'User/userlist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:44');
INSERT INTO `interfacelog` VALUES (1675, 'Role/gettree', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:44');
INSERT INTO `interfacelog` VALUES (1676, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:44');
INSERT INTO `interfacelog` VALUES (1677, 'User/userlist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:45');
INSERT INTO `interfacelog` VALUES (1678, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:45');
INSERT INTO `interfacelog` VALUES (1679, 'Role/gettree', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:45');
INSERT INTO `interfacelog` VALUES (1680, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:47:46');
INSERT INTO `interfacelog` VALUES (1681, 'User/userlist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:46');
INSERT INTO `interfacelog` VALUES (1682, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:46');
INSERT INTO `interfacelog` VALUES (1683, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:51');
INSERT INTO `interfacelog` VALUES (1684, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:52');
INSERT INTO `interfacelog` VALUES (1685, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:52');
INSERT INTO `interfacelog` VALUES (1686, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:54');
INSERT INTO `interfacelog` VALUES (1687, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:55');
INSERT INTO `interfacelog` VALUES (1688, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:47:56');
INSERT INTO `interfacelog` VALUES (1689, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:48:02');
INSERT INTO `interfacelog` VALUES (1690, 'Menu/menulist', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:48:03');
INSERT INTO `interfacelog` VALUES (1691, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:48:04');
INSERT INTO `interfacelog` VALUES (1692, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:48:05');
INSERT INTO `interfacelog` VALUES (1693, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:48:06');
INSERT INTO `interfacelog` VALUES (1694, 'User/logininfo', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:48:07');
INSERT INTO `interfacelog` VALUES (1695, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:48:07');
INSERT INTO `interfacelog` VALUES (1696, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:48:08');
INSERT INTO `interfacelog` VALUES (1697, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:48:09');
INSERT INTO `interfacelog` VALUES (1698, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:48:10');
INSERT INTO `interfacelog` VALUES (1699, 'Menu/getbtn', 1, '192.168.1.106', '200', 'GET', 'Success', '2023-09-07 17:48:11');
INSERT INTO `interfacelog` VALUES (1700, 'User/interfacelog', 1, '192.168.1.106', '200', 'POST', 'Success', '2023-09-07 17:48:11');

-- ----------------------------
-- Table structure for loginlog
-- ----------------------------
DROP TABLE IF EXISTS `loginlog`;
CREATE TABLE `loginlog`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `userid` bigint(0) NULL DEFAULT NULL,
  `userip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `resultcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `resultmsg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '登录日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of loginlog
-- ----------------------------
INSERT INTO `loginlog` VALUES (1, 1, '::1', '200', '登陆成功', '2023-08-30 13:04:08');
INSERT INTO `loginlog` VALUES (2, 1, '::1', '200', '登陆成功', '2023-08-30 14:08:57');
INSERT INTO `loginlog` VALUES (3, 1, '::1', '200', '登陆成功', '2023-08-30 14:35:28');
INSERT INTO `loginlog` VALUES (4, 1, '::1', '200', '登陆成功', '2023-08-30 15:41:29');
INSERT INTO `loginlog` VALUES (5, 1, '::1', '200', '登陆成功', '2023-08-30 15:41:29');
INSERT INTO `loginlog` VALUES (6, 1, '::1', '200', '登陆成功', '2023-08-30 15:41:29');
INSERT INTO `loginlog` VALUES (7, 1, '::1', '200', '登陆成功', '2023-08-30 16:07:09');
INSERT INTO `loginlog` VALUES (8, 1, '::1', '200', '登陆成功', '2023-08-30 16:28:00');
INSERT INTO `loginlog` VALUES (9, 1, '::1', '200', '登陆成功', '2023-08-30 16:49:41');
INSERT INTO `loginlog` VALUES (10, 1, '::1', '200', '登陆成功', '2023-08-30 17:10:11');
INSERT INTO `loginlog` VALUES (11, 1, '::1', '200', '登陆成功', '2023-08-31 10:10:07');
INSERT INTO `loginlog` VALUES (12, 1, '::1', '200', '登陆成功', '2023-08-31 10:30:22');
INSERT INTO `loginlog` VALUES (13, 1, '::1', '200', '登陆成功', '2023-08-31 10:52:05');
INSERT INTO `loginlog` VALUES (14, 0, '::1', '500', '验证码错误', '2023-08-31 12:09:15');
INSERT INTO `loginlog` VALUES (15, 1, '::1', '200', '登陆成功', '2023-08-31 12:09:20');
INSERT INTO `loginlog` VALUES (16, 1, '::1', '200', '登陆成功', '2023-08-31 12:50:15');
INSERT INTO `loginlog` VALUES (17, 1, '::1', '200', '登陆成功', '2023-08-31 13:16:10');
INSERT INTO `loginlog` VALUES (18, 1, '::1', '200', '登陆成功', '2023-08-31 13:44:10');
INSERT INTO `loginlog` VALUES (19, 1, '::1', '200', '登陆成功', '2023-08-31 14:05:51');
INSERT INTO `loginlog` VALUES (20, 1, '::1', '200', '登陆成功', '2023-08-31 14:28:17');
INSERT INTO `loginlog` VALUES (21, 1, '::1', '200', '登陆成功', '2023-08-31 15:11:09');
INSERT INTO `loginlog` VALUES (22, 1, '192.168.1.106', '200', '登陆成功', '2023-09-07 17:47:27');

-- ----------------------------
-- Table structure for organization
-- ----------------------------
DROP TABLE IF EXISTS `organization`;
CREATE TABLE `organization`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `singleTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'singleTitle',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `isLock` int(0) NOT NULL COMMENT '0启用1禁用',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '编码',
  `parentID` int(0) NULL DEFAULT NULL COMMENT '父级关系',
  `classList` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `classLayer` int(0) NULL DEFAULT NULL,
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of organization
-- ----------------------------
INSERT INTO `organization` VALUES (1, '人力资源组织', '人力资源组织', 1, 0, 'HR ORGANIZATION', 10, NULL, NULL, '2023-09-09 13:01:26', 1, 1, '2023-09-11 12:11:08', 1, 1);
INSERT INTO `organization` VALUES (2, '财务组织', '财务组织', 2, 0, 'Financial organization', 10, NULL, NULL, '2023-09-09 13:03:01', 1, 1, '2023-09-11 12:11:21', 1, 1);
INSERT INTO `organization` VALUES (3, '采购组织', '采购组织', 3, 0, 'Purchasing organization', 10, NULL, NULL, '2023-09-09 13:05:03', 1, 1, '2023-09-11 12:11:33', 1, 1);
INSERT INTO `organization` VALUES (4, '生产组织', '生产组织', 4, 0, 'Production Organization', 0, NULL, NULL, '2023-09-09 13:06:53', 1, 1, '2023-09-09 13:38:09', 1, 1);
INSERT INTO `organization` VALUES (5, '开发组织', '开发组织', 99, 0, 'Development organization', 0, NULL, NULL, '2023-09-09 13:07:14', 1, 1, NULL, NULL, NULL);
INSERT INTO `organization` VALUES (6, '测试组织', '测试组织', 99, 0, 'test organization', 0, NULL, NULL, '2023-09-09 13:07:28', 1, 1, NULL, NULL, NULL);
INSERT INTO `organization` VALUES (10, '总组织', '总组织', 1, 0, 'all', 0, NULL, NULL, '2023-09-11 12:10:58', 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `producttype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品类型（字典）',
  `productlabel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '商品标签（字典）',
  `productoriginalprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '原价',
  `productprice` decimal(10, 2) NULL DEFAULT NULL COMMENT '售价',
  `productnumber` int(0) NULL DEFAULT NULL,
  `outbound` int(0) NULL DEFAULT NULL COMMENT '是否出库，0入库，1出库',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `isLock` int(0) NOT NULL COMMENT '0启用1禁用',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES (1, '红富士·', '2', '25,26', 21312.00, 12312.00, 55, 0, NULL, 0, '2023-09-12 13:25:54', 1, 1, NULL, NULL, NULL);
INSERT INTO `product` VALUES (2, '发给她', '2', '25,27', 231.00, 12.00, 123, 0, NULL, 0, '2023-09-13 12:47:27', 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for productimg
-- ----------------------------
DROP TABLE IF EXISTS `productimg`;
CREATE TABLE `productimg`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `productID` int(0) NULL DEFAULT NULL,
  `imgurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of productimg
-- ----------------------------
INSERT INTO `productimg` VALUES (1, 1, 'e4f20ef9fb4346408d5c8a6eb3e68aad.jpeg');
INSERT INTO `productimg` VALUES (2, 1, '478167b5905448bfb10cab0afffdf7fe.jpeg');
INSERT INTO `productimg` VALUES (3, 2, 'da19297cb66d472ca9ca2b94b80be317.jpeg');

-- ----------------------------
-- Table structure for rolepermise
-- ----------------------------
DROP TABLE IF EXISTS `rolepermise`;
CREATE TABLE `rolepermise`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `roleid` int(0) NULL DEFAULT NULL,
  `menusid` bigint(0) NOT NULL,
  `tbuttonsid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `buttonsid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `filedsid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sfiledid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '角色权限' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rolepermise
-- ----------------------------
INSERT INTO `rolepermise` VALUES (4, 1, 1, '', '', '', '', '2023-08-29 11:59:29', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (5, 1, 2, '5', '6,7,8,9,15', '10,11,12,13,14,15,16', '7', '2023-08-29 11:59:29', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (6, 1, 21, '1', '3,4,24', '1,2,3,4,5,6', '1', '2023-08-29 11:59:29', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (7, 1, 26, '', '', '', '', '2023-08-30 13:17:36', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (8, 1, 27, '', '', '20,21,22,23,24,25,26', '', '2023-08-30 13:17:36', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (9, 1, 28, '', '', '27,28,29,30,31,32,33,34,35', '', '2023-08-30 13:17:36', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (10, 1, 29, '12,16', '13,14', '36,37,38,39,40,41,42,43,44', '2,3,4,5,6', '2023-08-30 15:52:25', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (11, 1, 33, '17', '18,19', '45,47,48,49,50,51,52', '8', '2023-09-09 12:10:49', 1, 1, '2023-09-15 12:05:34', 1, 1);
INSERT INTO `rolepermise` VALUES (12, 1, 23, '20', '21,22', '53,54,55,56,57,58,59', '9', '2023-09-09 14:42:50', 1, 1, '2023-09-15 12:05:34', 1, 1);

-- ----------------------------
-- Table structure for searchfield
-- ----------------------------
DROP TABLE IF EXISTS `searchfield`;
CREATE TABLE `searchfield`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `pid` int(0) NULL DEFAULT NULL COMMENT '页面ID',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `stype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `timerange` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否开启日期范围 0 开启 1 不开启',
  `timeformat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '时间格式',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `isLock` int(0) NOT NULL COMMENT '0启用1禁用',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '搜索字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of searchfield
-- ----------------------------
INSERT INTO `searchfield` VALUES (1, 21, '角色名称', 'title', 'text', 'false', '', 1, 0, '2023-08-31 15:33:51', 1, 1, '2023-08-31 16:27:02', 1, 1);
INSERT INTO `searchfield` VALUES (2, 29, '用户名', 'userName', 'text', 'false', '', 1, 0, '2023-08-31 16:27:53', 1, 1, NULL, NULL, NULL);
INSERT INTO `searchfield` VALUES (3, 29, '姓名', 'name', 'text', 'false', '', 99, 0, '2023-08-31 16:28:05', 1, 1, NULL, NULL, NULL);
INSERT INTO `searchfield` VALUES (4, 29, '手机号码', 'mobilePhone', 'text', 'false', '', 3, 0, '2023-08-31 16:28:18', 1, 1, NULL, NULL, NULL);
INSERT INTO `searchfield` VALUES (5, 29, '用户状态', 'userState', 'select', 'false', '', 99, 0, '2023-08-31 16:28:45', 1, 1, '2023-09-01 09:40:56', 1, 1);
INSERT INTO `searchfield` VALUES (6, 29, '下拉树', 'org', 'xmselect', 'false', '', 99, 0, '2023-09-01 10:35:39', 1, 1, NULL, NULL, NULL);
INSERT INTO `searchfield` VALUES (7, 2, '菜单名称', 'title', 'text', 'false', '', 99, 0, '2023-09-02 10:58:39', 1, 1, NULL, NULL, NULL);
INSERT INTO `searchfield` VALUES (8, 33, '组织名称', 'title', 'text', 'false', '', 99, 0, '2023-09-09 12:08:23', 1, 1, NULL, NULL, NULL);
INSERT INTO `searchfield` VALUES (9, 23, '字典名称', 'title', 'text', 'false', '', 99, 0, '2023-09-09 14:42:28', 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sqllog
-- ----------------------------
DROP TABLE IF EXISTS `sqllog`;
CREATE TABLE `sqllog`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `userid` bigint(0) NULL DEFAULT NULL,
  `userip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `sql` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `sqltime` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `sqlsate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'sql执行日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sqllog
-- ----------------------------

-- ----------------------------
-- Table structure for sysmenu
-- ----------------------------
DROP TABLE IF EXISTS `sysmenu`;
CREATE TABLE `sysmenu`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称',
  `singleTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '简称',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '编码',
  `identify` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限标识',
  `parentID` int(0) NULL DEFAULT NULL COMMENT '父级关系',
  `classList` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
  `classLayer` int(0) NULL DEFAULT NULL,
  `linkUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '链接URL',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '图标',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `isLock` int(0) NULL DEFAULT NULL COMMENT '是否锁定:0启用1禁用',
  `remark` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '备注',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmenu
-- ----------------------------
INSERT INTO `sysmenu` VALUES (1, '系统菜单管理', '系统菜单管理', '001', '', 0, ',1,', NULL, '', 'layui-icon layui-icon-cellphone', 1, 0, '', '2023-08-12 14:49:07', 1, 1, '2023-09-13 13:41:59', 1, 1);
INSERT INTO `sysmenu` VALUES (2, '平台菜单配置', '平台菜单配置', '002', 'menu', 1, ',1,2,', NULL, 'page/menu/sysmenulist.html', 'layui-icon layui-icon-cellphone', 1, 0, '', '2023-08-12 14:51:59', 1, 1, '2023-09-02 10:07:43', 1, 1);
INSERT INTO `sysmenu` VALUES (21, '系统角色管理', '系统角色管理', 'role', 'role', 1, ',1,23,21,', NULL, 'page/menu/role.html', 'layui-icon layui-icon-cellphone', 2, 0, '', '2023-08-14 16:40:29', 1, 1, '2023-08-31 12:20:06', 1, 1);
INSERT INTO `sysmenu` VALUES (23, '系统字典配置', '系统字典配置', 'dictionary', 'dictionary', 1, ',1,23,', NULL, 'page/menu/dictionary.html', 'layui-icon layui-icon-cellphone', 4, 0, '系统字典配置', '2023-08-15 10:59:49', 1, 1, '2023-09-09 13:42:33', 1, 1);
INSERT INTO `sysmenu` VALUES (26, '系统监控日志', '系统监控日志', '', '', 0, NULL, NULL, '', 'layui-icon layui-icon-cellphone', 2, 0, '', '2023-08-30 13:07:01', 1, 1, '2023-08-31 12:20:56', 1, 1);
INSERT INTO `sysmenu` VALUES (27, '登录日志监控', '登录日志监控', '', '', 26, NULL, NULL, 'page/loginfo/loginlog.html', 'layui-icon layui-icon-cellphone', NULL, 0, '', '2023-08-30 13:07:16', 1, 1, '2023-08-30 13:22:17', 1, 1);
INSERT INTO `sysmenu` VALUES (28, '接口日志监控', '接口日志监控', '', '', 26, NULL, NULL, 'page/loginfo/interfacelog.html', 'layui-icon layui-icon-cellphone', NULL, 0, '', '2023-08-30 13:07:29', 1, 1, '2023-08-30 13:22:38', 1, 1);
INSERT INTO `sysmenu` VALUES (29, '系统用户管理', '系统用户管理', '', 'user', 1, NULL, NULL, 'page/menu/user.html', 'layui-icon layui-icon-cellphone', 4, 0, '', '2023-08-30 15:43:27', 1, 1, '2023-09-09 14:28:48', 1, 1);
INSERT INTO `sysmenu` VALUES (33, '系统组织管理', '系统组织管理', 'organization', 'organization', 1, NULL, NULL, 'page/menu/organization.html', 'layui-icon layui-icon-cellphone', 3, 0, '', '2023-09-09 12:04:49', 1, 1, '2023-09-09 14:28:37', 1, 1);
INSERT INTO `sysmenu` VALUES (38, '商品管理', '商品管理', '', '', 1, NULL, NULL, 'page/menu/management.html', 'layui-icon layui-icon-cellphone', 99, 0, '', '2023-09-12 10:10:56', 1, 1, '2023-09-13 15:07:55', 1, 1);

-- ----------------------------
-- Table structure for sysmenubutton
-- ----------------------------
DROP TABLE IF EXISTS `sysmenubutton`;
CREATE TABLE `sysmenubutton`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `pid` int(0) NOT NULL COMMENT '页面id',
  `btevent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '事件',
  `singleTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'singleTitle',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `style` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '样式',
  `type` int(0) NOT NULL COMMENT '类型0是表格按钮1是头部按钮',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `isLock` int(0) NOT NULL COMMENT '0禁用1启用',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 47 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmenubutton
-- ----------------------------
INSERT INTO `sysmenubutton` VALUES (1, 21, 'add', NULL, '新增', 'background-color:#28a745', 1, 1, 0, '2023-08-17 10:12:50', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (3, 21, 'edit', NULL, '修改', 'background-color:#28a745', 0, 2, 0, '2023-08-17 10:38:38', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (4, 21, 'fp', NULL, '权限分配', 'background-color:#1E9FFF', 0, 2, 0, '2023-08-17 11:09:03', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (5, 2, 'add', NULL, '新增菜单', 'background-color:#28a745', 1, 1, 0, '2023-08-26 16:33:11', 1, 1, '2023-08-26 16:58:21', 1, 1);
INSERT INTO `sysmenubutton` VALUES (6, 2, 'edit', NULL, '修改', 'background-color:#28a745', 0, 1, 0, '2023-08-26 16:33:30', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (7, 2, 'button', NULL, '表格按钮', 'background-color:#1E9FFF', 0, 3, 0, '2023-08-26 16:33:45', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (8, 2, 'filed', NULL, '表格字段', 'background-color:#FFB800', 0, 5, 0, '2023-08-26 16:33:57', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (9, 2, 'del', NULL, '删除', 'background-color:#FF5722', 0, 99, 0, '2023-08-26 16:34:11', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (12, 29, 'add', NULL, '新增用户', 'background-color:#28a745', 1, 99, 0, '2023-08-30 17:09:38', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (13, 29, 'edit', NULL, '修改', 'background-color:#28a745', 0, 99, 0, '2023-08-30 17:10:36', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (14, 29, 'chooserole', NULL, '选择角色', 'background-color:#1E9FFF', 0, 99, 0, '2023-08-30 17:10:56', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (15, 2, 'search', NULL, '搜索字段', 'background-color:#31BDEC', 0, 4, 0, '2023-08-31 14:36:03', 1, 1, '2023-08-31 14:38:49', 1, 1);
INSERT INTO `sysmenubutton` VALUES (16, 29, 'export', NULL, '导出用户信息', '', 1, 99, 0, '2023-09-05 13:33:46', 1, 1, '2023-09-05 16:37:09', 1, 1);
INSERT INTO `sysmenubutton` VALUES (17, 33, 'add', NULL, '新增组织', 'background-color:#28a745', 1, 99, 0, '2023-09-09 12:07:06', 1, 1, '2023-09-09 15:07:52', 1, 1);
INSERT INTO `sysmenubutton` VALUES (18, 33, 'edit', NULL, '修改组织', 'background-color:#28a745', 0, 99, 0, '2023-09-09 12:07:19', 1, 1, '2023-09-09 15:07:58', 1, 1);
INSERT INTO `sysmenubutton` VALUES (19, 33, 'del', NULL, '删除组织', 'background-color:#FF5722', 0, 99, 0, '2023-09-09 12:07:59', 1, 1, '2023-09-09 15:08:04', 1, 1);
INSERT INTO `sysmenubutton` VALUES (20, 23, 'add', NULL, '新增字典', 'background-color:#28a745', 1, 99, 0, '2023-09-09 12:07:06', 1, 1, '2023-09-09 15:07:24', 1, 1);
INSERT INTO `sysmenubutton` VALUES (21, 23, 'edit', NULL, '修改字典', 'background-color:#28a745', 0, 99, 0, '2023-09-09 12:07:19', 1, 1, '2023-09-09 15:07:30', 1, 1);
INSERT INTO `sysmenubutton` VALUES (22, 23, 'del', NULL, '删除字典', 'background-color:#FF5722', 0, 99, 0, '2023-09-09 12:07:59', 1, 1, '2023-09-09 15:07:36', 1, 1);
INSERT INTO `sysmenubutton` VALUES (24, 21, 'del', NULL, '删除', 'background-color:#FF5722', 0, 999, 0, '1900-01-01 00:00:00', 0, 0, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (45, 38, 'add', NULL, '新增', 'background-color:#28a745', 1, 1, 0, '1900-01-01 00:00:00', 0, 0, NULL, NULL, NULL);
INSERT INTO `sysmenubutton` VALUES (46, 38, 'edit', NULL, '修改', 'background-color:#28a745', 0, 2, 0, '1900-01-01 00:00:00', 0, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sysmenufiled
-- ----------------------------
DROP TABLE IF EXISTS `sysmenufiled`;
CREATE TABLE `sysmenufiled`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `field` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设定字段名',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设定标题名称',
  `width` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设定列宽，若不填写，则自动分配',
  `minWidth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '单元格的最小宽度',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '设定列类型。可选值有：\r\nnormal（常规列，无需设定）\r\ncheckbox（复选框列）\r\nradio（单选框列）\r\nnumbers（序号列）\r\nspace（空列）',
  `fixedAlign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '浮动',
  `sort` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '排序',
  `eventName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '点击事件名',
  `style` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '自定义单元格样式。即传入任意的 CSS 字符',
  `align` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '单元格排列方式。可选值有：left、center、right',
  `isLock` int(0) NULL DEFAULT NULL COMMENT '是否锁定:0启用1禁用',
  `templet` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '模板名称',
  `totalRow` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '是否开启该列的自动合计功能',
  `pid` int(0) NOT NULL COMMENT '页面id',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 64 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '表头参数' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysmenufiled
-- ----------------------------
INSERT INTO `sysmenufiled` VALUES (1, 'title', '角色名称', '', '', '', '', 'flase', '', '', 'center', 0, '', 'flase', 21, 2, '2023-08-25 13:36:28', 1, 1, '2023-08-26 17:26:39', 1, 1);
INSERT INTO `sysmenufiled` VALUES (2, 'singleTitle', '角色简称', '200', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 21, 3, '2023-08-25 13:40:46', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (3, 'sortID', '排序', '80', NULL, '', '', 'true', '', '', 'center', 0, '', 'flase', 21, 3, '2023-08-25 13:51:03', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (4, 'isLock', '是否禁用', '100', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 21, 99, '2023-08-25 13:51:27', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (5, '', '选择', '48', NULL, 'checkbox', 'left', 'flase', '', '', 'center', 0, '', 'flase', 21, 1, '2023-08-25 16:40:33', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (6, '1', '操作', '200', NULL, '', 'right', 'flase', '', '', 'center', 0, '.toolbar', 'flase', 21, 999, '2023-08-26 10:33:20', 1, 1, '2023-08-26 17:27:03', 1, 1);
INSERT INTO `sysmenufiled` VALUES (10, NULL, '选择', '48', NULL, 'checkbox', NULL, 'flase', NULL, NULL, 'center', 0, NULL, 'flase', 2, 1, '2023-08-26 16:23:54', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (11, '', '操作', '350', NULL, '', '', 'flase', '', '', 'center', 0, '.toolbar', 'flase', 2, 999, '2023-08-26 16:23:59', 1, 1, '2023-08-31 14:37:36', 1, 1);
INSERT INTO `sysmenufiled` VALUES (12, 'title', '菜单名称', '', '', '', '', 'flase', '', '', 'left', 0, '', 'flase', 2, 2, '2023-08-26 16:26:57', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (13, 'identify', '页面搜索名称', '120', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 2, 99, '2023-08-26 16:27:22', 1, 1, '2023-09-02 10:07:11', 1, 1);
INSERT INTO `sysmenufiled` VALUES (14, 'linkUrl', '菜单url', '120', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 2, 99, '2023-08-26 16:27:35', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (15, 'sortID', '排序号', '90', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 2, 99, '2023-08-26 16:27:56', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (16, 'isLock', '是否禁用', '120', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 2, 99, '2023-08-26 16:28:14', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (20, NULL, '选择', '48', NULL, 'checkbox', NULL, 'flase', NULL, NULL, 'center', 0, NULL, 'flase', 27, 1, '2023-08-30 13:07:53', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (21, '', '序号', '100', NULL, 'numbers', '', 'flase', '', '', 'center', 0, '', 'flase', 27, 99, '2023-08-30 13:08:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (22, 'userid', '登录用户名', '', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 27, 99, '2023-08-30 13:08:33', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (23, 'userip', '用户IP地址', '200', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 27, 99, '2023-08-30 13:08:55', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (24, 'resultcode', '返回状态码', '100', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 27, 99, '2023-08-30 13:09:15', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (25, 'resultmsg', '返回信息', '280', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 27, 99, '2023-08-30 13:09:37', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (26, 'insertTime', '登录时间', '260', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 27, 99, '2023-08-30 13:09:54', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (27, NULL, '选择', '48', NULL, 'checkbox', NULL, 'flase', NULL, NULL, 'center', 0, NULL, 'flase', 28, 1, '2023-08-30 13:10:06', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (28, '', '序号', '100', NULL, 'numbers', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:10:22', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (29, 'interfacename', '请求路由', '', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:11:24', 1, 1, '2023-08-30 13:11:34', 1, 1);
INSERT INTO `sysmenufiled` VALUES (30, 'userid', '请求用户名', '200', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:11:59', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (31, 'userip', '用户IP', '250', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:12:17', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (32, 'resultcode', '返回状态码', '100', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:12:35', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (33, 'resultmsg', '返回信息', '280', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:12:59', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (34, 'requesttype', '请求类型', '100', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:13:23', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (35, 'insertTime', '请求时间', '260', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 28, 99, '2023-08-30 13:13:39', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (36, NULL, '操作', '200', NULL, NULL, NULL, 'flase', NULL, NULL, 'center', 0, '.toolbar', 'flase', 29, 999, '2023-08-30 15:44:17', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (37, NULL, '选择', '48', NULL, 'checkbox', NULL, 'flase', NULL, NULL, 'center', 0, NULL, 'flase', 29, 1, '2023-08-30 15:44:21', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (38, 'userName', '用户名', '160', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 29, 99, '2023-08-30 15:45:06', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (39, 'code', '编码', '80', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 29, 99, '2023-08-30 15:45:29', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (40, 'name', '姓名', '180', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 29, 99, '2023-08-30 15:45:43', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (41, 'mobilePhone', '手机号码', '', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 29, 99, '2023-08-30 15:45:55', 1, 1, '2023-08-30 15:47:03', 1, 1);
INSERT INTO `sysmenufiled` VALUES (42, 'sortID', '排序', '80', NULL, '', '', 'true', '', '', 'center', 0, '', 'flase', 29, 100, '2023-08-30 15:46:12', 1, 1, '2023-08-30 15:57:48', 1, 1);
INSERT INTO `sysmenufiled` VALUES (43, 'userStateID', '状态', '130', NULL, '', '', 'flase', '', '', 'center', 0, '#userStateID', 'flase', 29, 100, '2023-08-30 15:46:43', 1, 1, '2023-08-30 15:57:35', 1, 1);
INSERT INTO `sysmenufiled` VALUES (44, 'role', '所属角色', '180', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 29, 99, '2023-08-30 15:57:24', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (45, NULL, '操作', '200', NULL, NULL, NULL, 'flase', NULL, NULL, 'center', 0, '.toolbar', 'flase', 33, 999, '2023-09-09 12:08:30', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (47, NULL, '选择', '48', NULL, 'checkbox', NULL, 'flase', NULL, NULL, 'center', 0, NULL, 'flase', 33, 1, '2023-09-09 12:08:37', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (48, 'title', '组织名称', '', NULL, '', '', 'flase', '', '', 'left', 0, '', 'flase', 33, 99, '2023-09-09 12:09:09', 1, 1, '2023-09-09 13:01:55', 1, 1);
INSERT INTO `sysmenufiled` VALUES (49, 'singleTitle', '组织简称', '200', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 33, 99, '2023-09-09 12:09:33', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (50, 'code', '组织编码', '150', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 33, 99, '2023-09-09 12:09:51', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (51, 'sortID', '序号', '80', NULL, '', '', 'true', '', '', 'center', 0, '', 'flase', 33, 99, '2023-09-09 12:10:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (52, 'isLock', '是否禁用', '100', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 33, 99, '2023-09-09 12:10:34', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (53, NULL, '操作', '200', NULL, NULL, NULL, 'flase', NULL, NULL, 'center', 0, '.toolbar', 'flase', 23, 999, '2023-09-09 14:25:12', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (54, NULL, '选择', '48', NULL, 'checkbox', NULL, 'flase', NULL, NULL, 'center', 0, NULL, 'flase', 23, 1, '2023-09-09 14:25:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (55, 'title', '字典名称', '', NULL, '', '', 'flase', '', '', 'left', 0, '', 'flase', 23, 99, '2023-09-09 14:40:48', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (56, 'singleTitle', '字典简称', '200', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 23, 99, '2023-09-09 14:41:18', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (57, 'code', '字典编码', '150', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 23, 99, '2023-09-09 14:41:36', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (58, 'sortID', '序号', '100', NULL, '', '', 'true', '', '', 'center', 0, '', 'flase', 23, 99, '2023-09-09 14:41:52', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (59, 'isLock', '是否禁用', '100', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 23, 99, '2023-09-09 14:42:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (60, NULL, '操作', '200', NULL, NULL, NULL, 'flase', NULL, NULL, 'center', 0, '.toolbar', 'flase', 38, 999, '2023-09-12 13:40:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (61, NULL, '选择', '48', NULL, 'checkbox', NULL, 'flase', NULL, NULL, 'center', 0, NULL, 'flase', 38, 1, '2023-09-12 13:40:13', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysmenufiled` VALUES (62, 'name', '表名', '', NULL, '', '', 'flase', '', '', 'left', 0, '', 'flase', 38, 99, '2023-09-12 13:40:27', 1, 1, '2023-09-13 15:17:37', 1, 1);
INSERT INTO `sysmenufiled` VALUES (63, 'description', '表描述', '400', NULL, '', '', 'flase', '', '', 'center', 0, '', 'flase', 38, 99, '2023-09-13 15:16:27', 1, 1, '2023-09-13 15:17:48', 1, 1);

-- ----------------------------
-- Table structure for sysrole
-- ----------------------------
DROP TABLE IF EXISTS `sysrole`;
CREATE TABLE `sysrole`  (
  `ID` int(0) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `singleTitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'singleTitle',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `isLock` int(0) NOT NULL COMMENT '0启用1禁用',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sysrole
-- ----------------------------
INSERT INTO `sysrole` VALUES (1, '管理员', '管理员', 1, 0, '2023-08-17 09:33:01', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (2, '游客', '游客', 2, 0, '2023-08-17 14:14:18', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (4, '2', '2', 2, 1, '2023-08-31 10:11:47', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (5, '23', '213', 123, 1, '2023-08-31 10:41:20', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (6, '32', '后台管', 99, 0, '2023-08-31 11:00:50', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (8, '发方法', '312', 99, 0, '2023-09-04 15:13:19', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (9, '地方干活的', '对方过后', 99, 0, '2023-09-04 15:13:26', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (10, '电话费贵', '对方过后', 99, 0, '2023-09-04 15:13:33', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (11, '都好高', '东方红个', 99, 0, '2023-09-04 15:13:39', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (12, '都好肥', '方大化工', 99, 0, '2023-09-04 15:13:47', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (13, '提货人', '好突然', 99, 0, '2023-09-04 15:13:53', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (14, '还让他', '还让他', 99, 0, '2023-09-04 15:14:00', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (15, '1', '1', 99, 0, '2023-09-04 15:14:08', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (16, '123', '23', 9923, 0, '2023-09-04 15:14:15', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (17, '23', '121', 99, 0, '2023-09-04 15:14:21', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (18, '213', '31', 99, 0, '2023-09-04 15:14:27', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (19, '21', '11111111111', 99, 0, '2023-09-04 15:14:36', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (20, '32', '121', 99, 0, '2023-09-04 15:14:42', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (21, '21', '11113', 99, 0, '2023-09-04 15:14:48', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (22, '23', '32', 9932, 0, '2023-09-04 15:14:55', 1, 1, NULL, NULL, NULL);
INSERT INTO `sysrole` VALUES (23, '4324', '23', 99, 0, '2023-09-04 15:15:54', 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for userrole
-- ----------------------------
DROP TABLE IF EXISTS `userrole`;
CREATE TABLE `userrole`  (
  `ID` bigint(0) NOT NULL AUTO_INCREMENT,
  `userid` bigint(0) NOT NULL,
  `roleid` int(0) NOT NULL,
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of userrole
-- ----------------------------
INSERT INTO `userrole` VALUES (1, 1, 1, '2023-08-29 12:39:34', 1, 1, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `ID` bigint(0) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '帐号',
  `userPassword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户编码',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `mobilePhone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '手机号码',
  `profilePhotoUrl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '头像',
  `sortID` int(0) NULL DEFAULT NULL COMMENT '排序',
  `userStateID` int(0) NULL DEFAULT NULL COMMENT '用户状态 0启用1禁用',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `insertTime` datetime(0) NOT NULL COMMENT '添加时间（yyyy-MM-dd HH:mm:ss）',
  `insertUserID` bigint(0) NOT NULL,
  `insertOrgID` bigint(0) NOT NULL,
  `updateTime` datetime(0) NULL DEFAULT NULL COMMENT '修改时间（yyyy-MM-dd HH:mm:ss）',
  `updateUserID` bigint(0) NULL DEFAULT NULL,
  `updateOrgID` bigint(0) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '123456', '1', '打的', '15462635485', '58517d69fc9e470fa646154c9877fab5.jpeg', 1, 0, NULL, '2023-08-11 14:49:44', 1, 1, '2023-09-11 12:58:14', 1, 1);
INSERT INTO `users` VALUES (2, '张三', '123456', '32342', '张三', '154263685214', 'e5c2ef8487a14d43a68577f3f4301260.jpeg', 99, 0, NULL, '2023-09-02 09:58:43', 1, 1, '2023-09-08 12:29:11', 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
