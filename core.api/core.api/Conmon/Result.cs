﻿
using core.api.help;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Ocsp;
using SqlSugar;

namespace core.api.Conmon
{

    public class Result<T>
    {
        //返回的状态
        public int code { get; set; }

        //返回异常信息
        public string excMsg { get; set; }

        //正常返回的业务数据
        public object data { get; set; }

        //返回手写信息
        public object msg { get; set; }

        public Result(int code, string msg, T data = default(T))
        {
            this.code = code;
            this.msg = msg;
            this.data = data;
        }
        public Result(Code code, string msg,  T data = default(T))
        {
            this.code = (int)code;
            this.msg = msg;
            this.data = data;
        }
        public Result()
        {
            this.code = 200;
            this.msg = Code.Success.ToString();
            this.data = default(T);
        }
    }

    public class Result
    {
        //返回的状态
        public int code { get; set; }

        //返回异常信息
        public string excMsg { get; set; }

        //正常返回的业务数据
        public object data { get; set; }

        //返回手写信息
        public object msg { get; set; }

        public Result(int code, string msg, object data = null)
        {
            this.code = code;
            this.msg = msg;
            this.data = data;
        }
        public Result(Code code, string msg, object data = null)
        {
            this.code = (int)code;
            this.msg = msg;
            this.data = data;
        }
        public Result()
        {
            this.code = 200;
            this.msg = Code.Success.ToString();
            this.data = null;
        }
    }

    
    public partial class Reuse
         {
        /// <summary>
        /// 错误返回值
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static Result<T> ErrCode<T>(Code code, string msg = null)
        {
            return new Result<T>((int)code, msg ?? EnumExtension.GetEnumText(code));
        }
        /// <summary>
        /// 错误返回值
        /// </summary>
        /// <param name="code"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static Result ErrCode(Code code, string msg = null)
        {
            return new Result((int)code, msg ?? EnumExtension.GetEnumText(code));
        }
        public static Result SuccessCode(object data, string msg = null)
        {
            return new Result((int)Code.Success, msg ?? EnumExtension.GetEnumText(Code.Success), data = data);
        }
       
    }
        
    /// <summary>
    /// 
    /// </summary>
    public enum Code
    {
        [Text("请求(或处理)成功")]
        Success = 200,

        [Text("登录会话已过期,请重新登录")]
        SessionOutTime = 200100,

        [Text("暂未分配用户角色、菜单,请联系相关负责人为您分配角色、菜单")]
        NotUserRole = 200101,

        [Text("账号或密码错误,无效的用户")]
        NotFindUser = 301,

        [Text("旧密码错误")]
        OldPwdError = 301001,

        [Text("请求频繁!")]
        IpOverfrequency = 302,

        //[Text("账号或密码输入错误次数超过5次,请关闭浏览器重新打开登录!")]
        [Text("账号或密码输入错误次数超过5次,请5分钟后重试!")]
        OverErr = 303,

        [Text("账号未激活,请联系管理员!")]
        NotActiveUser = 304,

        [Text("账号状态异常,请联系管理员!")]
        UserStatusErr = 305,

        [Text("无权限登录后台系统!")]
        NotPowerEnterSystem = 306,

        [Text("账号已禁用,请联系管理员!")]
        ProhibitedUser = 307,

        [Text("账号已禁用,请联系管理员!")]
        SleepUser = 308,

        [Text("请求参数不完整或不正确")]
        ParameterError = 400,

        [Text("未授权标识")]
        Unauthorized = 401,

        [Text("未识别grant_type")]
        UnGrantType = 402,

        [Text("请求TOKEN失效或token为空,请重新登录")]
        TokenInvalid = 403,

        [Text("HTTP请求类型不合法")]
        HttpMehtodError = 405,

        [Text("HTTP请求不合法,请求参数可能被篡改")]
        HttpRequestError = 406,

        [Text("该URL已经失效")]
        URLExpireError = 407,

        [Text("无权限请求")]
        NoAuthority = 408,

        [Text("无此方法或参数错误")]
        MehotdError = 409,

        [Text("检测到恶意字符")]
        MaliciousCharacter = 410,

        [Text("验证码错误")]
        CodeError = 411,

        [Text("验证码已过期")]
        CodeOutTime = 412,

        [Text("运行环境错误")]
        ErrorEnvironment = 413,

        [Text("文件为空，请重新选择文件")]
        NotFile = 440,

        [Text("文件为空，请选择Excel文件")]
        NotExcelFile = 441,

        [Text("文件为空，请选择Pdf文件")]
        NotPdfFile = 442,

        [Text("文件导入失败，请核对导入模板和数据格式是否正确")]
        ImportError = 443,

        [Text("文件导出失败，请重新操作")]
        ExportError = 445,

        [Text("文件类型不正确，请重新选择")]
        FileExtensionError = 446,

        [Text("内部请求出错")]
        Error = 500,
        [Text("请进行手机号验证")]
        VeirCode = 400100,
    }
}
