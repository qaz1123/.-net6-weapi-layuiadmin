﻿using core.api.help;
using core.api.Model;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Newtonsoft.Json;
using SqlSugar;
using StackExchange.Redis;
using System.Text;
using SelectModel = core.api.Model.SelectModel;

namespace core.api.Conmon
{
    public class Searchcommon
    {
        private readonly ISqlSugarClient db;

        public Searchcommon(ISqlSugarClient db)
        {
            this.db = db;
        }

        public List<TreeSelect> Getsearchdata(int? pid, string field)
        {
            List<TreeSelect> list = new List<TreeSelect>();
            var menu = db.Queryable<sysmenu>().Where(v=>v.ID == pid).First();
            if(menu != null)
            {
                switch (menu.identify)
                {
                    case "user":
                        {
                            switch (field) 
                            {
                                case "userState":
                                    {
                                        list.Add(new TreeSelect {name="请选择", value = "" });
                                        list.Add(new TreeSelect { name = "启用", value = "0" });
                                        list.Add(new TreeSelect { name = "禁用", value = "1" });
                                    }
                                    break;
                                case "org":
                                    {
                                        var data =  db.Queryable<sysmenu>().Where(v => v.parentID == 0).ToList();
                                        list = CommonHelper.listtoTreeSelect(data);
                                    }
                                    break;
                            }

                        }
                        break;
                }

            }
            return list;
        }
        public string Getsearchwhere(List<searchfield> searchfields)
        {
            StringBuilder sb = new StringBuilder();

            foreach (var m in searchfields)
            {
                if (m != null)
                {
                    switch (m.stype)
                    {
                        case "text":
                            sb.Append($"<div class=\"layui-form-item\"><label class=\"layui-form-label\">{m.title}</label><div class=\"layui-input-block\"><input type=\"text\" id=\"{m.field}\" name=\"{m.field}\" autocomplete=\"off\"  class=\"layui-input\"></div></div>");
                            break;
                        case "select":
                            {
                                List<TreeSelect> list = new List<TreeSelect>();
                                list = Getsearchdata(m.pid,m.field);

                                sb.Append($"<div class=\"layui-form-item\"><label class=\"layui-form-label\">{m.title}</label><div class=\"layui-input-block\"><select name=\"{m.field}\" lay-search=\"\">");
                                if (list == null)
                                {
                                    sb.Append("</select>");
                                    break;
                                }
                                foreach (var item in list)
                                {
                                    sb.Append($"<option value=\"{item.value}\">{item.name}</option>");
                                }
                                sb.Append("</select></div></div>");
                            }
                            break;
                        case "xmselect":
                            {
                                List<TreeSelect> list = new List<TreeSelect>();
                                list = Getsearchdata(m.pid, m.field);

                                var data = JsonConvert.SerializeObject(list);
                                sb.Append($"<div class=\"layui-form-item smone\"><label class=\"layui-form-label smlable\">{m.title}</label><div class=\"layui-input-block smblock\">");
                                sb.Append($"<div class=\" xmSelect\" style=\"width:182px;height:26px\" data-json=\"\" lay-filter=\"{m.field}\" id=\"{m.field}\" data-type=\"xmselect\"  data-id=\"{m.field}\" data-name=\"{m.field}\"> </div> <script>document.querySelectorAll(\".xmSelect[data-name='{m.field}']\")[0].setAttribute(\"data-json\",JSON.stringify({data}));</script>");
                                sb.Append($"</div></div>");
                            }
                            break;
                        case "time":
                            {
                                sb.Append($"<div class=\"layui-form-item\"><label class=\"layui-form-label\">{m.title}</label><div class=\"layui-input-block\"><input type=\"text\" id=\"{m.field}\" name=\"{m.field}\" autocomplete=\"off\" data-stype=\"date\" data-range=\"{m.timerange}\" data-format=\"\" data-type=\"{m.timeformat}\"  class=\"layui-input\"></div></div>");
                            }
                            break;
                    }

                }
            }

            return sb.ToString();
        }


    }
}
