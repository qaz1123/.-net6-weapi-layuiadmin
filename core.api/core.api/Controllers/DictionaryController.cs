﻿using core.api.Conmon;
using core.api.help;
using core.api.Model.VO;
using core.api.Model;
using core.model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;

namespace core.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DictionaryController : ControllerBase
    {
        private readonly ISqlSugarClient db;
        private readonly UserInfo userInfo;
        private readonly IJWTService _jWTService;
        private readonly IRedisCacheManager _redis;
        private readonly IHttpContextAccessor _httpContextAccessor;
        /// <summary>
        /// 系统字典管理
        /// </summary>
        /// <param name="db"></param>
        /// <param name="userInfo"></param>
        /// <param name="jWTService"></param>
        /// <param name="redis"></param>
        /// <param name="httpContextAccessor"></param>
        public DictionaryController(ISqlSugarClient db, UserInfo userInfo, IJWTService jWTService, IRedisCacheManager redis, IHttpContextAccessor httpContextAccessor)
        {
            this.db = db;
            this.userInfo = userInfo;
            _jWTService = jWTService;
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }

        #region 搜索条件
        /// <summary>
        /// 搜索条件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ApiExplorerSettings(GroupName = "pc端")]
        public string Getsearchwhere()
        {
            string where = " 1=1 ";
            var form = HttpContext.Request.Form;
            var formDictionary = HttpContext.Request.Form.ToDictionary(key => key, value => HttpContext.Request.Form[""]);
            var values = formDictionary.Keys.ToList();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            values.ForEach(m =>
            {
                var k = m.Key;
                var v = m.Value;
                dict.Add(k, v);
            });

            dict.TryGetValue("title", out string title);
            if (!string.IsNullOrEmpty(title))
            {
                where += $" and title like '%{title}%'";
            }

            return where;
        }
        #endregion

        #region 系统字典列表
        /// <summary>
        /// 系统字典列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("dictionarylist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> dictionarylist()
        {
            var form = HttpContext.Request.Form;
            var pid = form["pid"];
            var where = Getsearchwhere();
            var list = db.Queryable<dictionary>().Where(where).Where(v => v.parentID == Convert.ToInt32(pid)).OrderBy(v => v.sortID, OrderByType.Asc).ToList();

            var data = new List<object>();
            list.ForEach(v =>
            {
                bool a = db.Queryable<dictionary>().Where(m => m.parentID == v.ID).Any();
                data.Add(new
                {
                    id = v.ID,
                    pid = DesHelper.Encrypt(v.parentID.ToString()),
                    title = v.title,
                    code = v.code,
                    singleTitle = v.singleTitle,
                    sortID = v.sortID,
                    haveChild = a,
                    isLock = CommonHelper.Getisjinyong(v.isLock),
                });
            });
            return new Result() { data = new { list = data, count = data.Count(), code = 0, msg = "" } };
        }
        #endregion

        #region 新增修改系统字典
        /// <summary>
        /// 新增修改系统字典
        /// </summary>
        /// <param name="dictionaryadd"></param>
        /// <returns></returns>
        [HttpPost("adddictionary")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> adddictionary(dictionaryadd dictionaryadd)
        {
            dictionary dictionary = new dictionary();

            dictionary.title = dictionaryadd.title;
            dictionary.singleTitle = dictionaryadd.singleTitle;
            dictionary.code = dictionaryadd.code;
            dictionary.sortID = Convert.ToInt32(dictionaryadd.sortID);
            dictionary.isLock = Convert.ToInt32(dictionaryadd.isLock);

            if (dictionaryadd.parentID == "")
            {
                dictionary.parentID = 0;
            }
            else
            {
                dictionary.parentID = Convert.ToInt32(dictionaryadd.parentID);
            }

            if (dictionaryadd.Id == 0)
            {
                dictionary.insertTime = DateTime.Now;
                dictionary.insertUserID = userInfo.Getuserinfo().UserID;
                dictionary.insertOrgID = userInfo.Getuserinfo().roleid;
                var id = db.Insertable(dictionary).ExecuteReturnBigIdentity();
                if (id > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "新增成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "新增失败");
                }
            }
            else
            {

                #region 判断所选层级是否是自己或子元素
                if (dictionaryadd.parentID != "")
                {
                    var list = db.Queryable<dictionary>().ToList();
                    var chidlist = CommonHelper.Getchildren(list, dictionaryadd.Id);


                    var pid = Convert.ToInt32(dictionaryadd.parentID);
                    if (pid == dictionaryadd.Id || chidlist.Contains(pid))
                    {
                        return Reuse.ErrCode(Code.Error, "不能选择自己或子节点");
                    }
                }
                #endregion

                dictionary.ID = Convert.ToInt32(dictionaryadd.Id);
                dictionary.updateTime = DateTime.Now;
                dictionary.updateUserID = userInfo.Getuserinfo().UserID;
                dictionary.updateOrgID = userInfo.Getuserinfo().roleid;
                var res = db.Updateable(dictionary).UpdateColumns(it => new { it.title, it.sortID, it.singleTitle, it.code, it.isLock, it.parentID, it.updateTime, it.updateUserID, it.updateOrgID }).ExecuteCommand() > 0;
                if (res)
                {
                    return Reuse.SuccessCode(Code.Success, "修改成功");
                }
                else
                {
                    return Reuse.ErrCode(Code.Error, "修改失败");
                }
            }
        }
        #endregion

        #region 新增修改下拉填充
        /// <summary>
        /// 新增修改下拉填充
        /// </summary>
        /// <returns></returns>
        [HttpGet("dictionarytree")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> dictionarytree()
        {
            var list = await db.Queryable<dictionary>().Where(v => v.isLock == 0 && v.parentID == 0).ToListAsync();
            var data = CommonHelper.listtoTreeSelect(list);
            return new Result() { data = new { data } };
        }
        #endregion

        #region 修改填充
        /// <summary>
        /// 修改填充
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("dictionaryinfo")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> dictionaryinfo(int? id)
        {
            var info = await db.Queryable<dictionary>().Where(v => v.ID == id).FirstAsync();
            var parenttitle = await db.Queryable<dictionary>().Where(v => v.ID == info.parentID).FirstAsync();

            return new Result()
            {
                data = new
                {
                    title = info.title,
                    singleTitle = info?.singleTitle ?? "",
                    parenttitle = parenttitle?.title ?? "",
                    code = info.code,
                    sortID = info.sortID,
                    parentID = info.parentID,
                    isLock = info.isLock,
                }
            };
        }
        #endregion

        #region 删除系统字典
        /// <summary>
        /// 删除系统字典
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("dictionarydel")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> dictionarydel(MyModel myModel)
        {
            var ishave = db.Queryable<dictionary>().Where(v => v.parentID == Convert.ToInt32(myModel.pid)).Any();

            if (ishave)
            {
                return Reuse.ErrCode(Code.Error, "该字典含有子集，无法删除");
            }
            else
            {
                var res = db.Deleteable<dictionary>().Where(v => v.ID == Convert.ToInt32(myModel.pid)).ExecuteCommand();
                if (res > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "删除成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "删除失败");
                }
            }
        }
        #endregion


    }
}
