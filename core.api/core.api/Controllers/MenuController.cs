﻿using core.api.Conmon;
using core.api.help;
using core.api.Model;
using core.api.Model.VO;
using core.model;
using Google.Protobuf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SqlSugar;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using static core.api.Controllers.UserController;

namespace core.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly ISqlSugarClient db;
        private readonly UserInfo userInfo;
        private readonly IJWTService _jWTService;
        private readonly IRedisCacheManager _redis;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public MenuController(ISqlSugarClient db, UserInfo userInfo, IJWTService jWTService, IRedisCacheManager redis, IHttpContextAccessor httpContextAccessor)
        {
            this.db = db;
            this.userInfo = userInfo;
            _jWTService = jWTService;
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }


        #region 系统菜单资源
        /// <summary>
        /// 获取系统菜单资源
        /// </summary>
        /// <returns></returns>
        [HttpGet("sysmenulist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> SysMenulist()
        {
           
            var userid = userInfo.Getuserinfo().UserID;
            if (userid == null)
            {
                return Reuse.ErrCode(Code.Error, "暂无用户信息");
            }
            var roleids = db.Queryable<userrole>().Where(v=>v.userid == userid).Select(v=>v.roleid).ToList();
            var menuids = db.Queryable<rolepermise>().Where(v=>SqlFunc.ContainsArray(roleids,v.roleid)).Select(v=>v.menusid).ToList();
            var list = db.Queryable<sysmenu>().Where(v=>v.isLock == 0 && SqlFunc.ContainsArray(menuids, v.ID)).OrderBy(v=>v.sortID,OrderByType.Asc).ToList();
            var syslist = ListToTree(list);

            var HomeInfo = new HomeInfo {
                title = "首页",
                href = "page/welcome-1.html?t=1",
            };

            var LogoInfo = new LogoInfo
            {
                title = "管理系统",
                href = "",
                image = "",
            };
            MenusInfoResultDTO menusInfoResultDTO = new MenusInfoResultDTO();
            menusInfoResultDTO.MenuInfo = syslist;
            menusInfoResultDTO.HomeInfo = HomeInfo;
            menusInfoResultDTO.LogoInfo = LogoInfo;

            var userinfo = new 
            {
                rolename = userInfo.Getuserinfo().rolename,
                username = userInfo.Getuserinfo().UserName,
                profilePhotoUrl = userInfo.Getuserinfo().profilePhotoUrl,
            };


            return new Result() { data = new { menusInfoResultDTO, userinfo } };
        }
        public static List<SystemMenu> GetChild(int id, List<sysmenu> syslist)
        {
            List<SystemMenu> sysmenulistall = new List<SystemMenu>();
            syslist.ForEach(m =>
            {
                SystemMenu sysmenulist = new SystemMenu();
                if (m.parentID == id)
                {
                    sysmenulist.Id = (long)m.ID;
                    sysmenulist.Title = m.title;
                    sysmenulist.PId = 0;
                    sysmenulist.Href = m.linkUrl+"?id="+m.ID;
                    sysmenulist.Icon = m.icon;
                    sysmenulist.Child = GetChild(m.ID, syslist);
                    sysmenulistall.Add(sysmenulist);
                }
            });
            return sysmenulistall;
        }
        public static List<SystemMenu> ListToTree(List<sysmenu> syslist)
        {
            List<SystemMenu> sysmenulistall = new List<SystemMenu>();
            syslist.ForEach(m =>
            {
                SystemMenu sysmenulist = new SystemMenu();

                if (m.parentID == 0)
                {
                    sysmenulist.Id = m.ID;
                    sysmenulist.Title = m.title;
                    sysmenulist.PId = 0;
                    sysmenulist.Href = "";
                    sysmenulist.Icon = m.icon;
                    sysmenulist.Child = GetChild(m.ID, syslist);
                    sysmenulistall.Add(sysmenulist);
                }
            });
            return sysmenulistall;
        }
        #endregion

        #region 平台菜单列表
        /// <summary>
        /// 平台菜单列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("menulist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> menulist()
        {
            var form = HttpContext.Request.Form;
            var pid =form["pid"];

            var list = db.Queryable<sysmenu>().Where(v=>v.parentID ==Convert.ToInt32(pid)).OrderBy(v => v.sortID, OrderByType.Asc).ToList();

            var data = new List<object>();
            list.ForEach(v =>
            {
                bool a = db.Queryable<sysmenu>().Where(m => m.parentID == v.ID).Any();
                data.Add(new
                {
                    id = v.ID,
                    pid = DesHelper.Encrypt(v.parentID.ToString()),
                    title = v.title,
                    identify = v.identify,
                    linkUrl = v.linkUrl,
                    sortID = v.sortID,
                    haveChild = a,
                    isLock = CommonHelper.Getisjinyong(v.isLock),
                });
            });
            return new Result() { data = new { list = data, count = data.Count(), code = 0, msg = "" } };
        }
        #endregion

        #region 新增菜单
        /// <summary>
        /// 新增菜单
        /// </summary>
        /// <param name="pars"></param>
        /// <returns></returns>
        [HttpPost("addmenu")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> Addmenu(sysmenuadd sysmenuadd)
        {
            sysmenu sysmenu = new sysmenu();
            sysmenu.title = sysmenuadd.title;
            sysmenu.singleTitle = sysmenuadd.singleTitle;
            sysmenu.code = sysmenuadd.code;
            sysmenu.sortID =Convert.ToInt32(sysmenuadd.sortID);
            sysmenu.identify = sysmenuadd.identify;
            sysmenu.linkUrl = sysmenuadd.linkUrl;
            sysmenu.remark = sysmenuadd.remark;
            sysmenu.isLock = sysmenuadd.isLock;
            if (sysmenuadd.parentID == "")
            {
                sysmenu.parentID = 0;
                sysmenu.icon = "fa fa-window-maximize";
            }
            else
            {
                sysmenu.parentID = Convert.ToInt32(sysmenuadd.parentID);
                sysmenu.icon = "layui-icon layui-icon-cellphone";
            }

            if (sysmenuadd.Id == 0)
            {
                sysmenu.insertTime = DateTime.Now;
                sysmenu.insertUserID = userInfo.Getuserinfo().UserID;
                sysmenu.insertOrgID = userInfo.Getuserinfo().roleid;
                var id = db.Insertable(sysmenu).ExecuteReturnBigIdentity();
                if (id > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "新增成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "新增失败");
                }
            }
            else
            {
                if (sysmenuadd.parentID != "")
                {
                    var pid = Convert.ToInt32(sysmenuadd.parentID);
                    if (pid == sysmenuadd.Id)
                    {
                        return Reuse.ErrCode(Code.Error, "不能选择自己或子节点");
                    }
                }
                    
                var list = db.Queryable<sysmenu>().ToList();
                var a = CommonHelper.Getchildren(list, sysmenuadd.Id);
                sysmenu.ID = Convert.ToInt32(sysmenuadd.Id);
                sysmenu.updateTime = DateTime.Now;
                sysmenu.updateUserID = userInfo.Getuserinfo().UserID;
                sysmenu.updateOrgID = userInfo.Getuserinfo().roleid;
                var res = db.Updateable(sysmenu).UpdateColumns(it => new { it.title,it.icon,it.sortID, it.singleTitle, it.code, it.identify, it.linkUrl, it.remark, it.isLock, it.parentID, it.updateTime, it.updateUserID, it.updateOrgID }).ExecuteCommand() > 0;
                if (res)
                {
                    return Reuse.SuccessCode(Code.Success, "修改成功");
                }
                else
                {
                    return Reuse.ErrCode(Code.Error, "修改失败");
                }
            }


        }
        #endregion

        #region 修改填充
        /// <summary>
        /// 菜单修改填充
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getmenuinfo")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> getmenuinfo(int? id)
        {
            var info = await db.Queryable<sysmenu>().Where(v => v.ID == id).FirstAsync();
            var parenttitle = await db.Queryable<sysmenu>().Where(v => v.ID == info.parentID).FirstAsync();

            return new Result()
            {
                data = new
                {
                    title = info.title,
                    singleTitle = info?.singleTitle ?? "",
                    parenttitle = parenttitle?.title ?? "",
                    code = info.code,
                    sortID = info.sortID,
                    identify = info.identify,
                    linkUrl = info.linkUrl,
                    parentID = info.parentID,
                    isLock = info.isLock,
                    remark = info.remark,
                }
            };
        }
        #endregion

        #region 新增修改下拉填充
        /// <summary>
        /// 新增修改下拉填充
        /// </summary>
        /// <returns></returns>
        [HttpGet("treeselect")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> Treeselect()
        {
            var list = await db.Queryable<sysmenu>().Where(v=>v.parentID == 0).ToListAsync();
            var data = CommonHelper.listtoTreeSelect(list);
            return new Result() { data = new { data } };
        }
        #endregion

        #region 删除系统菜单
        /// <summary>
        /// 删除系统菜单
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("menudel")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> menudel(MyModel myModel)
        {
            var ishave = db.Queryable<sysmenu>().Where(v => v.parentID == Convert.ToInt32(myModel.pid)).Any();

            if (ishave)
            {
                return Reuse.ErrCode(Code.Error, "该菜单含有子集，无法删除");
            }
            else
            {
                var res = db.Deleteable<sysmenu>().Where(v => v.ID == Convert.ToInt32(myModel.pid)).ExecuteCommand();
                if (res > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "删除成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "删除失败");
                }
            }
        }
        #endregion

        #region 页面按钮列表
        /// <summary>
        /// 页面按钮列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("stsbuttonlist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> stsbuttonlist()
        {
            var form = HttpContext.Request.Form;
            var id = form["id"];
            var list = db.Queryable<sysmenubutton>().Where(v=>v.pid == id).OrderBy(v => v.sortID, OrderByType.Asc).ToList();

            var data = list.Select(v => new
            {
                id = v.ID,
                title = v.title,
                btevent = v.btevent,
                style = v.style,
                sortID = v.sortID,
                type = v.type == 0? "<span style=\"color:#5FB878\">表格按钮</span>" : "<span style=\"color:#31BDEC\">头部按钮</span>",
                isLock = CommonHelper.Getisjinyong(v.isLock),
            });
            return new Result() { data = new { data = data, count = data.Count(), code = 0, msg = "" } };
        }
        #endregion

        #region 页面添加新增/修改/删除/按钮
        /// <summary>
        /// 添加新增按钮
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("addadd")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> addadd(MyModel myModel)
        {
            sysmenubutton sysmenubutton = new sysmenubutton();
            sysmenubutton.title = "新增";
            sysmenubutton.btevent = "add";
            sysmenubutton.style = "background-color:#28a745";
            sysmenubutton.type = 1;
            sysmenubutton.sortID = 1;
            sysmenubutton.isLock =0;
            sysmenubutton.pid = Convert.ToInt32(myModel.pid);

            var id = db.Insertable(sysmenubutton).ExecuteReturnBigIdentity();
            if (id > 0)
            {
                return Reuse.SuccessCode(Code.Success, "新增成功");
            }
            else
            {

                return Reuse.ErrCode(Code.Error, "新增失败");
            }
        }
        /// <summary>
        /// 添加修改按钮
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("addedit")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> addedit(MyModel myModel)
        {
            sysmenubutton sysmenubutton = new sysmenubutton();
            sysmenubutton.title = "修改";
            sysmenubutton.btevent = "edit";
            sysmenubutton.style = "background-color:#28a745";
            sysmenubutton.type = 0;
            sysmenubutton.sortID = 2;
            sysmenubutton.isLock = 0;
            sysmenubutton.pid = Convert.ToInt32(myModel.pid);

            var id = db.Insertable(sysmenubutton).ExecuteReturnBigIdentity();
            if (id > 0)
            {
                return Reuse.SuccessCode(Code.Success, "新增成功");
            }
            else
            {

                return Reuse.ErrCode(Code.Error, "新增失败");
            }
        }
        /// <summary>
        /// 添加删除按钮
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("adddel")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> adddel(MyModel myModel)
        {
            sysmenubutton sysmenubutton = new sysmenubutton();
            sysmenubutton.title = "删除";
            sysmenubutton.btevent = "del";
            sysmenubutton.style = "background-color:#FF5722";
            sysmenubutton.type = 0;
            sysmenubutton.sortID = 999;
            sysmenubutton.isLock = 0;
            sysmenubutton.pid = Convert.ToInt32(myModel.pid);

            var id = db.Insertable(sysmenubutton).ExecuteReturnBigIdentity();
            if (id > 0)
            {
                return Reuse.SuccessCode(Code.Success, "新增成功");
            }
            else
            {

                return Reuse.ErrCode(Code.Error, "新增失败");
            }
        }
        #endregion

        #region 新增修改页面按钮
        /// <summary>
        /// 新增修改页面按钮
        /// </summary>
        /// <param name="sysbuttonadd"></param>
        /// <returns></returns>
        [HttpPost("addsysbutton")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> Addsysbutton(sysbuttonadd sysbuttonadd)
        {
            sysmenubutton sysmenubutton = new sysmenubutton();
            sysmenubutton.title = sysbuttonadd.title;
            sysmenubutton.btevent = sysbuttonadd.btevent;
            sysmenubutton.style = sysbuttonadd.style;
            sysmenubutton.type = Convert.ToInt32(sysbuttonadd.type);
            sysmenubutton.sortID = Convert.ToInt32(sysbuttonadd.sortID);
            sysmenubutton.isLock = Convert.ToInt32(sysbuttonadd.isLock);
            sysmenubutton.pid = Convert.ToInt32(sysbuttonadd.pid);
            if (sysbuttonadd.ID == 0)
            {
                sysmenubutton.insertTime = DateTime.Now;
                sysmenubutton.insertUserID = userInfo.Getuserinfo().UserID;
                sysmenubutton.insertOrgID = 1;

                var id = db.Insertable(sysmenubutton).ExecuteReturnBigIdentity();
                if (id > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "新增成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "新增失败");
                }
            }
            else
            {
                sysmenubutton.ID = Convert.ToInt32(sysbuttonadd.ID);
                sysmenubutton.updateTime = DateTime.Now;
                sysmenubutton.updateUserID = userInfo.Getuserinfo().UserID;
                sysmenubutton.updateOrgID = 1;
                var res = db.Updateable(sysmenubutton).UpdateColumns(it => new { it.title, it.singleTitle, it.btevent, it.style, it.sortID, it.isLock, it.updateTime, it.updateUserID, it.updateOrgID }).ExecuteCommand() > 0;
                if (res)
                {
                    return Reuse.SuccessCode(Code.Success, "修改成功");
                }
                else
                {
                    return Reuse.ErrCode(Code.Error, "修改失败");
                }
            }

        }
        #endregion

        #region 页面按钮修改填充
        /// <summary>
        /// 页面按钮修改填充
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getbtninfo")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> getbtninfo(int? id)
        {
            var info = await db.Queryable<sysmenubutton>().Where(v => v.ID == id).FirstAsync();

            return new Result()
            {
                data = new
                {
                    title = info.title,
                    btevent = info.btevent,
                    style = info.style,
                    sortID = info.sortID,
                    type = info.type,
                    isLock = info.isLock,
                }
            };
        }
        #endregion

        #region 删除页面按钮
        /// <summary>
        /// 删除页面按钮
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("delbutton")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> delbutton(MyModel myModel)
        {
           var res =  db.Deleteable<sysmenubutton>().Where(v =>v.ID == Convert.ToInt32(myModel.pid)).ExecuteCommand();
            if (res > 0)
            {
                return Reuse.SuccessCode(Code.Success, "删除成功");
            }
            else
            {

                return Reuse.ErrCode(Code.Error, "删除失败");
            }
        }
        #endregion

        #region 获取页面按钮和表格字段
        /// <summary>
        /// 获取页面按钮和表格字段
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getbtn")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> getbtn(int? id)
        {
            var userid = userInfo.Getuserinfo().UserID;
            if (userid == null)
            {
                return Reuse.ErrCode(Code.Error, "暂无用户信息");
            }
            var roleids = db.Queryable<userrole>().Where(v => v.userid == userid).Select(v => v.roleid).ToList();
            var buttons = db.Queryable<rolepermise>().Where(v => SqlFunc.ContainsArray(roleids, v.roleid) && v.menusid == id).Select(v => new { v.buttonsid ,v.tbuttonsid,v.filedsid,v.sfiledid}).First();
            var btnids = buttons.tbuttonsid.Split(',').ToList();
            var lineids = buttons.buttonsid.Split(',').ToList();
            var colsids = buttons.filedsid.Split(',').ToList();
            var searchids = buttons.sfiledid.Split(',').ToList();

            //搜索字段
            var searchfidle = db.Queryable<searchfield>().Where(v => v.pid == id && v.isLock == 0 && SqlFunc.ContainsArray(searchids, v.ID)).OrderBy(v => v.sortID, OrderByType.Asc).ToList();
            Searchcommon searchcommon = new Searchcommon(db);
            var sfidle= searchcommon.Getsearchwhere(searchfidle);
            //头部按钮
            var btnlist = db.Queryable<sysmenubutton>().Where(v => v.pid == id && v.isLock == 0 && SqlFunc.ContainsArray(btnids,v.ID)).OrderBy(v => v.sortID, OrderByType.Asc).ToList();
            //表格按钮
            var linelist = db.Queryable<sysmenubutton>().Where(v => v.pid == id && v.isLock == 0 && SqlFunc.ContainsArray(lineids, v.ID)).OrderBy(v => v.sortID, OrderByType.Asc).ToList();
            //表格字段
            var colslist = db.Queryable<sysmenufiled>().Where(v => v.pid == id && v.isLock == 0 && SqlFunc.ContainsArray(colsids, v.ID)).OrderBy(v => v.sortID, OrderByType.Asc).ToList();
            var list_tfields = new List<Cols>();
            {
                foreach (var v in colslist)
                {
                    if (v != null)
                    {

                        var cols = new Cols
                        {
                            title = v.title,
                            type = v.type,
                            field = v.field,
                            width = v.width,
                            style = v.style,
                            templet = v.templet,
                            minWidth = v.minWidth,
                            eventName = v.eventName,
                            fixedAlign = v.fixedAlign,
                            unresize = true,
                            rowspan= 1,
                            colspan = 1,
                            align = v.align,
                            sort = v.sort == "true" ? true : false,
                            totalRow = v.totalRow == "true" ? true : false,
                            };
                        list_tfields.Add(cols);

                    }

                }
            }

               

            var data = new
            {
                btnlist,
                linelist,
                sfidle,
                cooa = JsonConvert.SerializeObject(list_tfields),
            };

            return new Result() { data = data };
        }
        #endregion

        #region 页面表头字段列表
        /// <summary>
        /// 页面表头字段列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("stsmenufiledlist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> stsmenufiledlist()
        {
            var form = HttpContext.Request.Form;
            var id = form["id"];
            var list = db.Queryable<sysmenufiled>().Where(v => v.pid ==id).OrderBy(v => v.sortID, OrderByType.Asc).ToList();

            var data = list.Select(v => new
            {
                id = v.ID,
                title = v.title,
                field = v.field,
                style = v.style,
                sortID = v.sortID,
                type = v.type,
                width = v.width,
                fixedAlign = v.fixedAlign,
                sort = v.sort,
                eventName = v.eventName,
                align = v.align,
                templet = v.templet,
                totalRow = v.totalRow,
                isLock = CommonHelper.Getisjinyong(v.isLock),
            });
            return new Result() { data = new { data = data, count = data.Count(), code = 0, msg = "" } };
        }
        #endregion

        #region 新增操作按钮
        /// <summary>
        /// 新增操作按钮
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("addtempt")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> addtempt(MyModel myModel)
        {
            sysmenufiled sysmenufiled = new sysmenufiled();
            sysmenufiled.title = "操作";
            sysmenufiled.width = 200.ToString();
            sysmenufiled.sortID = 999;
            sysmenufiled.isLock = 0;
            sysmenufiled.sort = "flase";
            sysmenufiled.totalRow = "flase";
            sysmenufiled.align = "center";
            sysmenufiled.templet = ".toolbar";
            sysmenufiled.pid = Convert.ToInt32(myModel.pid);
            sysmenufiled.insertTime = DateTime.Now;
            sysmenufiled.insertUserID = userInfo.Getuserinfo().UserID;
            sysmenufiled.insertOrgID = 1;
            var res = db.Insertable(sysmenufiled).ExecuteReturnBigIdentity();
            if (res > 0)
            {
                return Reuse.SuccessCode(Code.Success, "新增成功");
            }
            else
            {

                return Reuse.ErrCode(Code.Error, "新增失败");
            }
        }
        #endregion

        #region 新增多选按钮
        /// <summary>
        /// 新增多选按钮
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("addcheckbox")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> addcheckbox(MyModel myModel)
        {
            sysmenufiled sysmenufiled = new sysmenufiled();
            sysmenufiled.title = "选择";
            sysmenufiled.width =48.ToString();
            sysmenufiled.type = "checkbox";
            sysmenufiled.sortID = 1;
            sysmenufiled.sort = "flase";
            sysmenufiled.totalRow = "flase";
            sysmenufiled.align = "center";
            sysmenufiled.isLock = 0;
            sysmenufiled.pid =Convert.ToInt32(myModel.pid);
            sysmenufiled.insertTime = DateTime.Now;
            sysmenufiled.insertUserID = userInfo.Getuserinfo().UserID;
            sysmenufiled.insertOrgID = 1;
            var res = db.Insertable(sysmenufiled).ExecuteReturnBigIdentity();
            if (res > 0)
            {
                return Reuse.SuccessCode(Code.Success, "新增成功");
            }
            else
            {

                return Reuse.ErrCode(Code.Error, "新增失败");
            }
        }

        #endregion

        #region 新增修改表格字段
        /// <summary>
        /// 新增修改表格字段
        /// </summary>
        /// <param name="sysmenufiledadd"></param>
        /// <returns></returns>
        [HttpPost("addmenufiled")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> addmenufiled(sysmenufiledadd sysmenufiledadd)
        {
            #region 新增修改字段
            sysmenufiled sysmenufiled = new sysmenufiled();
            sysmenufiled.title = sysmenufiledadd.title;
            sysmenufiled.field = sysmenufiledadd.field;
            sysmenufiled.width = sysmenufiledadd.width;
            sysmenufiled.eventName = sysmenufiledadd.eventName;
            sysmenufiled.style = sysmenufiledadd.style;
            sysmenufiled.templet = sysmenufiledadd.templet;
            sysmenufiled.sortID = Convert.ToInt32(sysmenufiledadd.sortID);
            sysmenufiled.type = sysmenufiledadd.type;
            sysmenufiled.align = sysmenufiledadd.align;
            sysmenufiled.fixedAlign = sysmenufiledadd.fixedAlign;
            sysmenufiled.sort = sysmenufiledadd.sort;
            sysmenufiled.totalRow = sysmenufiledadd.totalRow;
            sysmenufiled.isLock = Convert.ToInt32(sysmenufiledadd.isLock); 
            sysmenufiled.pid = Convert.ToInt32( sysmenufiledadd.pid);
            #endregion

            if (sysmenufiledadd.Id == 0)
            {
                sysmenufiled.insertTime = DateTime.Now;
                sysmenufiled.insertUserID = userInfo.Getuserinfo().UserID;
                sysmenufiled.insertOrgID = userInfo.Getuserinfo().roleid;
                var id = db.Insertable(sysmenufiled).ExecuteReturnBigIdentity();
                if (id > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "新增成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "新增失败");
                }
            }
            else
            {
                sysmenufiled.ID = Convert.ToInt32(sysmenufiledadd.Id);
                sysmenufiled.updateTime = DateTime.Now;
                sysmenufiled.updateUserID = userInfo.Getuserinfo().UserID;
                sysmenufiled.updateOrgID = userInfo.Getuserinfo().roleid; ;
                var res = db.Updateable(sysmenufiled).UpdateColumns(it => new { it.title, it.field, it.width, it.style, it.sortID, it.isLock, it.eventName, it.templet, it.type, it.align, it.fixedAlign, it.sort, it.totalRow, it.updateTime, it.updateUserID, it.updateOrgID }).ExecuteCommand() > 0;
                if (res)
                {
                    return Reuse.SuccessCode(Code.Success, "修改成功");
                }
                else
                {
                    return Reuse.ErrCode(Code.Error, "修改失败");
                }
            }
        }
        #endregion

        #region 表格字段修改填充
        /// <summary>
        /// 表格字段修改填充
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getfieldinfo")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> getfieldinfo(int? id)
        {
            var info = await db.Queryable<sysmenufiled>().Where(v => v.ID == id).FirstAsync();

            return new Result()
            {
                data = new
                {
                    title = info.title,
                    field = info.field,
                    style = info.style,
                    sortID = info.sortID,
                    type = info.type,
                    isLock = info.isLock,
                    width = info.width,
                    eventName = info.eventName,
                    templet = info.templet,
                    align = info.align,
                    fixedAlign = info.fixedAlign,
                    sort = info.sort,
                    totalRow = info.totalRow,
                }
            };
        }
        #endregion

        #region 删除表格字段
        /// <summary>
        /// 删除表格字段
        /// </summary>
        /// <param name="myModel"></param>
        /// <returns></returns>
        [HttpPost("delfield")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> delfield(MyModel myModel)
        {
            var res = db.Deleteable<sysmenufiled>().Where(v => v.ID == Convert.ToInt32(myModel.pid)).ExecuteCommand();
            if (res > 0)
            {
                return Reuse.SuccessCode(Code.Success, "删除成功");
            }
            else
            {

                return Reuse.ErrCode(Code.Error, "删除失败");
            }
        }
        #endregion

        #region 搜索字段列表
        /// <summary>
        /// 搜索字段列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("searchlist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> searchlist()
        {
            var form = HttpContext.Request.Form;
            var page = form["page"];
            var limit = form["limit"];
            var id = form["id"];
            int totalCount = 0;

            var list = db.Queryable<searchfield>().Where(v=>v.pid == Convert.ToInt32(id)).OrderBy(v => v.sortID, OrderByType.Asc).ToPageList(Convert.ToInt32(page), Convert.ToInt32(limit), ref totalCount);
           
            var data = list.Select(v => new {

                id = v.ID,
                title = v.title,
                field = v.field,
                stype = CommonHelper.Gesearchtype(v.stype),
                timerange = v.timerange,
                sortID = v.sortID,
                isLock = CommonHelper.Getisjinyong(v.isLock),
                insertTime = CommonHelper.ConvertSqlTime(v.insertTime),

            });
            return new Result() { data = new { data = data, count = totalCount, code = 0, msg = "" } };
        }
        #endregion

        #region 新增修改搜索字段
        /// <summary>
        /// 新增修改搜索字段
        /// </summary>
        /// <param name="searchadd"></param>
        /// <returns></returns>
        [HttpPost("addsearch")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> addsearch(searchadd searchadd)
        {
            searchfield searchfield = new searchfield();
            searchfield.pid = Convert.ToInt32(searchadd.pid);
            searchfield.title = searchadd.title;
            searchfield.field = searchadd.field;
            searchfield.timeformat = searchadd.timeformat;
            searchfield.stype = searchadd.stype;
            searchfield.timerange = searchadd.timerange;
            searchfield.sortID = Convert.ToInt32(searchadd.sortID);
            searchfield.isLock = Convert.ToInt32(searchadd.isLock);

            if(searchadd.Id == 0)
            {
                searchfield.insertTime = DateTime.Now;
                searchfield.insertUserID = userInfo.Getuserinfo().UserID;
                searchfield.insertOrgID = userInfo.Getuserinfo().roleid;
                var id = db.Insertable(searchfield).ExecuteReturnBigIdentity();
                if (id > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "新增成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "新增失败");
                }
            }
            else
            {
                searchfield.ID = searchadd.Id;
                searchfield.updateTime = DateTime.Now;
                searchfield.updateUserID = userInfo.Getuserinfo().UserID;
                searchfield.updateOrgID = userInfo.Getuserinfo().roleid; ;
                var res = db.Updateable(searchfield).UpdateColumns(it => new { it.title, it.timeformat,it.field, it.stype, it.timerange, it.sortID, it.isLock,it.updateTime, it.updateUserID, it.updateOrgID }).ExecuteCommand() > 0;
                if (res)
                {
                    return Reuse.SuccessCode(Code.Success, "修改成功");
                }
                else
                {
                    return Reuse.ErrCode(Code.Error, "修改失败");
                }

            }
        }
        #endregion

        #region 搜索字段修改填充
        /// <summary>
        /// 搜索字段修改填充
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getsearchinfo")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> getsearchinfo(int? id)
        {
            var info = await db.Queryable<searchfield>().Where(v => v.ID == id).FirstAsync();

            return new Result()
            {
                data = new
                {
                    title = info.title,
                    timeformat = info.timeformat,
                    field = info.field,
                    sortID = info.sortID,
                    timerange = info.timerange,
                    isLock = info.isLock,
                    stype = info.stype,
                }
            };
        }
        #endregion

        #region 分配权限列表
        /// <summary>
        /// 分配权限列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("RolePer")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> RolePer(int id)
        {
            var menulist = db.Queryable<sysmenu>().Select(v => new { v.ID, v.title,v.sortID,v.singleTitle,v.parentID }).OrderBy(v => v.sortID, OrderByType.Asc).ToList();

            //表格字段
            var menufield = db.Queryable<sysmenufiled>().Where(m => SqlFunc.ContainsArray(menulist.Select(s => s.ID).ToArray(), m.pid)).ToList();
            //页面按钮
            var topbutton = db.Queryable<sysmenubutton>().Where(m => SqlFunc.ContainsArray(menulist.Select(s => s.ID).ToArray(), m.pid)).ToList();
            //搜索字段
            var searchfield = db.Queryable<searchfield>().Where(m => SqlFunc.ContainsArray(menulist.Select(s => s.ID).ToArray(), m.pid)).ToList();


            List<object> list = new List<object>(), sField, tField, tBtn, fBtn;
            //已有权限
            var havepromise = db.Queryable<rolepermise>().Where(v => v.roleid == id).ToList();
            var checkSysids = havepromise.Select(V => V.menusid).ToList();
            menulist.ForEach(v =>
            {
                var pfileds = havepromise.Where(m => m.menusid == v.ID).ToList();
                sField = new List<object>();
                tField = new List<object>();
                tBtn = new List<object>();
                fBtn = new List<object>();

                // 默认选中
                List<string> sFieldChecked = new List<string>();
                List<string> tFieldChecked = new List<string>();
                List<string> tBtnChecked = new List<string>();
                List<string> fBtnChecked = new List<string>();

                if (pfileds.Count > 0)
                {
                    pfileds.ForEach(n =>
                    {
                        if (n.sfiledid != null)
                            sFieldChecked = n.sfiledid.Split(',').ToList();
                        if (n.filedsid != null)
                            tFieldChecked = n.filedsid.Split(',').ToList();
                        if (n.tbuttonsid != null)
                            tBtnChecked = n.tbuttonsid.Split(',').ToList();
                        if (n.buttonsid != null)
                            fBtnChecked = n.buttonsid.Split(',').ToList();
                    });
                }

                //搜索字段
                searchfield.Where(n => n.pid == v.ID).ToList().ForEach(n =>
                {
                    sField.Add(new
                    {
                        id = n.ID,
                        n.title,
                        _checked = sFieldChecked.Contains(n.ID.ToString())
                    });
                });
                //表格字段
                menufield.Where(n=>n.pid == v.ID).ToList().ForEach(n =>
                {
                    tField.Add(new
                    {
                        id = n.ID,
                        n.title,
                        _checked = tFieldChecked.Contains(n.ID.ToString())
                    });
                });
                //顶部按钮
                topbutton.Where(m => m.type == 1 && m.pid == v.ID).ToList().ForEach(n =>
                {
                    tBtn.Add(new
                    {
                        id = n.ID,
                        n.title,
                        _checked = tBtnChecked.Contains(n.ID.ToString())
                    });
                });
                //行按钮
                topbutton.Where(m => m.type == 0 && m.pid == v.ID).ToList().ForEach(n =>
                {
                    fBtn.Add(new
                    {
                        id = n.ID,
                        n.title,
                        _checked = fBtnChecked.Contains(n.ID.ToString())
                    });
                });

                list.Add(new
                {
                    id = v.ID,
                    title = !string.IsNullOrEmpty(v.singleTitle) ? v.singleTitle : v.title,
                    sField,
                    tField,
                    tBtn,
                    fBtn,
                    pid = v.parentID,
                    haveChild = menulist.Any(n => n.parentID.Equals(v.ID)),
                    open = menulist.Any(n => n.parentID.Equals(v.ID))
                });

            });

            return new Result() { data = new { list, checkSysids } };
        }
        #endregion

        #region 角色分配权限
        /// <summary>
        /// 角色分配权限 
        /// </summary>
        /// <param name="roleID"></param>
        /// <param name="rolepromises"></param>
        /// <returns></returns>
        [HttpPost("setrolepermise")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> setrolepermise(Root roots)
        {
            var roleid = roots.roleID;
            var list = roots.list.ToList();

            var res = db.Ado.UseTran(() =>
            {
                List<long> tran_sysids = new List<long>();

                list.ForEach(n =>
                {
                    long sysID = n.sysID;
                    tran_sysids.Add(sysID);
                    var model = db.Queryable<rolepermise>().Where(n => n.roleid ==Convert.ToInt32(roleid) && n.menusid == sysID).ToList().FirstOrDefault();
                    if (model != null)
                    {
                        model.updateTime = DateTime.Now;
                        model.updateUserID = 1;
                        model.updateOrgID = 1;
                    }
                    else
                    {
                        model = new rolepermise();
                        model.roleid = Convert.ToInt32(roleid);
                        model.menusid = sysID;

                        model.insertTime = DateTime.Now;
                        model.insertUserID = 1;
                        model.insertOrgID = 1;
                    }
                    model.filedsid = n.tField;
                    model.buttonsid = n.fBtn;
                    model.tbuttonsid = n.tBtn;
                    model.sfiledid = n.sField;
                    db.Saveable(model).UpdateIgnoreColumns(it => new {it.insertTime, it.insertUserID, it.insertOrgID, it.roleid, it.menusid }).ExecuteReturnEntity();
                });
                var exit_sysids = db.Queryable<rolepermise>().Where(m => m.roleid == Convert.ToInt32(roleid)).Select(m => m.menusid).ToList();

                var del_sysids = tran_sysids.Count == 0 ? exit_sysids : exit_sysids.Except(tran_sysids).ToList();
                if (del_sysids.Count > 0)
                {
                    db.Deleteable<rolepermise>().Where(m => del_sysids.Contains(m.menusid) && m.roleid == Convert.ToInt32(roleid)).ExecuteCommand();
                }
            });

            if (res.IsSuccess)
                return Reuse.SuccessCode("设置成功");
            else
                return Reuse.ErrCode(Code.Error, "设置失败");

        }
        #endregion

    }
}
