﻿using core.api.Conmon;
using core.api.help;
using core.api.Model;
using core.api.Model.VO;
using core.quartz;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using NPOI.POIFS.Crypt.Dsig;
using NPOI.POIFS.Properties;
using NPOI.SS.Formula.Functions;
using Quartz.Impl;
using Quartz;
using SqlSugar;
using System.Linq;
using Code = core.api.Conmon.Code;
using Quartz.Impl.Matchers;
using static Quartz.Logging.OperationName;

namespace core.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly ISqlSugarClient db;
        private readonly UserInfo userInfo;
        private readonly IJWTService _jWTService;
        private readonly IRedisCacheManager _redis;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public RoleController(ISqlSugarClient db, UserInfo userInfo, IJWTService jWTService, IRedisCacheManager redis, IHttpContextAccessor httpContextAccessor)
        {
            this.db = db;
            this.userInfo = userInfo;
            _jWTService = jWTService;
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }

        #region huifu
        /// <summary>
        /// 恢复某个任务
        /// </summary>
        /// <returns></returns>
        [HttpPost("huifu")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> huifu()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler();
            var jobKeys = scheduler.GetJobKeys(GroupMatcher<JobKey>.AnyGroup());

            // 获取要暂停的任务和触发器  


            foreach (var jobKey in await jobKeys)
            {
                // 获取指定 Job 的名称
                var jobDetail = await scheduler.GetJobDetail(jobKey);
                var jobName = jobDetail.Key.Name;

                var a = scheduler.ResumeJob(new JobKey(jobDetail.Key.Name, jobDetail.Key.Group));
            }

            var tree = db.Queryable<sysmenu>().ToTree(it => it.Child, it => it.parentID, 0);
            return new Result() { data = tree };
        }
        #endregion

        #region 属性查询
        /// <summary>
        /// 属性查询
        /// </summary>
        /// <returns></returns>
        [HttpPost("tree")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> tree()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler();
            var jobKeys = scheduler.GetJobKeys(GroupMatcher<JobKey>.AnyGroup());

            // 获取要暂停的任务和触发器  


            foreach (var jobKey in await jobKeys)
            {
                // 获取指定 Job 的名称
                var jobDetail = await scheduler.GetJobDetail(jobKey);
                var jobName = jobDetail.Key.Name;


                var a = scheduler.PauseJob(new JobKey(jobDetail.Key.Name, jobDetail.Key.Group));
            }

            var tree = db.Queryable<sysmenu>().ToTree(it => it.Child, it => it.parentID, 0);
            return new Result() { data = tree };
        }
        #endregion

        #region 搜索条件
        /// <summary>
        /// 搜索条件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ApiExplorerSettings(GroupName = "pc端")]
        public string Getsearchwhere()
        {
            string where = " 1=1 ";
            var form = HttpContext.Request.Form;
            var formDictionary = HttpContext.Request.Form.ToDictionary(key => key, value => HttpContext.Request.Form[""]);
            var values = formDictionary.Keys.ToList();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            values.ForEach(m =>
            {
                var k = m.Key;
                var v = m.Value;
                dict.Add(k, v);
            });

            dict.TryGetValue("title", out string title);
            if (!string.IsNullOrEmpty(title))
            {
                where += $" and title like '%{title}%'";
            }

            return where;
        } 
        #endregion

        #region 角色列表
        /// <summary>
        /// 角色列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("rolelist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> rolelist()
        {
            var form = HttpContext.Request.Form;
            var page = form["page"];
            var limit = form["limit"];
            int totalCount = 0;
            var where = Getsearchwhere();
           var list = db.Queryable<sysrole>().Where(where).OrderBy(v => v.sortID, OrderByType.Asc).ToPageList(Convert.ToInt32(page), Convert.ToInt32(limit), ref totalCount);

            var data = list.Select(v => new
            {
                id = v.ID,
                title = v.title,
                singleTitle = v.singleTitle,
                sortID = v.sortID,
                isLock = CommonHelper.Getisjinyong(v.isLock),
            });
            return new Result() { data = new { data = data, count = totalCount, code = 0, msg = "" } };
        }
        #endregion

        #region 新增修改角色
        /// <summary>
        /// 新增修改角色
        /// </summary>
        /// <param name="sysroleAdd"></param>
        /// <returns></returns>
        [HttpPost("addrole")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> Addrole(sysroleadd sysroleAdd)
        {
            sysrole sysrole = new sysrole();
            sysrole.title = sysroleAdd.title;
            sysrole.singleTitle = sysroleAdd.singleTitle;
            sysrole.sortID = sysroleAdd.sortID;
            sysrole.isLock = sysroleAdd.isLock;
            if (sysroleAdd.ID == 0)
            {
                sysrole.insertTime = DateTime.Now;
                sysrole.insertUserID = userInfo.Getuserinfo().UserID;
                sysrole.insertOrgID = userInfo.Getuserinfo().roleid;

                var id = db.Insertable(sysrole).ExecuteReturnBigIdentity();
                if (id > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "新增成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "新增失败");
                }
            }
            return Reuse.ErrCode(Code.Error, "新增失败");
        }
        #endregion

        #region 获取左侧树
        /// <summary>
        /// 获取左侧树
        /// </summary>
        /// <returns></returns>
        [HttpPost("gettree")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> gettree()
        {
            var list = db.Queryable<sysrole>().ToList();

            var data = list.Select(v => new {
                title = v.title,
                id = v.ID,
            });


            return new Result() { data = data };
        }
        #endregion


    }
}
