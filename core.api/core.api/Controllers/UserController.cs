﻿using core.api.Conmon;
using System.IO;
using core.api.filter;
using core.api.help;
using core.api.Model;
using core.api.Model.VO;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SqlSugar;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NPOI.SS.UserModel;
using Microsoft.AspNetCore.StaticFiles;
using NPOI.XSSF.UserModel;

namespace core.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class  UserController : ControllerBase
    {
        private  readonly ISqlSugarClient db;
        private readonly UserInfo userInfo;
        private readonly IJWTService _jWTService;
        private  readonly IRedisCacheManager _redis;
        private  readonly IHttpContextAccessor _httpContextAccessor;
        private readonly string HttpsUrl;
        public UserController(ISqlSugarClient db, UserInfo userInfo, IJWTService jWTService, IRedisCacheManager redis, IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            this.db = db;
            this.userInfo = userInfo;
            _jWTService = jWTService;
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;

            var httpsSettings = configuration["Kestrel:Endpoints:Https:Url"];
            HttpsUrl = httpsSettings;
        }

        #region 生成图片验证码
        /// <summary>
        /// 生成图片验证码
        /// </summary>
        /// <param name="codeType">验证码类型 0：纯数字 1：数字+字母 2：数字运算 默认1</param>
        /// <returns></returns>
        [HttpGet("captcha")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public Result Captcha(string codeType = "0")
        {
            string uuid = Guid.NewGuid().ToString().Replace("-", "");
            var codeInfo = new VerifyCode();
            if (codeType == "1")
            {
                codeInfo = CharCode.CreateVerifyCode(4, VerifyCodeType.CHAR);
            }
            else if (codeType == "2")
            {
                codeInfo = CharCode.CreateVerifyCode(4, VerifyCodeType.ARITH);
            }
            else if (codeType == "0")
            {
                codeInfo = CharCode.CreateVerifyCode(4, VerifyCodeType.NUM);
            }
                _redis.Set(uuid, codeInfo, TimeSpan.FromMinutes(5));
            var obj = new { uuid, img = codeInfo.Base64Str };
            return new Result() { data = obj };
        }
        #endregion


        #region 用户登录
        /// <summary>
        /// 用户登录获取token
        /// </summary>
        /// <param name="loginBody"></param>
        /// <returns></returns>
        [HttpPost("login")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> Login(LoginBody loginBody)
        {
            var username = loginBody.username;
            var password = loginBody.password;
            var captcha = loginBody.captcha;
            var uuid = loginBody.uuid;

            var codeInfo = new VerifyCode();
            var code =  _redis.Get<VerifyCode>(uuid);
            if(code == null) {
                return Reuse.ErrCode(Code.Error, "验证码过期");
            }
            var path = HttpsUrl + "/wwwroot" + "/phton" + "/";
            if (code.Code.Equals(captcha))
            {
                var userinfo = await db.Queryable<users>().Where(v => v.userName == username).FirstAsync();
                if(userinfo == null)
                {
                    return Reuse.ErrCode(Code.Error, "无用户信息");
                }
                if (username == userinfo.userName && password == userinfo.userPassword)
                {
                    var roleid = db.Queryable<userrole>().Where(v=>v.userid == userinfo.ID).First();
                    var rolename = db.Queryable<sysrole>().Where(v=>v.ID == roleid.roleid).First().title; 
                    string token = _jWTService.GetToken(username, password);
                    //用户缓存信息
                    var redismodel = new Redismodel
                    {
                        UserID = userinfo.ID,
                        name = userinfo.name,
                        UserName = userinfo.userName,
                        roleid = roleid.roleid,
                        rolename = rolename,
                        profilePhotoUrl = path+ userinfo.profilePhotoUrl,
                    };
                    _redis.Set(token, redismodel, TimeSpan.FromMinutes(20));

                    return Reuse.SuccessCode(token, "登陆成功");
                }
                else
                {
                    return Reuse.ErrCode(Code.NotFindUser, "用户名或密码错误");
                }
            }
            else
            {
                return Reuse.ErrCode(Code.Error, "验证码错误");
            }

        }
        #endregion

        #region 新增修改用户
        /// <summary>
        /// 新增修改用户
        /// </summary>
        /// <param name="adduser"></param>
        /// <returns></returns>
        [HttpPost("adduser")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> adduser(adduser adduser)
        {
            var fileName = "";
            users users = new users();

            if (CommonHelper.isbase64(adduser.pfile))
            {
                 fileName = CommonHelper.SaveImg(adduser.pfile);
                users.profilePhotoUrl = fileName;
            }

            var path = Directory.GetCurrentDirectory() + "/wwwroot" + "/phton"+"/";

            users.userName = adduser.userName;
            users.name = adduser.name;
            users.userPassword = adduser.userPassword;
            users.code = adduser.code;
            users.mobilePhone = adduser.mobilePhone;
            users.sortID = Convert.ToInt32(adduser.sortID);
            users.userStateID = Convert.ToInt32(adduser.isLock);

            if (adduser.Id == 0)
            {

                users.insertTime = DateTime.Now;
                users.insertUserID = userInfo.Getuserinfo().UserID;
                users.insertOrgID = userInfo.Getuserinfo().roleid;

                var id = db.Insertable(users).ExecuteReturnBigIdentity();
                if (id > 0)
                {
                    return Reuse.SuccessCode(Code.Success, "新增成功");
                }
                else
                {

                    return Reuse.ErrCode(Code.Error, "新增失败");
                }
            }
            else
            {
                var model = db.Queryable<users>().Where(v => v.ID == adduser.Id).First();

               
                if (model.profilePhotoUrl != null)
                {
                    if (!CommonHelper.isbase64(adduser.pfile))
                    {
                        users.profilePhotoUrl = model.profilePhotoUrl;
                    }
                    else
                    {
                        if (System.IO.File.Exists(path + model.profilePhotoUrl))
                        {
                            System.IO.File.Delete(path + model.profilePhotoUrl);
                        }
                    }
                }

               
                users.ID = adduser.Id;
                users.updateTime = DateTime.Now;
                users.updateUserID = userInfo.Getuserinfo().UserID;
                users.updateOrgID = userInfo.Getuserinfo().roleid;

                var res = db.Updateable(users).UpdateColumns(it => new { it.userName, it.name, it.sortID, it.userPassword, it.code, it.profilePhotoUrl, it.mobilePhone, it.remark, it.userStateID, it.updateTime, it.updateUserID, it.updateOrgID }).ExecuteCommand() > 0;
                if (res)
                {
                    return Reuse.SuccessCode(Code.Success, "修改成功");
                }
                else
                {
                    return Reuse.ErrCode(Code.Error, "修改失败");
                }
            }
        }
        #endregion

        #region 修改填充
        /// <summary>
        /// 修改填充
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("getuserinfo")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> getuserinfo(int? id)
        {

            var info = await db.Queryable<users>().Where(v => v.ID == id).FirstAsync();
             var path = HttpsUrl + "/wwwroot" + "/phton" +"/";
            return new Result()
            {
                data = new
                {
                    userName = info.userName,
                    name = info.name,
                    userPassword = info.userPassword,
                    code = info.code,
                    mobilePhone = info.mobilePhone,
                    sortID = info.sortID,
                    isLock = info.userStateID,
                    profilePhotoUrl = path + info.profilePhotoUrl,
                    remark = info.remark,
                }
            };
        }
        #endregion

        #region 获取用户列表
        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("userlist")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> userlist()
        {
            var form = HttpContext.Request.Form;
            var page = form["page"];
            var limit = form["limit"];
            int totalCount = 0;
            var where = Getsearchwhere();
            var userinfo = db.Queryable<users>().Where(where).ToPageList(Convert.ToInt32(page), Convert.ToInt32(limit), ref totalCount);
            var userids = userinfo.Select(V => V.ID).ToList();
            var roleids = db.Queryable<userrole>().Where(v => SqlFunc.ContainsArray(userids, v.userid)).ToList();
            var roleinfo = db.Queryable<sysrole>().Where(v => SqlFunc.ContainsArray(roleids.Select(v => v.roleid).ToList(), v.ID)).ToList();
            List<object> data = new List<object>();
            userinfo.ForEach(v =>
            {
                var rid = roleids.Where(a => a.userid == v.ID).ToList();
                data.Add(new {
                    id = v.ID,
                    name = v.name,
                    userName = v.userName,
                    code = v.code,
                    mobilePhone = v.mobilePhone,
                    sortID = v.sortID,
                    userStateID = v.userStateID,
                    role = rid.Count() ==0?"": roleinfo.Where(n=>n.ID == rid?.First()?.roleid).First()?.title,
                    insertTime = CommonHelper.ConvertSqlTime(v.insertTime),
                });
            });
          
            return new Result() { data = new { data = data, count = totalCount, code = 0, msg = "" } };
        }

        #endregion

        #region 切换用户状态
        public class sustate
        {
            /// <summary>
            /// 
            /// </summary>
            public int id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public bool type { get; set; }
        }
        [HttpPost("switchustate")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> switchustate(sustate sustate)
        {
            var user = db.Queryable<users>().Where(v => v.ID == sustate.id).First();
            var res = db.Ado.UseTran(() =>
            {
                if(sustate.type == true)
                {
                    user.userStateID = 0;
                    db.Updateable(user).UpdateColumns(it => new { it.userStateID }).ExecuteCommand();
                }
                else
                {
                    user.userStateID = 1;
                    db.Updateable(user).UpdateColumns(it => new { it.userStateID }).ExecuteCommand();
                }
            });

            if (res.IsSuccess)
                return Reuse.SuccessCode("设置成功");
            else
                return Reuse.ErrCode(Code.Error, "设置失败");
        }

        #endregion

        #region 登录日志监控列表
        /// <summary>
        /// 登录日志监控列表
        /// </summary>
        /// <returns></returns>
        [HttpPost("logininfo")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> logininfo()
        {
            var form = HttpContext.Request.Form;
            var page = form["page"];
            var limit = form["limit"];
            int totalCount = 0;

            var list = db.Queryable<loginlog>().OrderBy(v => v.ID, OrderByType.Desc).ToPageList(Convert.ToInt32(page), Convert.ToInt32(limit), ref totalCount);
            var userids = list.Select(V => V.userid).ToList();
            var userlist = db.Queryable<users>().Where(v => SqlFunc.ContainsArray(userids, v.ID)).ToList();
            var data = list.Select(v => new {

                userid = userlist.Where(a => a.ID == v.userid).FirstOrDefault()?.userName,
                userip = v.userip,
                resultcode = v.resultcode,
                resultmsg = v.resultmsg,
                insertTime = CommonHelper.ConvertSqlTime(v.insertTime),

            });
            return new Result() { data = new { data = data, count = totalCount, code = 0, msg = "" } };
        }

        #endregion

        #region 接口日志监控
        /// <summary>
        /// 接口日志监控
        /// </summary>
        /// <returns></returns>
        [HttpPost("interfacelog")]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> interfacelog()
        {
            var form = HttpContext.Request.Form;
            var page = form["page"];
            var limit = form["limit"];
            int totalCount = 0;

            var list = db.Queryable<interfacelog>().OrderBy(v=>v.ID,OrderByType.Desc).ToPageList(Convert.ToInt32(page), Convert.ToInt32(limit), ref totalCount);
            var userids = list.Select(V => V.userid).ToList();
            var userlist = db.Queryable<users>().Where(v => SqlFunc.ContainsArray(userids, v.ID)).ToList();
            var data = list.Select(v => new {

                userid = userlist.Where(a => a.ID == v.userid).FirstOrDefault()?.userName,
                userip = v.userip,
                interfacename = v.interfacename,
                requesttype = v.requesttype,
                resultcode = v.resultcode,
                resultmsg = v.resultmsg,
                insertTime = CommonHelper.ConvertSqlTime(v.insertTime),

            });
            return new Result() { data = new { data = data, count = totalCount, code = 0, msg = "" } };
        }
        #endregion

        #region 退出登录
        /// <summary>
        /// 退出登录
        /// </summary>
        /// <returns></returns>
        [HttpPost("outlogin")]
        [AllowAnonymous]
        [ApiExplorerSettings(GroupName = "pc端")]
        public async Task<Result> outlogin()
        {
            string token = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(' ').Last();

            _redis.Remove(token);

            if (_redis.Get(token))
            {
                return Reuse.ErrCode(Code.Error, "退出失败");
            }
            else
            {
                return Reuse.SuccessCode("退出成功");
            }
        }
        #endregion

        #region 导出用户信息
        /// <summary>
        /// 将数据写入Excel，并导出.xlsx文件
        /// </summary>
        /// <returns></returns>
        [HttpPost("Export2")]
        public async Task<ActionResult> Export2()
        {

            // 创建一个工作簿
            IWorkbook workbook = new XSSFWorkbook();

            // 创建一个工作表
            ISheet sheet = workbook.CreateSheet("Sheet1");

            // 添加表头
            IRow headerRow = sheet.CreateRow(0);
            //当前行创建列
            headerRow.CreateCell(0).SetCellValue("ID");
            headerRow.CreateCell(1).SetCellValue("姓名");
            headerRow.CreateCell(2).SetCellValue("录入时间");
            headerRow.CreateCell(3).SetCellValue("密码");
            headerRow.CreateCell(4).SetCellValue("用户名");
            //这个循环是  设置每列的宽度，如果不需要可以省略
            #region 列宽
            for (int n = 0; n < 5; n++)
            {
                sheet.SetColumnWidth(n, 20 * 256);
            }
            #endregion
            //查询数据库数据

            var list = db.Queryable<users>().ToList();

            //编程默认excel第一行是0，第二行是1，第一行我们已经写好标题，那就应该再第二行开始写入数据
            int i = 1;
            //循环表数据依次将数据写入
            foreach (var item in list)
            {
                //创建行
                IRow dataRow = sheet.CreateRow(i);
                //下边的都是创建列和写入值
                dataRow.CreateCell(0).SetCellValue(item.ID.ToString());
                dataRow.CreateCell(1).SetCellValue(item.name);
                dataRow.CreateCell(2).SetCellValue(item.insertTime.ToString("yyyy-MM-dd HH:mm:ss"));
                dataRow.CreateCell(3).SetCellValue(item.userPassword);
                dataRow.CreateCell(4).SetCellValue(item.userName);
                i++;
            }
            //存储目录
            var path = Path.Combine(Directory.GetCurrentDirectory(), "File", "Excel");
            //查看是否存在，不存在创建
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var filename = $"{DateTime.Now.ToString("yyyyMMdd HHmmss")}.xlsx";
            // 保存工作簿
            var filePath = Path.Combine(path, filename);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                workbook.Write(fileStream, true);
            }

            // 导出文件
            var provider = new FileExtensionContentTypeProvider();
            if (!provider.TryGetContentType(filePath, out var contentType))
            {
                contentType = "application/octet-stream";
            }
            var fileContent = System.IO.File.ReadAllBytes(filePath);
            System.IO.File.Delete(filePath);
            return File(fileContent, contentType, filename);
        }
        #endregion

        #region 搜索条件
        /// <summary>
        /// 搜索条件
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ApiExplorerSettings(GroupName = "pc端")]
        public  string Getsearchwhere()
        {
            string where = " 1=1 ";
            var form = HttpContext.Request.Form;
            var formDictionary = HttpContext.Request.Form.ToDictionary(key => key, value => HttpContext.Request.Form[""]);
            var values = formDictionary.Keys.ToList();
            Dictionary<string, string> dict = new Dictionary<string, string>();
            values.ForEach(m =>
            {
                var k = m.Key;
                var v = m.Value;
                dict.Add(k, v);
            });
            dict.TryGetValue("treeid", out string treeid);
            if (!string.IsNullOrEmpty(treeid))
            {
                where += $"and id in (select userid from userrole where roleid in({treeid}))";
            }
            dict.TryGetValue("userName", out string userName);
            if (!string.IsNullOrEmpty(userName))
            {
                where += $" and userName like '%{userName}%'";
            }
            dict.TryGetValue("mobilePhone", out string mobilePhone);
            if (!string.IsNullOrEmpty(mobilePhone))
            {
                where += $" and mobilePhone like '%{mobilePhone}%'";
            }
            dict.TryGetValue("name", out string name);
            if (!string.IsNullOrEmpty(name))
            {
                where += $" and name like '%{name}%'";
            }
            dict.TryGetValue("userState", out string userState);
            if (!string.IsNullOrEmpty(userState))
            {
                where += $" and userStateID in ({userState})";
            }
            return where;
        }
        #endregion

    }
}
