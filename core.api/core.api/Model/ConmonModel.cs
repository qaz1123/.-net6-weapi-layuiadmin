﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;

namespace core.api.Model
{
    public class ConmonModel
    {
    }
    public class LoginBody
    {
        /// <summary>
        /// 
        /// </summary>
        public string? username { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string? password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string captcha { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string? uuid { get; set; }
    }
    public class TreeSelect
    {
        public string? value { get; set; }
        public string? name { get; set; }
        public string? text { get; set; }
        public bool selected
        {
            get;
            set;
        }

        public bool disabled
        {
            get;
            set;
        }
        public string optgroup
        {
            get;
            set;
        }
        public List<TreeSelect> children { get; set; }

    }
    public class Cols
    {
        public string? title { get; set; }
        public bool? unresize { get; set; }
        
        public string? minWidth { get; set; }
        public string? field { get; set; }
        public string width { get; set; }
        public string? style { get; set; }
        public string? templet { get; set; }
        public string? type { get; set; }
        public string? align { get; set; }
        public int? rowspan { get; set; }
        public int? colspan { get; set; }
        public bool? sort { get; set; }
        public bool? totalRow { get; set; }

        [JsonProperty(PropertyName = "fixed")]
        public string? fixedAlign { get; set; }

        [JsonProperty(PropertyName = "event")]
        public string? eventName { get; set; }

    }

    public class MyModel
    {
        public int pid { get; set; }
    }

    public class ListItem
    {
        /// <summary>
        /// 
        /// </summary>
        public int sysID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string sField { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tField { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tBtn { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fBtn { get; set; }
    }

    public class Root
    {
        /// <summary>
        /// 
        /// </summary>
        public string roleID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<ListItem> list { get; set; }
    }
    public class SelectModel
    {
        public string text { get; set; }
        public string name { get; set; }
        public dynamic value { get; set; }

        public List<SelectModel> children { get; set; }

        public SelectModel(string text, dynamic value)
        {
            this.text = text;
            this.value = value;
        }

        public SelectModel()
        {
        }

        public SelectModel(string text, dynamic value, List<SelectModel> children)
        {
            this.text = text;
            this.value = value;
            this.children = children;
        }
        public SelectModel(string text, string name, dynamic value, List<SelectModel> children)
        {
            this.text = text;
            this.name = name;
            this.value = value;
            this.children = children;
        }
    }
}
