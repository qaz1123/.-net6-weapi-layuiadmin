﻿namespace core.api.Model.VO
{
    public class searchadd
    {
        /// <summary>
        /// 
        /// </summary>
        public string title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string field { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string stype { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string sortID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string timerange { get; set; }
        public string timeformat { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string isLock { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string pid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
    }
}
