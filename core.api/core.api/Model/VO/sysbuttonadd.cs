﻿namespace core.api.Model.VO
{
    public class sysbuttonadd
    {
        public System.Int32 ID { get; set; }
        public System.String actype { get; set; }
        public System.String pid { get; set; }

        /// <summary>
        /// 事件
        /// </summary>
        public System.String btevent { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public System.String title { get; set; }

        /// <summary>
        /// 样式
        /// </summary>
        public System.String style { get; set; }

        /// <summary>
        /// 类型0是表格按钮1是头部按钮
        /// </summary>
        public System.String type { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.String sortID { get; set; }

        /// <summary>
        /// 0禁用1启用
        /// </summary>
        public System.String isLock { get; set; }
    }
}
