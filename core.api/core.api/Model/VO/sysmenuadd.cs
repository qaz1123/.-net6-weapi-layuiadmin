﻿namespace core.api.Model.VO
{
    public class sysmenuadd
    {
        /// <summary>
        /// 
        /// </summary>
        public System.String type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public System.Int32? Id { get; set; }
        /// <summary>
        /// 名称
        /// </summary>
        public System.String title { get; set; }

        /// <summary>
        /// 简称
        /// </summary>
        public System.String singleTitle { get; set; }
        /// <summary>
        /// 编码
        /// </summary>
        public System.String code { get; set; }
        public System.String sortID { get; set; }

        /// <summary>
        /// 权限标识
        /// </summary>
        public System.String identify { get; set; }

        /// <summary>
        /// 父级关系
        /// </summary>
        public System.String parentID { get; set; }
        /// <summary>
        /// 链接URL
        /// </summary>
        public System.String linkUrl { get; set; }
        /// <summary>
        /// 是否锁定:0启用1禁用
        /// </summary>
        public System.Int32? isLock { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public System.String remark { get; set; }
    }
}
