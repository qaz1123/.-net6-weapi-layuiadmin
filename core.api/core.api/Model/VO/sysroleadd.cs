﻿namespace core.api.Model.VO
{
    public class sysroleadd
    {
        public System.Int32 ID { get; set; }
        public System.String type { get; set; }


        /// <summary>
        /// 名称
        /// </summary>
        public System.String title { get; set; }

        /// <summary>
        /// singleTitle
        /// </summary>
        public System.String singleTitle { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.Int32? sortID { get; set; }

        /// <summary>
        /// 0禁用1启用
        /// </summary>
        public System.Int32 isLock { get; set; }
    }
}
