﻿using SqlSugar;

namespace core.api.Model
{
    public class rolepermise
    {
        /// <summary>
        /// 角色权限
        /// </summary>
        public rolepermise()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int32? roleid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String tbuttonsid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 menusid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String buttonsid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String filedsid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String sfiledid { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertOrgID { get; set; }

        /// <summary>
        /// 修改时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime? updateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateOrgID { get; set; }
    }
}
