﻿using SqlSugar;

namespace core.api.Model
{
    public class searchfield
    {
        /// <summary>
        /// 搜索字段
        /// </summary>
        public searchfield()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 页面ID
        /// </summary>
        public System.Int32? pid { get; set; }
        public string timeformat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String field { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public System.String stype { get; set; }

        /// <summary>
        ///日期范围
        /// </summary>
        public System.String timerange { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.Int32? sortID { get; set; }

        /// <summary>
        /// 0启用1禁用
        /// </summary>
        public System.Int32 isLock { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertOrgID { get; set; }

        /// <summary>
        /// 修改时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime? updateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateOrgID { get; set; }
    }
}
