﻿using SqlSugar;

namespace core.api.Model
{
    public class sysrole
    {
        /// <summary>
        /// 
        /// </summary>
        public sysrole()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public System.String title { get; set; }

        /// <summary>
        /// singleTitle
        /// </summary>
        public System.String singleTitle { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.Int32? sortID { get; set; }

        /// <summary>
        /// 0禁用1启用
        /// </summary>
        public System.Int32 isLock { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertOrgID { get; set; }

        /// <summary>
        /// 修改时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime? updateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateOrgID { get; set; }
    
}
}
