//读取配置文件
using core.api.Conmon;
using core.api.filter;
using core.api.help;
using core.api.Model;
using core.common.Log;
using core.quartz;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.FileProviders;
using Microsoft.OpenApi.Models;
using MySqlConnector;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using SqlSugar;
using Sundial;
using System.Reflection;

public class Program
{

    public static async Task Main(string[] args)
    {

       //await SyncJobFactory.StartScheduler();

        IConfiguration configuration = new ConfigurationBuilder()
                        .AddJsonFile("appsettings.json")
                        .Build();
        var builder = WebApplication.CreateBuilder(args);
        builder.Services.AddSingleton<ILoggerHelper, LogHelper>();
       
        //log日志注入
        //注册appsettings读取类
        builder.Services.AddSingleton(new AppSettings(builder.Configuration));
        //注入redis
        builder.Services.AddSingleton<IRedisCacheManager, RedisCacheManager>();
        //注入用户缓存
        builder.Services.AddScoped<UserInfo, CustomUser>();
        //注入sqlsugar
        builder.Services.AddSqlsugarSetup(builder.Configuration);


        builder.Services.AddHttpContextAccessor();
        //配置跨域
        builder.Services.AddCors(options =>
        {
            options.AddDefaultPolicy(
                policy =>
                {
                    policy.AllowAnyOrigin().AllowAnyHeader();
                });
        });
        //注册权限验证，每次调用接口都要禁过HelloFilter类
        builder.Services.AddControllers(options =>
        {
            options.Filters.Add<HelloFilter>();
        });
        //全局异常捕获
        builder.Services.AddControllers(options =>
        {
            options.SuppressAsyncSuffixInActionNames = false;
            options.Filters.Add(typeof(GlobalExceptionsFilter));
        });
        // Add services to the container.
        builder.Services.Configure<JWTTokenOption>(builder.Configuration.GetSection("JWTTokenOption"));
        builder.Services.AddTransient<IJWTService, JWTService>();
        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        //builder.Services.AddSwaggerGen();
        builder.Services.AddSwaggerGen(options => {
            // 注释
            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            // 第二个参数为是否显示控制器注释,我们选择true
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename), true);
            // 生成多个文档显示
            typeof(ApiVersions).GetEnumNames().ToList().ForEach(version =>
            {
                //添加文档介绍
                options.SwaggerDoc(version, new OpenApiInfo
                {
                    Title = $"后台管理系统",
                    Version = version,
                    Description = $"项目名:"
                });
            });
        });

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            //app.UseSwaggerUI();
            app.UseSwaggerUI(options =>
            {
                //options.SwaggerEndpoint($"/swagger/V1/swagger.json", $"版本选择:V1");
                //如果只有一个版本也要和上方保持一致
                typeof(ApiVersions).GetEnumNames().ToList().ForEach(version =>
                {
                    //切换版本操作
                    //参数一是使用的哪个json文件,参数二就是个名字
                    options.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"{version}");
                });
            });
        }

        
        //app.UseHttpsRedirection();

        //app.UseAuthorization();

        //app.MapControllers();

        //app.Run();
        app.UseCors();
        app.UseAuthorization();
        app.MapControllers();

        app.UseDefaultFiles();
        app.UseStaticFiles();
        app.UseStaticFiles(new StaticFileOptions { FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot")), RequestPath = new PathString("/wwwroot") });
        app.Run();

    }


    
}

   


