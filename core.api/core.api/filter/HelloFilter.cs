﻿using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Google.Protobuf.Reflection;
using System.IO;
using core.api.Conmon;
using core.api.help;
using SqlSugar;
using StackExchange.Redis;
using core.api.Controllers;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.Http;
using System.Reflection.Metadata;
using Google.Protobuf.WellKnownTypes;
using core.api.Model;
using System;
using Microsoft.AspNetCore.Authorization;
using core.common.IPHelp;

namespace core.api.filter
{
    public class HelloFilter : ActionFilterAttribute
    {
        private readonly UserInfo userInfo; 
        private readonly ISqlSugarClient db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IRedisCacheManager _redis;

        public HelloFilter(UserInfo userInfo, IHttpContextAccessor httpContextAccessor, ISqlSugarClient db, IRedisCacheManager redis)
        {
            this.userInfo = userInfo;
            this._httpContextAccessor = httpContextAccessor;
            this.db = db;
            _redis = redis;
        }



        /// <summary>
        /// Action方法调用之前执行
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                //跳过token验证
                var descriptor = context.ActionDescriptor as ControllerActionDescriptor;
                var actionname = descriptor.ActionName;
                String[] names = new String[] { "captcha", "login", "addmenu", "menulist" , "Export2", "tree" , "huifu" };
                if (names.Contains(actionname.ToLower()))
                {
                    return;
                }
                string token = context.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(' ').Last();
                if (token == null || !userInfo.HaveToken())
                {
                    context.Result = new JsonResult(Reuse.ErrCode(Code.TokenInvalid, "token已过期请重新登录"));
                    return;
                }
            }
            catch (Exception)
            {

                context.Result = new JsonResult(Reuse.ErrCode(Code.TokenInvalid, "权限验证失败"));
                return;
            }
          

                string param = string.Empty;
            string globalParam = string.Empty;

            foreach (var arg in context.ActionArguments)
            {
                string value = Newtonsoft.Json.JsonConvert.SerializeObject(arg.Value);
                param += $"{arg.Key} : {value} \r\n";
                globalParam += value;
            }
            //Console.WriteLine($"webapi方法名称:【{descriptor.ActionName}】接收到参数为：{param}");
        }
        /// <summary>
        /// Action 方法调用后，Result 方法调用前执行
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuted(ActionExecutedContext context) { }
        /// <summary>
        /// Result 方法调用前执行
        /// </summary>
        /// <param name="context"></param>
        public override void OnResultExecuting(ResultExecutingContext context) {
        }
        /// <summary>
        /// Result 方法调用后执行
        /// </summary>
        /// <param name="context"></param>
        public override void OnResultExecuted(ResultExecutedContext context)
        {

            //var descriptor = context.ActionDescriptor as ControllerActionDescriptor;
            //var menu = descriptor.ControllerName + "/" + descriptor.ActionName; //请求路由
            //var type = context.HttpContext.Request.Method;//请求类型
            //string result = string.Empty;
            //if (context.Result is ObjectResult)
            //{
            //    var resultobj = ((ObjectResult)context.Result).Value;
            //    string msg = ((dynamic)resultobj).msg;
            //    dynamic code = ((dynamic)resultobj).code;
            //    string ipAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            //    result = Newtonsoft.Json.JsonConvert.SerializeObject(((ObjectResult)context.Result).Value);
            //    //接口监控
            //    if (descriptor.ActionName.ToLower() == "login")
            //    {
            //        string data = ((dynamic)resultobj).data;

            //        loginlog loginlog = new loginlog();
            //        if (_redis.Get(data))
            //        {
            //            loginlog.userid = _redis.Get<Redismodel>(data).UserID;
            //        }
            //        else
            //        {
            //            loginlog.userid = Convert.ToInt64(0);
            //        }
            //        loginlog.userip = IPHelp.GetCurrentIp(null);
            //        loginlog.resultcode = Convert.ToString(code);
            //        loginlog.resultmsg = msg;
            //        loginlog.insertTime = DateTime.Now;
            //        db.Insertable(loginlog).ExecuteReturnBigIdentity();
            //    }
            //    else
            //    {
            //        interfacelog interfacelog = new interfacelog();
            //        interfacelog.userid = userInfo.Getuserinfo() == null ? Convert.ToInt64(0) : userInfo.Getuserinfo().UserID;
            //        interfacelog.userip = IPHelp.GetCurrentIp(null); ;
            //        interfacelog.resultcode = Convert.ToString(code);
            //        interfacelog.resultmsg = msg;
            //        interfacelog.requesttype = type;
            //        interfacelog.interfacename = menu;
            //        interfacelog.insertTime = DateTime.Now;
            //        db.Insertable(interfacelog).ExecuteReturnBigIdentity();
            //    }
            //}

        }
    }


}
