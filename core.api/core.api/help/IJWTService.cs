﻿namespace core.api.help
{
    public interface IJWTService
    {
        /// <summary>
        /// 获取 Token。
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        string GetToken(string userName, string password);
    }
}
