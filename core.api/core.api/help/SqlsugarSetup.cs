﻿using core.api.Conmon;
using core.api.Controllers;
using core.api.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using MySqlX.XDevAPI;
using SqlSugar;

namespace core.api.help
{
    public static class SqlsugarSetup
    {

        public static void AddSqlsugarSetup(this IServiceCollection services, IConfiguration configuration,string dbName = "ConnectionString")
        {
            
            SqlSugarScope sqlSugar = new SqlSugarScope(new ConnectionConfig()
            {
                DbType = DbType.MySql,
                ConnectionString = configuration[dbName],
                IsAutoCloseConnection = true,
            },
                db =>
                {
                    sqllog sqllog = new sqllog();
                   // var ipAddress = httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
                    //单例参数配置，所有上下文生效       
                    db.Aop.OnLogExecuting = (sql, pars) =>
                    {

                        
                        //sqllog.sql = sql;
                        //sqllog.userid = userInfo.Getuserinfo() == null ? Convert.ToInt64(0) : userInfo.Getuserinfo().UserID;
                        //sqllog.userip = ipAddress;
                        //sqllog.sqlsate = "";
                        //sqllog.sqltime = db.Ado.SqlExecutionTime.ToString();

                        //db.Insertable(sqllog).ExecuteReturnBigIdentity();
                    };

                    db.Aop.OnError = (exp) =>//SQL报错
                    {
                      try
                        {
                            string _sql = exp.Sql + "\r\n" + exp.Parametres.ToString();

                            //sqllog.sql = _sql;
                            //sqllog.userid = userInfo.Getuserinfo() == null ? Convert.ToInt64(0) : userInfo.Getuserinfo().UserID;
                            //sqllog.userip = ipAddress;
                            //sqllog.sqlsate = "失败";
                            //sqllog.sqltime = db.Ado.SqlExecutionTime.ToString();

                            //db.Insertable(sqllog).ExecuteReturnBigIdentity();

                        }
                        catch (Exception ex)
                        {
                            
                        }

                    };
                    //技巧：拿到非ORM注入对象
                    //services.GetService<注入对象>();
                });
            services.AddSingleton<ISqlSugarClient>(sqlSugar);//这边是SqlSugarScope用AddSingleton
        }

    }
}
