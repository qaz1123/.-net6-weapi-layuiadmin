﻿using core.api.Conmon;
using SqlSugar;
using StackExchange.Redis;

namespace core.api.help
{
    public interface UserInfo
    {
        public bool HaveToken();
        public Redismodel Getuserinfo();

    }
    public class CustomUser : UserInfo
    {
        private readonly IRedisCacheManager _redis;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CustomUser(IRedisCacheManager redis, IHttpContextAccessor httpContextAccessor)
        {
            _redis = redis;
            _httpContextAccessor = httpContextAccessor;
        }
        public Redismodel Getuserinfo()
        {
            string token = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(' ').Last();
            return _redis.Get<Redismodel>(token);
        }

        public bool HaveToken()
        {
            string token = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(' ').Last();
            if (_redis.Get(token))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}
