﻿using SqlSugar;

namespace core.api.Model
{
    public class interfacelog
    {
        /// <summary>
        /// 接口日志监控
        /// </summary>
        public interfacelog()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 路由
        /// </summary>
        public System.String interfacename { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? userid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String userip { get; set; }

        /// <summary>
        /// 返回状态码
        /// </summary>
        public System.String resultcode { get; set; }

        /// <summary>
        /// 请求类型
        /// </summary>
        public System.String requesttype { get; set; }

        /// <summary>
        /// 返回信息
        /// </summary>
        public System.String resultmsg { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }
    }
}
