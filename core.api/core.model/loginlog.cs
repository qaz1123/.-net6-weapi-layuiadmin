﻿using SqlSugar;

namespace core.api.Model
{
    public class loginlog
    {
        /// <summary>
        /// 登录日志
        /// </summary>
        public loginlog()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? userid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String userip { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String resultcode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String resultmsg { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }
    }
}
