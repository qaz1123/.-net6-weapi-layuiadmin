﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace core.model
{
    /// <summary>
    /// 
    /// </summary>
    public class organization
    {
        /// <summary>
        /// 
        /// </summary>
        public organization()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String title { get; set; } 

        /// <summary>
        /// 编码
        /// </summary>
        public System.String code { get; set; }

        /// <summary>
        /// singleTitle
        /// </summary>
        public System.String singleTitle { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.Int32? sortID { get; set; }

        /// <summary>
        /// 0启用1禁用
        /// </summary>
        public System.Int32 isLock { get; set; }

        /// <summary>
        /// 父级关系
        /// </summary>
        public System.Int32? parentID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String classList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int32? classLayer { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertOrgID { get; set; }

        /// <summary>
        /// 修改时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime? updateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateOrgID { get; set; }
    }
}
