﻿using SqlSugar;

namespace core.api.Model
{
    public class sqllog
    {
        /// <summary>
        /// sql执行日志
        /// </summary>
        public sqllog()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? userid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String userip { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String sql { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String sqltime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String sqlsate { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }
    }
}
