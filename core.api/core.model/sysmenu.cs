﻿using SqlSugar;

namespace core.api.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class sysmenu
    {
        /// <summary>
        /// 
        /// </summary>
        public sysmenu()
        {
        }
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public List<sysmenu> Child { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public System.String title { get; set; }

        /// <summary>
        /// 简称
        /// </summary>
        public System.String singleTitle { get; set; }

        /// <summary>
        /// 编码
        /// </summary>
        public System.String code { get; set; }

        /// <summary>
        /// 权限标识
        /// </summary>
        public System.String identify { get; set; }

        /// <summary>
        /// 父级关系
        /// </summary>
        public System.Int32? parentID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.String classList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int32? classLayer { get; set; }

        /// <summary>
        /// 链接URL
        /// </summary>
        public System.String linkUrl { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public System.String icon { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.Int32? sortID { get; set; }

        /// <summary>
        /// 是否锁定:0启用1禁用
        /// </summary>
        public System.Int32? isLock { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public System.String remark { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertOrgID { get; set; }

        /// <summary>
        /// 修改时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime? updateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateOrgID { get; set; }
    }
}
