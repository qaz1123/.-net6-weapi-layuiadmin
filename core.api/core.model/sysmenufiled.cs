﻿using Newtonsoft.Json;
using SqlSugar;

namespace core.api.Model
{
    public class sysmenufiled
    {
        /// <summary>
        /// 表头参数
        /// </summary>
        public sysmenufiled()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int32 ID { get; set; }

        /// <summary>
        /// 设定字段名
        /// </summary>
        public System.String field { get; set; }

        /// <summary>
        /// 设定标题名称
        /// </summary>
        public System.String title { get; set; }

        /// <summary>
        /// 设定列宽，若不填写，则自动分配
        /// </summary>
        public System.String width { get; set; }

        /// <summary>
        /// 单元格的最小宽度
        /// </summary>
        public System.String minWidth { get; set; }

        /// <summary>
        /// 设定列类型。可选值有：
        ///normal（常规列，无需设定）
        ///checkbox（复选框列）
        ///radio（单选框列）
        ///numbers（序号列）
        ///space（空列）
        /// </summary>
        public System.String type { get; set; }

        /// <summary>
        /// 浮动
        /// </summary>
        [JsonProperty(PropertyName = "fixed")]
        public System.String fixedAlign { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.String sort { get; set; }

        /// <summary>
        /// 点击事件名
        /// </summary>
        [JsonProperty(PropertyName = "event")]
        public System.String eventName { get; set; }

        /// <summary>
        /// 自定义单元格样式。即传入任意的 CSS 字符
        /// </summary>
        public System.String style { get; set; }

        /// <summary>
        /// 单元格排列方式。可选值有：left、center、right
        /// </summary>
        public System.String align { get; set; }
        /// <summary>
        /// 页面ID
        /// </summary>
        public System.Int32 pid { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.Int32? sortID { get; set; }

        /// <summary>
        /// 是否锁定:0启用1禁用
        /// </summary>
        public System.Int32? isLock { get; set; }

        /// <summary>
        /// 模板名称
        /// </summary>
        public System.String templet { get; set; }

        /// <summary>
        /// 是否开启该列的自动合计功能
        /// </summary>
        public System.String totalRow { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertOrgID { get; set; }

        /// <summary>
        /// 修改时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime? updateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateOrgID { get; set; }
    }
}
