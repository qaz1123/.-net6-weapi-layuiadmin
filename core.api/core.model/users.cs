﻿using SqlSugar;
using System.Security.Principal;

namespace core.api.Model
{
    public class users
    {
        /// <summary>
        /// 用户表
        /// </summary>
        public users()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public System.Int64 ID { get; set; }

        /// <summary>
        /// 帐号
        /// </summary>
        public System.String userName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        public System.String userPassword { get; set; }

        /// <summary>
        /// 用户编码
        /// </summary>
        public System.String code { get; set; }

        /// <summary>
        /// 用户姓名
        /// </summary>
        public System.String name { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public System.String mobilePhone { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public System.String profilePhotoUrl { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public System.Int32? sortID { get; set; }

        /// <summary>
        /// 用户状态
        /// </summary>
        public System.Int32? userStateID { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public System.String remark { get; set; }

        /// <summary>
        /// 添加时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime insertTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64 insertOrgID { get; set; }

        /// <summary>
        /// 修改时间（yyyy-MM-dd HH:mm:ss）
        /// </summary>
        public System.DateTime? updateTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateUserID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public System.Int64? updateOrgID { get; set; }
    }
}
