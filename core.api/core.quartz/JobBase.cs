﻿using Quartz;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.quartz
{
    public class JobBase
    {
        private readonly ISqlSugarClient db;
        public JobBase(ISqlSugarClient db)
        {
            this.db = db;
        }
        public async Task<string> ExecuteJob(IJobExecutionContext context, Func<Task> func)
        {

            Console.WriteLine($"Job_Blogs_Quartz 执行 {DateTime.Now.ToShortTimeString()}");
            return "aaaaaa";
        }

    }
}
