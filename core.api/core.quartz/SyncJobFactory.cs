﻿using Quartz.Spi;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using Quartz.Impl;
using SqlSugar;
using core.api.Model;

namespace core.quartz
{
    /// <summary>
    /// 自定义Job工厂
    /// </summary>
    public class SyncJobFactory
    {
        public static async Task StartScheduler()
        {
            StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();
            var scheduler = await schedulerFactory.GetScheduler();
            await scheduler.Start();
          
            scheduler.Start();
            //Trigger时间触发机制
            var trigger = TriggerBuilder.Create()
                .WithIdentity("哈哈哈", "嘿嘿嘿")
                //.StartNow() //立即执行
                .WithSimpleSchedule(w => w.WithIntervalInSeconds(5).RepeatForever())// WithIntervalInSeconds(5).RepeatForever() 五秒执行一次无限循环
                .Build();
            //Job详细描述
            var jobDetail = JobBuilder.Create<SyncJobl>()
                .WithDescription("1")
                .WithIdentity("xm", "zx")
                .Build();
            //把时间和任务通过载体关联起来
            await scheduler.ScheduleJob(jobDetail, trigger);
            // 开始调度器  
            scheduler.Start();
        }
    }
}
