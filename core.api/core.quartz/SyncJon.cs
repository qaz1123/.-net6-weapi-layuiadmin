﻿using core.api.Model;
using Quartz;
using Quartz.Impl;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace core.quartz
{
    /// <summary>
    /// 定时执行的任务JOB
    /// </summary>
    
  public class SyncJobl : IJob
    {
        private readonly ISqlSugarClient db;
        public SyncJobl(ISqlSugarClient db)
        {
            db = db;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Execute(IJobExecutionContext context)
        {
          var list = db.Queryable<users>().ToList();
            await Task.Run(() =>
            {
                Console.WriteLine($"******************************");
                Console.WriteLine($"测试时间{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff")}");
                Console.WriteLine($"测试信息" + context);
                Console.WriteLine($"******************************");
                Console.WriteLine();
            });

        }
    }
      
}
