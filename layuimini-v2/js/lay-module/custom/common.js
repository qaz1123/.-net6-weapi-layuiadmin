/**
 * 
 */


layui.define(['table', 'utils', 'laytpl', 'tree', 'treetable', 'form', 'laydate', 'xmSelect'], function (exports) {
    var table = layui.table;
    var $ = layui.jquery;
    var utils = layui.utils;
    var laytpl = layui.laytpl;
    var treetable = layui.treetable;
    var form = layui.form;
    var laydate = layui.laydate;
    var xmSelect = layui.xmSelect;
    var tree = layui.tree;

    let field = [];
    let option = {};
    var ttable = null;
    var common = {
        pageconfig(opt, istree, id) {
        var loadIndex = layer.load(2);

            let html = ` <div class="layuimini-container">
                <div class="layuimini-main">
                    <div class="layui-collapse">
                        <div class="layui-colla-item">
                            <h2 class="layui-colla-title">搜索条件</h2>
                            <div class="layui-colla-content layui-show">
                            <div class="layui-form layui-form-pane ply_edit" lay-filter="ply_edit">

                                <div class="layui-inline">
                                <button class="layui-btn layui-btn-xs scarchtj"  lay-submit lay-filter="datasearchbtn" data-event="reload" style="background-color: #28a745;">搜索</button>
                                </div>
                            </div>
                        </div>
                        <div class="layui-colla-item addleft" style="position: relative">
                            <h2 class="layui-colla-title">表格</h2>
                            <div class="layui-colla-content layui-show" id="tablewidth">
                                <table id="demo" lay-filter="test"></table>
                            </div>
                           
                        </div>

                    </div>
                </div>
            </div>
            <!-- 头部按钮 -->
            <div class="layui-hide toptoolbar">
               <div class="layui-btn-container tanletoolbar"></div>
            </div>
           
            <!-- 表格按钮 -->
            <div class="layui-hide fun-btns">
                <div class="layui-btn-container toolbar"></div>
            </div>
            `


            utils.promise()
                .then(() => {
                    // 获取页面按钮
                    utils.ajaxGet(utils.host + "api/menu/getbtn?id=" + id).then(res => {
                        utils.doneCb(res, () => {
                            field = JSON.parse(res.data.cooa)
                            // 字段宽度为空时，根据页面大小自适应
                            var allwidth = 0;
                            field.map(v => {
                                if (v.width != 0) {
                                    allwidth += parseInt(v.width);
                                }
                            });
                            if (!istree) {
                                field.map(v => {
                                    if (v.width == 0) {
                                        if ($("#tablewidth").width() - allwidth - 10 > 0) {
                                            v.width = $("#tablewidth").width() - allwidth - 10
                                        }
                                    }
                                });
                            }
                            //搜索字段
                            $(".ply_edit").append(res.data.sfidle);
                            form.render();
                            //表格按钮
                            laytpl(utils.listTemp).render(res.data.linelist, function (html) {
                                let fun_btns_wrapper = $('.fun-btns').find('.toolbar');
                                fun_btns_wrapper.append(html);
                            });
                            //表格头部按钮
                            laytpl(utils.listTemp).render(res.data.btnlist, function (html) {
                                let fun_btns_wrapper = $('.toptoolbar').find('.tanletoolbar');
                                fun_btns_wrapper.append(html);
                            });
                            //xmselect下拉树渲染
                            $('.ply_edit div[data-type="xmselect"]').each(function () {
                                let $this = $(this);
                                let data = $this.data('json');
                                let id = $this.data('id');

                                xmSelect.render({
                                    el: '#' + id + '',
                                    name: id,
                                    autoRow: true,
                                    model: { label: { type: 'text' } },
                                    radio: true,
                                    clickClose: true,
                                    tree: {
                                        show: true,
                                        strict: false,
                                    },
                                    theme: {
                                        color: "#1e9fff",
                                        maxColor: "#1e9fff",
                                        hover: '#eeeeee'
                                    },
                                    toolbar: {
                                        show: true,
                                        list: ['CLEAR']
                                    },
                                    height: 'auto',
                                    data: data,
                                })

                            });

                            //搜索栏日期格式表单渲染
                            $('.ply_edit input[data-stype="date"]').each(function () {
                                let $this = $(this);
                                let type = $this.data('type');
                                let format = $this.data('format');
                                let range = $this.data('range');
                                let id = $this.attr('id');

                                let opt = {
                                    elem: '#' + id,
                                    type: type,
                                    theme: '#5593C8'
                                };
                                range && Object.assign(opt, { range: true })
                                format && Object.assign(opt, { format: format })
                                laydate.render(opt);
                            });
                        });
                    })
                        .then(() => {
                            if (istree) {
                                option = {
                                    tree: {
                                        iconIndex: 1,
                                        idName: 'id',
                                        pidName: 'pid',
                                        haveChildName: 'haveChild',
                                        isPidData: true
                                    },
                                    headers: {
                                        'Authorization': utils.token,
                                    },
                                    method: 'POST',
                                    elem: '#demo',
                                    url: opt.url,
                                    where: opt.where,
                                    height: `full-${$('.layui-colla-item')[0].offsetHeight + 70}`,
                                    toolbar: '.tanletoolbar',
                                    page: false,
                                    cols: field,
                                    done: function () {

                                    }
                                }
                                ttable = treetable.render(option)
                            } else {
                                option = {
                                    elem: '#demo',
                                    height: `full-${$('.layui-colla-item')[0].offsetHeight + 70}`,
                                    url: opt.url,
                                    width: 'auto',
                                    toolbar: '.tanletoolbar',
                                    method: "post",
                                    page: opt.page,
                                    headers: {
                                        'Authorization': utils.token
                                    },
                                    cols: [field],
                                    where: opt.where,
                                    parseData: function (res) {
                                        return {
                                            "code": res.data.code, //解析接口状态
                                            "msg": res.data.msg, //解析提示文本
                                            "count": res.data.count, //解析数据长度
                                            "data": res.data.data //解析数据列表
                                        };
                                    },
                                    done: function () {

                                    }
                                }
                                table.render(option)
                            }
                        })
                })
                .then(() => {
                    if (istree) {
                        form.on('submit(datasearchbtn)', function (data) {
                            Object.assign(data.field, { id: id })
                            ttable.reload({
                                url: opt.url,
                                where: data.field,
                            });
                        });
                    } else {
                        form.on('submit(datasearchbtn)', function (data) {
                            Object.assign(data.field, { id: id })
                            table.reload('demo', {
                                url: opt.url,
                                where: data.field,
                                page: {
                                    curr: 1
                                }
                            });
                        });
                    }

                })
                .then(() => {
                    $('body').append(html).promise().done(() => {

                    });
                })
                .then(() => {
                    // 是否存在左侧搜索
                    if(opt.lefttree){
                      $(".addleft").append(` <div class="lefttree">
                        <div class="lefttreetitle"><span style="margin-left:15px">${opt.lefttreetitle}</span></div>
                        <div id="lefttree"></div>
                        </div>`);
                        $("#tablewidth").addClass("tablepaddingleft")

                        utils.ajaxPost(opt.lefttreeurl, {}).then(res => {
                            utils.doneCb(res, () => {
                                var inst1 = tree.render({
                                    elem: '#lefttree', //绑定元素
                                    data: res.data,
                                    click: function (obj) {
                                        $(".layui-tree-set-active").removeClass("layui-tree-set-active");
                                        $(obj.elem).addClass("layui-tree-set-active");
                                        table.reload('demo',{
                                            url: opt.url,
                                            where: {
                                                treeid:obj.data.id
                                            },
                                        });
                                    }
                                });
                            });
                        });
                    }
                    if (opt && opt.calback) opt.calback(opt);
                }).then(()=>layer.close(loadIndex))

        },

        pageedit(opt) {
            opt.cols = opt.cols || 1;
            var width = 100 / opt.cols - 0.5;
            var _html = '';

            for (var i = 0; i < opt.arr.length; i++) {
                var othis = opt.arr[i];
                _html += `<div class="layui-form-item ${othis.class ? othis.class : ''}${othis.hide ? 'layui-hide' : ''}"${othis.pane ? othis.pane : ''} style="width:${width}%;${othis.style ? othis.style : ''}">`;
                _html += '<label class="layui-form-label ';
                _html += '">' + (othis.required ? '<span style="color:red">*</span>&nbsp;&nbsp;' : '<span style="color:red;visibility:hidden;">*</span>&nbsp;&nbsp;') +
                    othis.title + '：&nbsp;&nbsp;</label>' +
                    '<div class="layui-input-block">'
                if (typeof othis.tempt == 'function') {
                    _html += othis.tempt();
                } else {
                    let type = othis.type,
                        name = `name="${othis.field}"`,
                        title = othis.title ? (`title="${othis.title}"`) : '',
                        value = othis.value ? othis.value : '',
                        disabled = othis.disabled ? 'disabled="disabled"' : '',
                        readonly = othis.readonly ? 'readonly="readonly"' : '',
                        placeholder = othis.placeholder ? `placeholder="${othis.placeholder}"` : `placeholder="请输入${othis.title}"`,
                        required = othis.required ? ('lay-verify="' + (othis.requiredText || 'required') + '"') : '',
                        filter = othis.filter ? ('lay-filter="' + othis.filter + '"') : '',
                        search = othis.search ? 'lay-search' : '',
                        notreset = othis.notreset ? 'lay-notreset' : '',
                        attr = othis.attr ? othis.attr : '',
                        height = othis.height ? ('style="height:' + othis.height + '"') : '';

                    if (type == 'input' || !type) {
                        _html += `<input type="text" ${attr} ${readonly} ${name} value="${value}" ${placeholder} autocomplete="off" ${required} class="layui-input" ${filter} ${disabled} ${title}>`;
                    } else if (othis.type == 'number') {
                        _html += `<input type="number" ${attr} ${readonly} ${name} value="${value}" ${placeholder} autocomplete="off" ${required} class="layui-input" ${filter} ${disabled} ${title}>`
                    }
                    else if (othis.type == 'select') {
                        _html += `<select ${attr} ${name} ${readonly} ${required} ${filter} ${search} ${disabled} ${notreset}>`;
                        if (value) {
                            value.forEach(v => {
                                let selected = v.selected ? 'selected' : '',
                                    _value = `value="${v.value}"`,
                                    _text = v.text;
                                _html += `<option ${selected} ${_value}>${_text}</option>`;
                            });
                        }
                        _html += '</select>';
                    } else if (othis.type == 'textarea') {
                        _html += `<textarea ${attr}  ${readonly} ${height} class="layui-textarea" ${name} value="${value}" ${placeholder} autocomplete="off" ${required} ${filter} ${title} ${disabled}></textarea>`;
                    } else if (othis.type == 'radio') {
                        value.forEach(v => {
                            _html += `<input  ${attr} ${readonly} type="radio" ${name} ${filter} value="${v.value}" title="${v.text}" ${v.checked ? 'checked' : ''} ${disabled}>`;
                        });
                    } else if (othis.type == "checkbox") {
                        value.forEach(v => {
                            _html += `<input  ${attr} ${readonly} type="checkbox" ${name} ${filter} value="${v.value}" title="${v.text}" ${v.checked ? 'checked' : ''} ${disabled}>`;
                        });
                    }
                }
                _html += '</div></div> ';
            }

            $(opt.ele).html(_html).promise().done(() => {
                if (!opt.btnHide) {
                    _html = '<div class="layuibutton"><button class="layui-btn layui-bg-blue" lay-submit lay-filter="saveBtn">' + (opt.btnText || '确认保存') + '</button> </div>';
                    $(opt.ele).append(_html);
                }
            })
                .promise().done(() => {
                    if ($(opt.ele).find('select').length > 0 || $(opt.ele).find('*[type="checkbox"]').length > 0 || $(opt.ele).find('*[type="radio"]').length > 0)
                        form.render();
                    if (opt && opt.calback) opt.calback();
                })
                .promise().done(() => {
                    // if (opt.submit) {

                    // }
                })
        },
        select(data) {
            if (!data) return '';

            let html = '';
            for (var i = 0; i < data.length; i++) {
                html += `<option ${data[i].selected ? 'selected' : ''} value="${data[i].value}">${data[i].text}</option>`;
            }
            return html;
        },
    }

    exports('common', common);
})