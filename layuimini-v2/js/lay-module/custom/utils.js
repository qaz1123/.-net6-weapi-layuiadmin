/**
 * 
 */

layui.define([], function (exports) {
     var  $ = layui.$;
    var base_host = 'https://localhost:7526/';

    var utils = {

        listTemp: `
               {{#  layui.each(d, function(index, item){ }}
               <button class="layui-btn layui-btn-xs" style="{{item.style}}" lay-event="{{item.btevent}}">{{item.title}}</button>
                {{#  }) }}
                `,
        /**
         * 请求路径
         */
        host: base_host,
        //token
        token: localStorage['token'] != undefined ? localStorage['token'] : '',
        /**
         * 
         * @returns 
         */
        promise: function () {
            return new Promise(resolve => {
                resolve();
            })
        },
        /**
         * 获取地址栏参数
         * @param {*} name 
         * @returns 
         */
        getUrlPar: function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return unescape(r[2]);
            };
            return null;
        },
        /**
         * 
         * @param {*} url 
         * @returns 
         */
        ajaxGet: function (url) {
            return axios({
                url: url,
                method: 'get',
                headers: {
                    'Authorization': utils.token
                }
            }).then(function (response) {
                return response.data;
            }).catch(function (err) {
                return err;
            })
        },
        /**
         * 
         * @param {*} url 
         * @param {*} data 
         * @param {*} postJson 
         * @param {*} formTran 
         * @returns 
         */
        ajaxPost: function (url, data, postJson, formTran) {
            let timeStamp = new Date().getTime(),
                nonce = (function () {
                    let str = `xxxxxx`;
                    return str.replace(/[xy]/g, function (c) {
                        var r = Math.random() * 16 | 0;
                        var v = c == 'x' ? r : (r & 0x3 | 0x8);
                        return v.toString(16)
                    });
                })();
            !postJson && (postJson = true);
            if (postJson) {
                return axios({
                    url: url,
                    method: 'post',
                    data: data,
                    headers: {
                        'Nonce': nonce,
                        'TimeStamp': timeStamp,
                        // 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                        'Authorization': utils.token
                    }
                })
                    .then(function (response) {
                        return response.data;
                    }).catch(function (err) {
                        return err;
                    })
            }
            //表单
            formTran = formTran || true
            return axios({
                url: url,
                method: 'post',
                data: data,
                headers: {
                    'Nonce': nonce,
                    'TimeStamp': timeStamp,
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Authorization': utils.token
                },
                transformRequest: [function (data, headers) {
                    if (formTran) {
                        var ret = '';
                        for (var it in data) {
                            ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&';
                        }
                    } else
                        ret = data;

                    return ret;
                }]
            }).then(function (response) {
                return response.data;
            }).catch(function (err) {
                return err;
            })

        },
        /**
         * ajax 请求成功回调
         * @param {*} res 
         * @param {*} doneCb 
         * @param {*} errCb 
         */
        doneCb: function (res, doneCb, errCb) {
            if (res && res.code) {
                if (res.code == 200) {
                    if (doneCb)
                        doneCb();
                } else if (res.code == 403 || res.code == 200100) {
                    top.layer.msg(res.msg, { anim: 0, time: 2000 }, function () {
                        top.window.location = '/login.html';
                    })
                } else {
                    if (errCb)
                        errCb()
                    else
                        top.layer.msg(res.msg, { anim: 0, time: 2000 })
                }
            } else {
                top.layer.msg("请求出错，未知原因", { anim: 0, time: 2000 })
            }
        },
        //下载文件  
        postDownFile: function (url, data, fileName) {
            //let loading = layer.msg('下载中,请耐心等待...', { shade: [0.3, '#000000'] });
            // let loading = this.loadding();
            return axios({
                url: url,
                method: 'POST',
                data: data,
                headers: {
                    'Authorization': utils.token
                },
                onDownloadProgress(progress) {
                    if (downCb) { downCb(progress) }
                    else {
                        var progressHtml = `<div class="progress-div"> <div class="progress-text"> </div> <div class="progress-content"> <div class="layui-progress layui-progress-big" lay-showPercent="true" lay-filter="demo"> <div class="layui-progress-bar layui-bg-green" lay-percent="0%"></div> </div> </div> <div class="progress-footer"> <span class="load-size">00M</span>/<span class="total-size">00M</span> </div> </div>`;
                        if (progress.total / 1024 < 1024*5) {
                            return;
                        }
                        window.loadPickerIndex = window.loadPickerIndex || layer.open({
                            title: false,
                            closeBtn: 0,
                            id: "progress-layer",
                            type: 1,
                            content: progressHtml
                        });
                       layer.style(window.loadPickerIndex, {
                            "border-radius": '30px',
                            "border": "1px solid #009688"
                        });
                        var str = "M";
                        var total = 0;
                        var loadSize = 0;
                        if (progress.total / 1024 < 1024) {
                            str = "KB";
                            total = progress.total / 1024;
                            loadSize = progress.loaded / 1024;
                        }
                        else {
                            total = progress.total / 1024 / 1024;
                            loadSize = progress.loaded / 1024 / 1024;
                        }
                        $(".total-size").text(total.toFixed(2) + str);
                        $(".load-size").text(loadSize.toFixed(2) + str);
                        var bfb = Math.round(progress.loaded / progress.total * 100) + '%';
                        $(".layui-progress-bar").attr("lay-percent", bfb);
                        element.progress('demo', bfb);
                        if(bfb==100+"%"){
                            $(".progress-text").text("等待浏览器验证中....");
                            setTimeout(function () {
                                layer.close(window.loadPickerIndex);
                                delete window.loadPickerIndex;
                            }, progress.total/1024/1024*15);
                        }
                    }
                },
                responseType: 'blob',
            }).then((res) => {
                const url = window.URL.createObjectURL(new Blob([res.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', fileName);
                document.body.appendChild(link);
                link.click();

                return res;
            }).catch(err => {
                console.error(err)
            }).finally(() => {
                // layer.close(loading)
                }
            );
        },
        closeparent: function () {
            var iframeIndex = parent.layer.getFrameIndex(window.name);
            parent.layer.close(iframeIndex);
            window.parent.location.reload();
        },
        tableclose: function () {
            var iframeIndex = parent.layer.getFrameIndex(window.name);
            parent.layer.close(iframeIndex);
        },
        tablereload: function () {
            if ($('button[data-event="reload"]'))
                $('button[data-event="reload"]').trigger('click');
        },
    }

    exports('utils', utils);
})